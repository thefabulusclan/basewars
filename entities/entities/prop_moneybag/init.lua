AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

function ENT:Initialize()

	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Entity:DrawShadow( false )

	// Don't collide with the player
	// too bad this doesn't actually work.
	self.Entity:SetCollisionGroup( COLLISION_GROUP_WEAPON )

	timer.Simple(60, function()
		if IsValid(self.Entity) then
			self:Remove()
		end
	end)

	local phys = self.Entity:GetPhysicsObject()


	if (phys:IsValid()) then
		phys:Wake()
	end

	self.Entity.called = false

end

function ENT:Use(ply)
	if !self.Entity.called then
		Notify( ply, 2, 4, "You found $" .. self.Entity.Amount );
		ply:AddMoney( self.Entity.Amount );
		ply:ConCommand("play tfc/chaching.mp3" )
		self.Entity.called = true
		self.Entity:Remove();
	end
end

function ENT:Touch( ent )
	if ent:GetClass( ) == "prop_moneybag" then
		if !ent.called && !self.Entity.called then
				self:GetTable().Amount = self:GetTable().Amount + ent:GetTable( ).Amount
				ent.called = true
				ent:Remove( )
		end
	end
end
