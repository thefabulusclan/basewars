AddCSLuaFile "cl_init.lua";
AddCSLuaFile "shared.lua";
include "shared.lua";

function ENT:UpdateMoney( )
	if self:IsValid( ) then
		self.Entity:GetBWOwner():SetNWString( "vaultamount", self.Money )
	end;
end;

function ENT:BWPrePhys()
	self:SetModel( "models/props_lab/powerbox01a.mdl" )
end

function ENT:BWInit( )
	self:EmitSound( "ambient/machines/thumper_startup1.wav" )

	self.Entity:SetMaxHealth(1000)
	self.Entity:SetHealth(1000)

	self.Money = 0
	self.KeyPress = CurTime()
	self.lastInterest = CurTime()
end;

function ENT:Use( activator, caller )
	if self.KeyPress + 5 < CurTime() then
		if activator:IsUserGroup("owner") || activator:IsUserGroup("dev") || self.Entity:GetBWOwner() == activator then
			umsg.Start( "Bankmenu", activator )
				umsg.Entity(self.Entity:GetBWOwner())
			umsg.End( )
			self.KeyPress = CurTime()
		else
			self.KeyPress = CurTime()
			activator:ChatPrint( "That isn't your Bank!" )
		end
	end
end

function ENT:Think()
	if !IsValid(self.Entity:GetBWOwner()) then
		self.Entity:Remove()
	end
	if self.lastInterest + 300 < CurTime() then
		self.Entity:GiveInterest()
		self.lastInterest = CurTime()
	end
end

function ENT:GiveInterest()
	if self.Money>1000000000000 then Notify(self.Entity:GetBWOwner(), 4, 3, "Your bank is too full to collect interest!") return end
	if !self.Entity:IsPowered() then
		Notify(self.Entity:GetBWOwner(), 4, 3, "The bank needs power to provide interest!")
		return
	end
	if self.Money > 100 then
		self.Money = math.Round(self.Money+(self.Money*((self.Entity:GetUpgrade()/100)+0.01)))
		self:UpdateMoney()
		Notify( self.Entity:GetBWOwner(), 0, 3, "Your bank has developed interest!" )
	end
end

function ENT:EjectMoney( )
	if self.Money > 0 then
		local moneybag = ents.Create( "prop_moneybag" )
      moneybag:SetModel( "models/props/cs_assault/Money.mdl" )
			moneybag:SetPos( self:GetPos( ) + Vector( 0, 0, 20 ) )
			moneybag:SetAngles( self:GetAngles( ) )
			moneybag:SetColor( Color(200, 255, 200, 255 ))
			moneybag:Spawn()
			moneybag:GetTable().MoneyBag = true
			moneybag:GetTable().Amount = self.Money
			moneybag:SetVelocity( Vector( 0, 0, 10 ) * 10 )
			moneybag.Ejected = true

			self.Money = 0
			self:UpdateMoney()
	else
		Notify(self.Entity:GetBWOwner(), 4, 3, "This bank is empty!")
		end
end

function ENT:BWRemove()
	self:EjectMoney();
	self:EmitSound( "ambient/machines/thumper_shutdown1.wav" )
end;

function ENT:Touch(ent)
	if ent:GetClass() == "prop_moneybag" then
		if !ent.Ejected && !ent.called then
			self.Money = self.Money + ent:GetTable().Amount
			ent.called = true
			ent:Remove()
			self:UpdateMoney()
		end
	end
end


function ll_vault_eject( ply, cmd, argv )
	local trace = {}
		trace.start = ply:GetShootPos( )
		trace.endpos = ply:GetShootPos( ) + ( ply:GetAimVector( ) * 200 )
		trace.filter = ply
		local traceline = util.TraceLine( trace )
		if traceline.HitNonWorld and traceline.Entity:IsValid( ) then
			traceline.Entity:EjectMoney( )
			traceline.Entity:EmitSound( "ambient/alarms/klaxon1.wav" )
		end;
end
concommand.Add( "ll_vault_eject", ll_vault_eject )

function ll_vault_deposit( ply, cmd, args )
	if !args[1] or tonumber(args[1]) == 0 then Notify( ply, 4, 3, "Please type a number!") return end
	local trace = {}
		trace.start = ply:GetShootPos()
		trace.endpos = ply:GetShootPos() + ( ply:GetAimVector( ) * 200 )
		trace.filter = ply
		local traceline = util.TraceLine( trace )
		if !ply:CanAfford(tonumber(args[1])) then Notify(ply, 4, 3, "You cannot afford this!") return end
		if traceline.HitNonWorld and traceline.Entity:IsValid() then
			ply:AddMoney(-tonumber(args[1]))
			Notify(ply, 0, 3, "You deposited $" .. args[1] .."!")
			traceline.Entity.Money = traceline.Entity.Money + tonumber(args[1])
			traceline.Entity:UpdateMoney()
		end
end
concommand.Add( "ll_vault_deposit", ll_vault_deposit )
