include('shared.lua')

function ENT:Draw( )


	self.Entity:DrawModel()

	local Pos = self:GetPos()
	local Ang = Angle( 0, 0, 0) -- self:GetAngles()
	local scans = self.Entity:GetNWInt("scans")
	local upgrade = self.Entity:GetUpgrade()

--	if ply:GetNWEntity( "drawmoneytitle" ) == true then

	surface.SetFont("HUDNumber5")
	local TextWidth = surface.GetTextSize("Radar Tower")
	local TextWidth2 = surface.GetTextSize("Level:"..upgrade)
	local TextWidth3 = surface.GetTextSize("Number of Scans:" ..scans)

	local ply = self.Entity:GetBWOwner()

	Ang:RotateAroundAxis(Ang:Forward(), 90)
	local TextAng = Ang
---TextAng, LocalPlayer():GetPos():Distance(self:GetPos()) / 500
	TextAng:RotateAroundAxis(TextAng:Right(), CurTime() * -180)
	cam.Start3D2D(Pos + Ang:Right() * -137, TextAng, 0.15)
		draw.WordBox(.7, -TextWidth*0.5, -30, "Radar Tower", "HUDNumber5", Color(0, 0, 0, 155), Color(255,255,255,255))
		draw.WordBox(.7, -TextWidth2*0.5, 18, "Level:"..upgrade, "HUDNumber5", Color(0, 0, 0, 155), Color(255,255,255,255))
		draw.WordBox(.7, -TextWidth3*0.5 +5, 70,"Number of Scans:" ..scans, "HUDNumber5", Color(0, 0, 0, 155), Color(255,255,255,255))
	cam.End3D2D()
end

function Scangui()
	local Master = vgui.Create("DFrame")
	Master:SetPos(ScrW()/2-200, ScrH()/2-150)
	Master:SetSize(400, 400)
	Master:SetTitle("Scan")
	Master:SetVisible(true)
	Master:SetDeleteOnClose(true)
	Master:MakePopup()

	local List = vgui.Create("DListView", Master)
	List:SetPos(10,30)
	List:SetSize(380, 300)
	List:Clear()
	List:AddColumn("Name")
	List:AddColumn("Distance")
	List:AddColumn("Team")

	for k,v in pairs(player.GetAll()) do
		List:AddLine(v:GetName(), v:GetPos():Distance(LocalPlayer():GetPos()), team.GetName(v:Team()), v:SteamID())
	end

	List.DoDoubleClick = function(parent, index, line)
		LocalPlayer():ConCommand("bw_scan \"" .. line:GetValue(4) .. "\"")
		timer.Simple(0.5, function()
			List:Clear()
			for k,v in pairs(player.GetAll()) do
				List:AddLine(v:GetName(), v:GetPos():Distance(LocalPlayer():GetPos()), team.GetName(v:Team()), v:SteamID())
			end
		end)
	end

	local scanButton = vgui.Create("DButton", Master)
	scanButton:SetPos(10, 350)
	scanButton:SetText("Scan")
	scanButton.DoClick = function()
		LocalPlayer():ConCommand("bw_scan \"" .. List:GetLine(List:GetSelectedLine()):GetValue(4) .. "\"")
	end

	local raidButton = vgui.Create("DButton", Master)
	raidButton:SetPos(325, 350)
	raidButton:SetText("Raid")
	raidButton.DoClick = function()
		LocalPlayer():ConCommand("bw_raid \"" .. List:GetLine(List:GetSelectedLine()):GetValue(4) .. "\"")
	end

end
usermessage.Hook("Scangui", Scangui)
