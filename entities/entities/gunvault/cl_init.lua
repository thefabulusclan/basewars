include('shared.lua')

function MsgGunVault( msg )
	local gunlist = string.Explode(",", msg:ReadString() )
	local vault = msg:ReadShort()
	local inputenabled = false;
	if( HelpToggled or GUIToggled ) then
		inputenabled = true;
	end
	local panel = vgui.Create( "DFrame" )
	panel:SetPos((ScrW()/2)-85, (ScrH()/2) - 40)
	panel:SetName( "Panel" )
	panel:SetSize(170,200)
	panel:SetSizable(false)
	panel:SetTitle("Select a gun")
	panel:ShowCloseButton(true)
	panel:SetVisible( true )
	panel:MakePopup()
	local ybutton = {}
	local upgradelabel = {}
	if gunlist[1]!="" then
		for i=1, table.Count(gunlist), 1 do
			ybutton[i] = vgui.Create( "DButton", panel );
			ybutton[i]:SetPos( 15, 20+(i*15) );
			ybutton[i]:SetSize( 130, 14 );
			local gunname = gunlist[i]
			ybutton[i]:SetText( gunname );
			ybutton[i].DoClick = function(msg) LocalPlayer():ConCommand( "withdrawgun " .. vault .. " " .. i .. "\n" ) panel:Close() end
			ybutton[i]:SetVisible( true );
		end
	else
		local label = vgui.Create( "DLabel", panel );
		label:SetPos( 15, 45 );
		label:SetSize( 130, 40 );
		label:SetText( "This Gun Vault is empty. \nDrop guns into it before \ntrying to take guns out." );
		label:SetVisible( true );
	end
end
usermessage.Hook( "gunvaultgui", MsgGunVault );
