-- ============================================
-- =                                          =
-- =          Crate SENT by Mahalis           =
-- =                                          =
-- ============================================

// copy pasta from DURG lab

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:BWPrePhys()
	self.Entity:SetModel( "models/props/cs_militia/footlocker01_closed.mdl")
end

function ENT:BWInit()
	self.Entity:SetNWBool("sparking",false)
	self.Entity:SetMaxHealth(750)
	self.Entity:SetHealth(750)
	local ply = self.Entity:GetBWOwner()
	self.Locked = true
	self.LastUsed = CurTime()
	self.Guns = {}
end

function ENT:Use(activator,caller)
	if (self.LastUsed+0.5>CurTime()) then
		self.LastUsed = CurTime()
	else
		self.LastUsed = CurTime()
		local ply = self.Entity:GetBWOwner()
		if (activator==ply || !self.Locked || activator:IsAllied(ply)) then
			if (activator==ply || activator:IsAllied(ply)) then
				self.Locked = true
			end
			umsg.Start( "gunvaultgui", activator );
				umsg.String( table.concat(self.Guns, ",") )
				umsg.Short( self.Entity:EntIndex() )
			umsg.End()
		else
			Notify(activator,4,3,"This gun vault is locked! use a lock pick to force it open.")
		end
	end
end

function ENT:Think()
	if (!IsValid(self.Entity:GetBWOwner())) then
		self.Entity:Remove()
	end
end

function ENT:CanDropGun(gunnum)
	local gunt = self.Guns[gunnum]
	if (gunt!=nil) then return true else return false end
end

function ENT:DropGun(gunnum, ply, hgt)
	local gunt = table.remove(self.Guns, tonumber(gunnum))
	local gun = SpawnWeapon(gunt , ply, self.Entity:GetPos()+Vector(0,0,hgt))
	gun.Ejected = true
end

function ENT:Touch(gun)
	if (gun:GetClass()=="spawned_weapon" && gun.Data != nil && !gun.Ejected  && table.Count(self.Guns)<10) then
		table.insert(self.Guns, gun.Data)
		gun.Ejected = true
		gun:Remove()
	end
end

function ENT:IsLocked()
	return self.Locked
end

function ENT:SetUnLocked()
	self.Locked = false
	return true
end

function ccWithdrawGun( ply, cmd, args )
	// in case of jackasses trying to exploit the game, or any other wierd shit that could happen.
	if (args[1]==nil || args[2] == nil) then
		return
	end
	gnum = tonumber(args[2])
	if (!ents.GetByIndex(args[1]):IsValid() || gnum>10 || gnum<1) then
		return
	end
	local vault = ents.GetByIndex(args[1])
	if (vault:GetClass()!="gunvault" || ply:GetPos():Distance(vault:GetPos())>200) then return end
	if (vault:IsLocked() && ply!=vault:GetBWOwner() && !ply:IsAllied(ent:GetBWOwner())) then return end
	if (vault:CanDropGun(gnum)) then
		vault:DropGun(gnum, ply, 25, false)
	else
		Notify(ply,4,3, "This gun does not exist!")
	end
end
concommand.Add( "withdrawgun", ccWithdrawGun );
