ENT.Type = "anim"
ENT.Base = "base_structure"
ENT.PrintName = "Supply Table"
ENT.Author = "Rickster"
ENT.Spawnable = false
ENT.AdminSpawnable = false
-- used by gamemode for power plant
ENT.PowerReq = 0
ENT.MaxUpgrade = 3
ENT.SparkPos = Vector(0,0,20)
