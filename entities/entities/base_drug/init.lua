AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:Initialize()

	self.Entity:SetModel( "models/props_lab/jar01a.mdl")
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	self.Entity:SetColor(150, 50, 50, 255)
	local phys = self.Entity:GetPhysicsObject()
	if(phys:IsValid()) then phys:Wake() end

	if (self.Owner:IsPlayer()) then
		self.Owner:GetTable().maxDrugs =self.Owner:GetTable().maxDrugs + 1
	end

	timer.Simple(60, function()
		if IsValid(self.Entity) then
			self:Remove()
		end
	end)

end

function ENT:Use(activator,caller)
	self.Entity:Remove()
end

function ENT:OnRemove()
	local effectdata = EffectData()
	effectdata:SetOrigin( self.Entity:GetPos() )
	effectdata:SetMagnitude( 2 )
	effectdata:SetScale( 2 )
	effectdata:SetRadius( 3 )
	util.Effect( "Sparks", effectdata )
	timer.Destroy(tostring(self.Entity))
	if (self.Owner != nil) then
		self.Owner:GetTable().maxDrugs = self.Owner:GetTable().maxDrugs - 1
	end
end

function ENT:IsDrug()
	return true
end
