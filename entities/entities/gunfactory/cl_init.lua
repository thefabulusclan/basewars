include('shared.lua')

function MsgGunFactory( msg )
	local upgrade = msg:ReadShort()
	local vault = msg:ReadShort()
	local inputenabled = false;
	local cancelmode = msg:ReadBool()
	if( HelpToggled or GUIToggled ) then
		inputenabled = true;
	end
	local panel = vgui.Create( "DFrame" );
	panel:SetPos( ScrW()/2-200 , ScrH() / 2 - 50 );
	panel:SetName( "Panel" );
	local ttl = "Select weapon."
	if cancelmode then
		ttl = "Cancel weapon"
	end
	panel:SetSize(160,115)
	panel:SetKeyboardInputEnabled( false );
	panel:SetMouseInputEnabled( true );
	panel:SetVisible( true );
	panel:MakePopup()
	if !cancelmode then
		laser = vgui.Create( "DButton", panel);
		laser:SetPos( 15, 35 );
		laser:SetSize( 130, 14 );
		laser:SetText( "Harpoon" );
		laser.DoClick = function(msg) LocalPlayer():ConCommand( "setgunfactoryweapon " .. vault .. " harpoon\n" ); panel:Close() end
		laser:SetVisible( true );

		lrifle = vgui.Create( "DButton", panel);
		lrifle:SetPos( 15, 50 );
		lrifle:SetSize( 130, 14 );
		lrifle:SetText( "Vollmer HMG" );
		lrifle.DoClick = function(msg) LocalPlayer():ConCommand( "setgunfactoryweapon " .. vault .. " fas2_vollmer\n" ); panel:Close() end
		lrifle:SetVisible( true );

		if upgrade>=1 then
			glauncher = vgui.Create( "DButton", panel);
			glauncher:SetPos( 15, 65 );
			glauncher:SetSize( 130, 14 );
			glauncher:SetText( "Grenade Launcher" );
			glauncher.DoClick = function(msg) LocalPlayer():ConCommand( "setgunfactoryweapon " .. vault .. " m9k_ex41\n" ); panel:Close() end
			glauncher:SetVisible( true );

			ar2 = vgui.Create( "DButton", panel);
			ar2:SetPos( 15, 80 );
			ar2:SetSize( 130, 14 );
			ar2:SetText( "M82 Rifle" );
			ar2.DoClick = function(msg) LocalPlayer():ConCommand( "setgunfactoryweapon " .. vault .. " rifle\n" ); panel:Close() end
			ar2:SetVisible( true );
		end

		if upgrade>=2 then
			wslayer = vgui.Create( "DButton", panel);
			wslayer:SetPos( 15, 95 );
			wslayer:SetSize( 130, 14 );
			wslayer:SetText( "Rocket Launcher" );
			wslayer.DoClick = function(msg) LocalPlayer():ConCommand( "setgunfactoryweapon " .. vault .. " m202\n" ); panel:Close() end
			wslayer:SetVisible( true );
		end
	else
		cancel = vgui.Create( "DButton", panel);
		cancel:SetPos( 15, 35 );
		cancel:SetSize( 130, 14 );
		cancel:SetText( "Cancel" );
		cancel.DoClick = function(msg) LocalPlayer():ConCommand( "setgunfactoryweapon " .. vault .. " resetbutton\n" ); panel:Close() end
		cancel:SetVisible( true );
	end
end
usermessage.Hook( "gunfactorygui", MsgGunFactory );

function MsgGunFactoryGet( msg )
	local time = msg:ReadFloat()
	local ent = msg:ReadEntity()
	if IsValid(ent) then
		ent:GetTable().TimeToFinish = time
	end
end
usermessage.Hook( "gunfactoryget", MsgGunFactoryGet );
