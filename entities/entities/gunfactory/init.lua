AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:BWInit()

	self.Entity:SetModel( "models/props_lab/crematorcase.mdl")
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	local phys = self.Entity:GetPhysicsObject()
	if(phys:IsValid()) then phys:Wake() end
	self.Entity:SetNWBool("sparking",false)
	self.Entity:SetMaxHealth(200)
	self.Entity:SetHealth(200)
	local ply = self.Entity:GetBWOwner()
	self.Ready = true
	self.Entity:SetNWInt("power",0)
	self.GunType = nil
	self.LastUsed = CurTime()
	self.User = self.Entity:GetBWOwner()
end

function ENT:Use( activator )
	if self.LastUsed>CurTime() then
		self.LastUsed = CurTime()+0.3
		return
	end
	self.LastUsed = CurTime()+0.3
	local ready = false
	if !self.Ready then
		ready = true
	end
	umsg.Start( "gunfactorygui", activator );
		umsg.Short( self.Entity:GetUpgrade() )
		umsg.Short( self.Entity:EntIndex() )
		umsg.Bool(ready)
	umsg.End()
end

function ENT:CanProduce(guntype, ply)
	if guntype=="resetbutton" && ply == self.Entity:GetBWOwner() then
		return true
	elseif !self.Entity:IsPowered() || !self.Ready then
		return false
	elseif guntype=="harpoon" then
		return true
	elseif guntype=="rifle" then
		return true
	elseif guntype=="m9k_ex41" && self.Entity:GetUpgrade()>=1 then
		return true
	elseif guntype=="fas2_vollmer" && self.Entity:GetUpgrade()>=1 then
		return true
	elseif guntype=="m202" && self.Entity:GetUpgrade()>=2 then
		return true
	else
		return true
	end
end

function ENT:StartProduction(ply,guntype)
	local time = 120
	if guntype=="m9k_ex41" then time=180 elseif guntype=="m202" then time = 600 elseif guntype=="fas2_vollmer" then time = 240 end
	self.User = ply
	if guntype=="resetbutton" then
		self.Entity:SetNWBool("sparking",false)
		timer.Destroy( tostring(self.Entity) .. "spawned_weapon")
		timer.Destroy( tostring(self.Entity) .. "dostuff")
		self.Ready = true
		self.GunType = nil
		Notify(ply, 2, 3, "Gun production stopped." )
		umsg.Start("gunfactoryget",ply)
			umsg.Float(0)
			umsg.Entity(self.Entity)
		umsg.End()
	else
		self.Entity:SetNWBool("sparking",true)
		timer.Create( tostring(self.Entity) .. "spawned_weapon", time, 1, function() self:createGun() end)
		timer.Create( tostring(self.Entity) .. "dostuff", 60, time/60, function() self:DoStuff() end)
		self.Ready = false
		self.GunType = guntype
		Notify(ply, 2, 3, "Producing a " .. self.GunType )
		umsg.Start("gunfactoryget",ply)
			umsg.Float(time+CurTime())
			umsg.Entity(self.Entity)
		umsg.End()
	end
end

function ENT:DoStuff()
	local owner = self.User
	local productioncost = 150
	if owner != self.Entity:GetBWOwner() then
		productioncost = 250
	end
	if !self.Entity:IsPowered() then
		Notify(owner, 4, 3, "Failed to create weapon. Low power")
		self.Entity:SetNWBool("sparking",false)
		timer.Destroy(tostring(self.Entity) .. "dostuff")
		timer.Destroy(tostring(self.Entity) .. "spawned_weapon")
		self.Ready = true
		umsg.Start("gunfactoryget",ply)
			umsg.Float(time+CurTime())
			umsg.Entity(self.Entity)
		umsg.End()
		return
	end
	if !owner:CanAfford(productioncost) then
		Notify(owner, 4, 3, "Failed to create weapon. Not enough money")
		self.Entity:SetNWBool("sparking",false)
		timer.Destroy(tostring(self.Entity) .. "dostuff")
		timer.Destroy(tostring(self.Entity) .. "spawned_weapon")
		self.Ready = true
		umsg.Start("gunfactoryget",ply)
			umsg.Float(time+CurTime())
			umsg.Entity(self.Entity)
		umsg.End()
		return
	end
	owner:AddMoney(-productioncost)
	Notify(owner,3,3, "Used $"..tostring(productioncost).." creating a weapon in gun factory")
end

function ENT:createGun()
	self.Entity:SetNWBool("sparking",false)
	self.Ready = true
	local gun = ents.Create("spawned_weapon")
	if self.GunType=="resetbutton" then
		return
	elseif self.GunType=="harpoon" then
		gun:SetModel( "models/weapons/w_harpooner.mdl" );
		gun:SetNWString("weaponclass", "m9k_harpoon")
	elseif self.GunType=="rifle" then
		gun:SetModel("models/weapons/w_snip_sg550.mdl")
		gun:SetNWString("weaponclass","fas2_m82")
	elseif self.GunType=="m202" then
		gun:SetModel("models/weapons/w_rocket_launcher.mdl")
		gun:SetNWString("weaponclass","m9k_m202")
	elseif self.GunType=="m9k_ex41" then
		gun:SetModel("models/weapons/w_ex41.mdl")
		gun:SetNWString("weaponclass","m9k_ex41")
	elseif self.GunType=="fas2_vollmer" then
		gun:SetModel("models/weapons/w_irifle.mdl")
		gun:SetNWString("weaponclass","fas2_vollmer")
	end
	local trace = { }
		trace.start = self.Entity:GetPos()+self.Entity:GetAngles():Up()*15;
		trace.endpos = trace.start + self.Entity:GetAngles():Forward()*-30 + self.Entity:GetAngles():Right()*-30 + self.Entity:GetAngles():Up()*60
		trace.filter = self.Entity
	local tr = util.TraceLine( trace );
	gun:SetPos(tr.HitPos)
	local ang = self.Entity:GetAngles()
	ang:RotateAroundAxis(ang:Up(), 90)
	gun:SetAngles(ang)
	gun.Owner = self.User
	gun:Spawn()
	Notify(self.User,3,3,"Gun Factory weapon production complete.")
end

function ENT:Think()
	if (IsValid(self.Entity:GetBWOwner())==false) then
		self.Entity:Remove()
	end
	if(self.Entity:GetNWBool("sparking") == true) then
		local ang = self.Entity:GetAngles()
	end

end

function ENT:BWRemove( )
	timer.Destroy(tostring(self.Entity) .. "spawned_weapon")
	timer.Destroy(tostring(self.Entity) .. "dostuff")
end


function ccSetWeapon( ply, cmd, args )
	if (args[1]==nil || args[2] == nil) then
		return
	end

    local trace = { }
	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 150;
	trace.filter = ply;

	guntype = tostring(args[2])
	local tr = util.TraceLine( trace );
	if (!IsValid(tr.Entity)) then
		return
	end
	local targent = tr.Entity
    if !IsValid(ents.GetByIndex(args[1])) || (guntype!="harpoon" && guntype!="rifle" && guntype!="m9k_ex41" && guntype!="fas2_vollmer" && guntype!="m202" && guntype!="resetbutton") then
		return
	end
	local vault = ents.GetByIndex(args[1])
    if (targent:GetClass()!="gunfactory" || ply:GetPos():Distance(targent:GetPos())>300) then return end
	if (targent:CanProduce(guntype, ply)) then
		targent:StartProduction(ply,guntype)
	else
		if guntype=="resetbutton" then
			Notify(ply, 0, 3,"Only Gun Factory owner can cancel weapon production." );
		else
			Notify( ply, 0, 3, "Cant make weapon." );
		end
	end
end

concommand.Add( "setgunfactoryweapon", ccSetWeapon );
