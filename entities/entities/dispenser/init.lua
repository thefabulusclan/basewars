// copypasta from microwave.

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:SpawnFunction( ply, tr )

	if ( !tr.Hit ) then return end

	local SpawnPos = tr.HitPos + tr.HitNormal * 42

	local ent = ents.Create( "dispenser" )
	ent:SetPos( SpawnPos )
	ent:Spawn()
	ent:Activate()
	return ent

end

function ENT:BWInit()

	self.Entity:SetModel( "models/props_lab/reciever_cart.mdl")
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	local phys = self.Entity:GetPhysicsObject()
	if(phys:IsValid()) then phys:Wake() end
	self.Entity:SetNWBool("sparking",false)
	self.Entity:SetMaxHealth(500)
	self.Entity:SetHealth(500)
	local ply = self.Entity:GetBWOwner()
	self.Entity:SetNWInt("power",0)
	self.scrap = false
end

function ENT:Use(activator,caller)
	if self.Entity:GetNWBool("sparking") == true || self.Entity:GetPos():Distance(activator:GetPos()) > 150 then return end
	plgun = activator:GetActiveWeapon()
	if activator:Health()<activator:GetMaxHealth() then
		activator:SetHealth(activator:Health()+15)
		if (activator:Health()>activator:GetMaxHealth()) then activator:SetHealth(activator:GetMaxHealth()) end
	end
	// if they have rocket launcher swep, and no ammo, give them ammo so they can select it.
	self.Entity:SetNWBool("sparking",true)
	if (self.Entity:GetUpgrade()>0) then
		timer.Create( tostring(self.Entity) .. "resup", 0.75, 1, function() self:resupply() end)
	else
		timer.Create( tostring(self.Entity) .. "resup", 1, 1, function() self:resupply() end)
	end
	if (activator:GetAmmoCount("rpg_round")<=0 && activator:HasWeapon("weapon_rocketlauncher")) then
		activator:GiveAmmo(2, "rpg_round")
		return "" ;
	end
	if (activator:GetAmmoCount("xbowbolt")<=0 && activator:HasWeapon("weapon_tranqgun")) then
		activator:GiveAmmo(5, "xbowbolt")
		return "" ;
	end
	if (plgun:GetClass() == "weapon_rpg") then
		activator:GiveAmmo(2, plgun:GetPrimaryAmmoType())
	elseif (plgun:Clip1()>0) then
		activator:GiveAmmo(plgun:Clip1()*2, plgun:GetPrimaryAmmoType())
	end
	if (self.Entity:GetUpgrade()==2) then
		activator:SetArmor(activator:Armor()+2)
		if (activator:Armor()>100) then activator:SetArmor(100) end
	end
end

function ENT:resupply()
	self.Entity:SetNWBool("sparking",false)
end

function ENT:Think()
	if (IsValid(self.Entity:GetBWOwner())!=true) then
		self.Entity:Remove()
	end
end

function ENT:BWRemove( )
	local ply = self.Entity:GetBWOwner()
	if IsValid(ply) then
	end
	timer.Destroy(tostring(self.Entity) .. "resup")
end
