AddCSLuaFile "cl_init.lua";
AddCSLuaFile "shared.lua";
include "shared.lua";

function ENT:UpdateMoney( )
	if self:IsValid( ) then
		self.Entity:GetBWOwner():SetNWString( "vaultamount", self.Money );
	end;
end;

local called = false;
function ENT:BWInit( )
	self:SetModel( "models/props_lab/powerbox01a.mdl" );
	self:PhysicsInit( SOLID_VPHYSICS );
	self:SetMoveType( MOVETYPE_VPHYSICS );
	self:SetSolid( SOLID_VPHYSICS );
	self:EmitSound( "ambient/machines/thumper_startup1.wav" );

	local phys = self:GetPhysicsObject( );
	if( phys:IsValid( ) ) then
		phys:Wake( );
	end;

	self.Entity:SetMaxHealth(1000)
	self.Entity:SetHealth(1000)

	self.Money = 0;
	self.KeyPress = CurTime( );
end;

function ENT:Use( activator, caller )
	if CurTime( ) >= self.KeyPress then
		if self.Entity:GetBWOwner() == activator || activator:IsUserGroup("owner") || activator:IsUserGroup("dev") then
			umsg.Start( "MoneyVaultMenu", activator )
				umsg.Entity(self.Entity:GetBWOwner())
			umsg.End( );
			self.KeyPress = CurTime( ) + 5;
		else
			self.KeyPress = CurTime( ) + 5;
			activator:ChatPrint( "That isn't your Money Vault!" );
		end;
	end;
end;

function ENT:EjectMoney( )
	if self.Money > 0 then
		local moneybag = ents.Create( "prop_moneybag" );
      moneybag:SetModel( "models/props/cs_assault/Money.mdl" );
			moneybag:SetPos( self:GetPos( ) + Vector( 0, 0, 20 ) );
			moneybag:SetAngles( self:GetAngles( ) );
			moneybag:SetColor( Color(200, 255, 200, 255 ))
			moneybag:Spawn( );
			moneybag:GetTable( ).MoneyBag = true;
			moneybag:GetTable( ).Amount = self.Money;
			moneybag:SetVelocity( Vector( 0, 0, 10 ) * 10 );
			moneybag.Ejected = true;

			self.Money = 0;
			self:UpdateMoney( );
	else
		Notify(self.Entity:GetBWOwner(), 4, 3, "This Money Vault is empty!")
		end;
end;

function ENT:BWRemove( )
	self:EjectMoney( );
	self:EmitSound( "ambient/machines/thumper_shutdown1.wav" );
end;

function ENT:Touch( ent )
	if ent:GetClass( ) == "prop_moneybag" then
		if not ent.Ejected and not called then
			self.Money = self.Money + ent:GetTable( ).Amount;
			ent:Remove( );
			self:UpdateMoney( );
			called = true;
			timer.Simple( 0.5, function( )
				called = false;
			end );
		end;
	end;
end;


function ll_vault_eject( ply, cmd, argv )
	local trace = { }
		trace.start = ply:GetShootPos( )
		trace.endpos = ply:GetShootPos( ) + ( ply:GetAimVector( ) * 200 )
		trace.filter = ply
		local traceline = util.TraceLine( trace )
		if traceline.HitNonWorld and traceline.Entity:IsValid( ) then
			traceline.Entity:EjectMoney( )
			traceline.Entity:EmitSound( "ambient/alarms/klaxon1.wav" )
		end;
end
concommand.Add( "ll_vault_eject", ll_vault_eject )

function ll_vault_deposit( ply, cmd, args )
	if !args[1] or tonumber(args[1]) == 0 then Notify( ply, 4, 3, "Please type a number!") return end
	local trace = { }
		trace.start = ply:GetShootPos( )
		trace.endpos = ply:GetShootPos( ) + ( ply:GetAimVector( ) * 200 )
		trace.filter = ply
		local traceline = util.TraceLine( trace )
		if !ply:CanAfford(tonumber(args[1])) then Notify(ply, 4, 3, "You cannot afford this!") return end
		if traceline.HitNonWorld and traceline.Entity:IsValid( ) then
			ply:AddMoney(-tonumber(args[1]))
			Notify(ply, 0, 3, "You deposited $" .. args[1] .."!")
			traceline.Entity.Money = traceline.Entity.Money + tonumber(args[1])
			traceline.Entity:UpdateMoney()
		end
end
concommand.Add( "ll_vault_deposit", ll_vault_deposit )
