include "shared.lua";

local DFrame1;
local DPanel1;
local DLabel1;
local DLabel2;
local DButton1;
local DTextEntry1;
local DButton2;

local function OpenMenu( msg )
	owner = msg:ReadEntity()
	DFrame1 = vgui.Create( "DFrame" );
	DFrame1:SetSize( 201, 126 );
	DFrame1:SetPos( ScrW()/2-100, ScrH()/2-63 );
	DFrame1:SetTitle( "Money Vault" );
	DFrame1:SetSizable( true );
	DFrame1:SetDeleteOnClose( false );
	DFrame1:MakePopup( );

	DPanel1 = vgui.Create( "DPanel", DFrame1 );
	DPanel1:SetSize( 193, 95 );
	DPanel1:SetPos( 4, 27 );

	DButton1 = vgui.Create( "DButton", DPanel1 );
	DButton1:SetSize( 185, 51 );
	DButton1:SetPos( 4, 40 );
	DButton1:SetText( "Eject" );
	DButton1.DoClick = function( )
    RunConsoleCommand( "ll_vault_eject" );
	DFrame1:Close()
  end

	DLabel2 = vgui.Create( "DLabel", DPanel1 )
	DLabel2:SetPos( 58, 14 )
	DLabel2:SetTextColor(Color(0,0,0,255))
	DLabel2:SetText( owner:GetNWString( "vaultamount", "0" ) )
	DLabel2:SizeToContents( )

	DLabel1 = vgui.Create( "DLabel", DPanel1 )
	DLabel1:SetTextColor( Color( 10, 135, 10, 255 ) )
	DLabel1:SetPos( 8, 14 )
	DLabel1:SetText( "Contents: " )
	DLabel1:SizeToContents( )
	surface.PlaySound "items/ammocrate_open.wav";
end;
usermessage.Hook( "MoneyVaultMenu", OpenMenu );

hook.Add( "Think", "MoneyVaultUpdate", function( )
	if DLabel2 then
		DLabel2:SetText( owner:GetNWInt( "vaultamount", 0 ) );
	end;
end );