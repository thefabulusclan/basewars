ENT.Type = "anim"
ENT.Base = "base_structure"
ENT.PrintName = "Power Plant"
ENT.Author = "HLTV Proxy"
ENT.Spawnable = false
ENT.AdminSpawnable = false

ENT.MaxUpgrade = 5

function ENT:MaxSlots()
  return 5 + 9 * self.Entity:GetUpgrade()
end

function ENT:SetupBWTables()
  self:NetworkVar("Int", 2, "TakenSockets")
  if SERVER then
    self.Entity:SetTakenSockets(0)
  end
end
