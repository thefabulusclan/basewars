-- ============================================
-- =                                          =
-- =          Crate SENT by Mahalis           =
-- =                                          =
-- ============================================

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:BWPrePhys()
	self.Entity:SetModel( "models/props_wasteland/laundry_washer003.mdl")
end

function ENT:BWInit()
	self.Entity:SetMaxHealth(300)
	self.Entity:SetHealth(300)
	local ply = self.Entity:GetBWOwner()
	self.Entity.Inactive = false
	self.Powdist=1024
	self.Entity.sockets = {}
	for i=1, self:MaxSlots() do
		self.Entity.sockets[i] = 0
	end

	timer.Create( tostring(self.Entity), 60, 0, function() self:giveMoney() end)
	timer.Create( tostring(self.Entity) .. "fuckafkfags", 1200, 1, function() self:shutOff() end)
	timer.Create( tostring(self.Entity) .. "notifyoff", 1080, 1, function() self:notifypl() end)
end

function ENT:giveMoney()
	local ply = self.Entity:GetBWOwner()
	if(IsValid(ply) && !self.Entity.Inactive) then
		if ply:CanAfford(5) then
			ply:AddMoney( -5 );
			Notify( ply, 2, 3, "$5 spent to keep Generator running." );
		else
			Notify( ply, 4, 3, "Generator has shut off from lack of money" )
			self.Entity:shutOff()
		end
	elseif (self.Entity.Inactive) then
		Notify( ply, 4, 3, "A Generator is inactive, press use on it to make it active again." );
	end
end

function ENT:shutOff()
	local ply = self.Entity:GetBWOwner()
	self.Entity.Inactive = true
	Notify( ply, 1, 3, "NOTICE: A GENERATOR HAS GONE INACTIVE" );
	Notify( ply, 1, 3, "PRESS USE ON IT TO MAKE IT WORK AGAIN" );
	self.Entity:SetColor(Color(255,0,0,255))
end

function ENT:notifypl()
	local ply = self.Entity:GetBWOwner()
	Notify( ply, 4, 3, "NOTICE: A GENERATOR IS ABOUT TO GO INACTIVE" );
	Notify( ply, 4, 3, "PRESS USE ON IT TO KEEP IT WORKING" );
	self.Entity:SetColor(Color(255,150,150,255))
end

function ENT:Use(activator,caller)
	if activator:CanAfford(5) && activator == self.Entity:GetBWOwner() then
		timer.Start( tostring(self.Entity) .. "fuckafkfags")
		timer.Start( tostring(self.Entity) .. "notifyoff")
		self.Entity.Inactive = false
		self.Entity:SetColor(Color(255,255,255,255))
	end
end

function ENT:Think()
	if (!IsValid(self.Entity:GetBWOwner())) then
		self.Entity:Remove()
	end
	self.Entity:UpdateSockets()
	self.Entity:NextThink(CurTime()+1)
	return true
end

function ENT:BWRemove( )
	self.Entity:ClearSockets()
	timer.Destroy(tostring(self.Entity))
	timer.Destroy(tostring(self.Entity) .. "fuckafkfags")
	timer.Destroy(tostring(self.Entity) .. "notifyoff")
end

function ENT:UpdateSockets()
	if self.Entity.Inactive then
		self.Entity:ClearSockets()
	else
		for i=1, self:MaxSlots() do
			if self.Entity.sockets[i] == nil then self.Entity.sockets[i] = 0 end
			local slotent = ents.GetByIndex(self.Entity.sockets[i])
			if !IsValid(slotent) then
				self.Entity:Socket(i, self.Entity:FindStructure())
			elseif IsValid(slotent) && slotent:GetPos():Distance(self.Entity:GetPos()) > self.Powdist then
				self.Entity:UnSocket(i)
			end
		end
	end
end

function ENT:FindStructure()
	local closest = nil
	for k, v in pairs( ents.FindInSphere(self.Entity:GetPos(), self.Powdist)) do
		if v:GetTable().Structure && !v:IsPowered() then
			if v:GetOwner() == self.Entity:GetOwner() then return v end
			closest = v
		end
	end
	return closest
end

function ENT:ClearSockets()
	for i=1,self:MaxSlots() do
		if IsValid(ents.GetByIndex(self.Entity.sockets[i])) then
			self.Entity:UnSocket(i)
		end
	end
end

function ENT:UnSocket(i)
	local ent = ents.GetByIndex(self.Entity.sockets[i])
	if IsValid(ent) && ent:GetPower() > 0 then
		ent:SetPower(ent:GetPower() - 1)
	end
	self.Entity:SetTakenSockets(self.Entity:GetTakenSockets() - 1)
	self.Entity.sockets[i] = 0
end

function ENT:Socket(i, ent)
	if self.Entity.sockets[i] != 0 then
		self.Entity:SetTakenSockets(self.Entity:GetTakenSockets() - 1)
		self.Entity.sockets[i] = 0
	end
	if IsValid(ent) && ent:GetPower() < ent.PowerReq then
		ent:SetPower(ent:GetPower() + 1)
		self.Entity.sockets[i] = ent:EntIndex()
		self.Entity:SetTakenSockets(self.Entity:GetTakenSockets() + 1)
	end
end
