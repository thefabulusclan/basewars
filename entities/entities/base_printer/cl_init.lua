/******************************************
	Base Printer
		Designed By Oggal
			for use of TFC BaseWars Servers
			Continued  Deveolpment for AVG
*******************************************/
include('shared.lua')


function ENT:PrinterMenu()
	local totalValue = "test"
	local PrintPanel = vgui.Create( "DFrame" )
	PrintPanel:SetPos( 100, 100 )
	PrintPanel:SetSize( 300, 200 )
	PrintPanel:SetTitle( self.Entity:GetNWString("PrintName","Printer").."    Level: ".. self.Entity:GetUpgrade() )
	PrintPanel:SetDraggable( true )
	PrintPanel:MakePopup()

	local SellButton = vgui.Create("DButton",PrintPanel)
	SellButton:SetText("Refresh")
	SellButton:SetSize(50,20)
	SellButton:SetPos(10,170)


	local UpgradeButton = vgui.Create("DButton",PrintPanel)
	UpgradeButton:SetText("Upgrade")
	UpgradeButton:SetSize(50,20)
	UpgradeButton:SetPos(70,170)
	UpgradeButton:SetEnabled(self.Entity:GetUpgrade()<self.MaxUpgrade)

	local RestockButton = vgui.Create("DButton",PrintPanel)
	RestockButton:SetText("Restock")
	RestockButton:SetSize(50,20)
	RestockButton:SetPos(130,170)
	RestockButton:SetEnabled(self.Entity:GetNWBool("isLow"))

	local PrinterInfo = vgui.Create("DPanel",PrintPanel)
	PrinterInfo:SetSize(280, 130)
	PrinterInfo:SetPos(10,30)

	local ValueText = vgui.Create("DLabel" ,PrinterInfo)
	ValueText:SetText("Printer Cost:" .. string.Comma(tonumber(self.Entity:GetNWString("printercost"))*math.pow(2,self.Entity:GetUpgrade())))
	ValueText:SetTextColor(Color(255,0,0))
	ValueText:SetSize(200,20)
	ValueText:SetPos(20,30)

	local ExspenceText = vgui.Create("DLabel" ,PrinterInfo)
	ExspenceText:SetText("Total Upkeep:" ..string.Comma(tonumber(self.Entity:GetNWString("totalUpkeep"))))
	ExspenceText:SetTextColor(Color(255,0,0))
	ExspenceText:SetSize(200,20)
	ExspenceText:SetPos(20,50)

	local CostText = vgui.Create("DLabel" ,PrinterInfo)
	CostText:SetText("Total Cost:" .. string.Comma((tonumber(self.Entity:GetNWString("totalUpkeep")))+(tonumber(self.Entity:GetNWString("printercost"))*math.pow(2,self.Entity:GetUpgrade()))))
	CostText:SetTextColor(Color(255,0,0))
	CostText:SetSize(200,20)
	CostText:SetPos(10,10)

	local PrintText = vgui.Create("DLabel" ,PrinterInfo)
	PrintText:SetText("Total Output:" .. string.Comma(tonumber(self.Entity:GetNWString("totalPrint"))).."")
	PrintText:SetTextColor(Color(0,255,0))
	PrintText:SetSize(200,20)
	PrintText:SetPos(10,70)

	local PriceText = vgui.Create("DLabel" ,PrinterInfo)
	PriceText:SetText("Profit:" .. string.Comma((tonumber(self.Entity:GetNWString("totalPrint"))-(tonumber(self.Entity:GetNWString("totalUpkeep"))+(tonumber(self.Entity:GetNWString("printercost"))*math.pow(2,self.Entity:GetUpgrade()))))))
	if( 0 >tonumber(self.Entity:GetNWString("totalPrint"))-(tonumber(self.Entity:GetNWString("totalUpkeep"))+(tonumber(self.Entity:GetNWString("printercost"))*math.pow(2,self.Entity:GetUpgrade())))) then
		PriceText:SetTextColor(Color(255,0,0))
	else
		PriceText:SetTextColor(Color(0,255,0))
	end
	PriceText:SetSize(200,20)
	PriceText:SetPos(10,100)


	local function updateScreen()
		if( 0 >tonumber(self.Entity:GetNWString("totalPrint"))-(tonumber(self.Entity:GetNWString("totalUpkeep"))+(tonumber(self.Entity:GetNWString("printercost"))*math.pow(2,self.Entity:GetUpgrade())))) then
			PriceText:SetTextColor(Color(255,0,0))
		else
			PriceText:SetTextColor(Color(0,255,0))
		end
		PriceText:SetText("Profit:" .. string.Comma((tonumber(self.Entity:GetNWString("totalPrint"))-(tonumber(self.Entity:GetNWString("totalUpkeep"))+(tonumber(self.Entity:GetNWString("printercost"))*math.pow(2,self.Entity:GetUpgrade()))))))
		ExspenceText:SetText("Total Upkeep:" ..string.Comma(tonumber(self.Entity:GetNWString("totalUpkeep"))))
		PrintPanel:SetTitle( self.Entity:GetNWString("PrintName","Printer").."    Level: ".. self.Entity:GetUpgrade() )
		ValueText:SetText("Printer Cost:" .. string.Comma(tonumber(self.Entity:GetNWString("printercost"))*math.pow(2,self.Entity:GetUpgrade())))
		PrintText:SetText("Total Output:" .. string.Comma(tonumber(self.Entity:GetNWString("totalPrint"))).."")
		CostText:SetText("Total Cost:" .. string.Comma((tonumber(self.Entity:GetNWString("totalUpkeep")))+(tonumber(self.Entity:GetNWString("printercost"))*math.pow(2,self.Entity:GetUpgrade()))))

		RestockButton:SetEnabled(self.Entity:GetNWBool("isLow"))
		UpgradeButton:SetEnabled(self.Entity:GetUpgrade()<self.MaxUpgrade)
	end



	UpgradeButton.DoClick = function()
		LocalPlayer():ConCommand("bw_upgrade " .. self.Entity:EntIndex())
		timer.Simple(0.1,updateScreen)
	end

	SellButton.DoClick = function()
		timer.Simple(0.1,updateScreen)
	end

	RestockButton.DoClick = function()
		LocalPlayer():ConCommand("ResuplyPrinter " .. self.Entity:EntIndex())
		timer.Simple(0.1,updateScreen)
	end

end

function ENT:Draw( bDontDrawModel )
	self.Entity:DrawModel()
	local Pos = self:GetPos()
	local Ang = self:GetAngles()
	local maxhealth = self.Entity:GetMaxHealth()

	local owner = self.Entity:GetBWOwner()
	owner = (IsValid(owner) and owner:Nick()) or "unknown"
	local upgrade = self.Entity:GetUpgrade()

	surface.SetFont("HUDNumber5")
	local TextWidth = surface.GetTextSize(self.Entity:GetNWString("PrintName"))
	local TextWidth2 = surface.GetTextSize("Level:"..upgrade)

	Ang:RotateAroundAxis(Ang:Up(), 90)

	local Health = self.Entity:Health()
	local DamageColorScale = Color(0, 159, 107, 155)

	if Health < maxhealth/2 then
		DamageColorScale = Color(255, 205, 0, 155)
	end
		if Health < maxhealth/4 then
		DamageColorScale = Color(180, 0, 0, 100)
	end
		--cam.IgnoreZ( true )
	cam.Start3D2D(Pos + Ang:Up() * 5, Ang, 0.1)
		draw.WordBox(.75, -TextWidth*0.5, -70, self.Entity:GetNWString("PrintName"), "HUDNumber5", Color(140, 120, 83, 155), Color(255,255,255,255))
		draw.WordBox(.75, -TextWidth2*0.5, -24, "Level:"..upgrade, "HUDNumber5", Color(140, 120, 83, 155), Color(255,255,255,255))
		draw.RoundedBox(5, -TextWidth2 * 0.7, 22, (Health/maxhealth)*(TextWidth2*1.4), TextWidth2 * 0.3, DamageColorScale)
	cam.End3D2D()
		--cam.IgnoreZ( false )
end


usermessage.Hook("PrinterMenu",function( msg )
	local E = msg:ReadShort()
	ents.GetByIndex(E):PrinterMenu()
end)
