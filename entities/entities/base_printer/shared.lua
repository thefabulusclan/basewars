/******************************************
	Base Printer
		Designed By Oggal
			for use of TFC BaseWars Servers
			Continued  Deveolpment for AVG
*******************************************/
ENT.Type 		= "anim"
ENT.Base 		= "base_structure"

ENT.PrintName	= "Base Printer"
ENT.Author		= "Dadadah and Oggal"
ENT.Contact		= ""

ENT.Spawnable		= false
ENT.AdminSpawnable	= false

ENT.PowerReq = nil
ENT.MaxUpgrade = 6

function ENT:SetupBWTables()
  self:NetworkVar("Int", 2, "PowerReq")
end
