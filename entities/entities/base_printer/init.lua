/******************************************
	Base Printer
		Designed By Oggal
			for use of TFC BaseWars Servers
			Continued  Deveolpment for AVG
*******************************************/

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:BaseInitialize(str_model, int_printer_cost, int_printer_number, str_printer_name, int_printer_health)
	if str_model!= nil then
		self.Entity:SetModel( str_model )
	end

	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	local phys = self.Entity:GetPhysicsObject()
	if(phys:IsValid()) then phys:Wake() end

	local ply = self.Entity:GetBWOwner()

	self.lastPrint = CurTime()
	self.PrintCount = 0
	self.NearInact = 9

	self.Entity.Inactive = false
	self.Entity.printNum = int_printer_number
	self.Entity.ResuplyValue = int_printer_cost/20
	self.Entity.PrinterCost = int_printer_cost
	self.PowerReq = int_printer_number
	self:SetPowerReq(int_printer_number)
	self.Entity.FINE_PrintTime = math.Round(20*(int_printer_number*1.5))
	self.Entity.color = self.Entity:GetColor()

	self.Entity:SetNWBool("sparking",false)
	self.Entity:SetNWBool("isLow",false)
	self.Entity:SetMaxHealth(int_printer_health)
	self.Entity:SetHealth(int_printer_health)
	self.Entity:SetNWString("totalPrint",0)
	self.Entity:SetNWString("totalUpkeep",0)
	self.Entity:SetNWString("printercost",int_printer_cost)
	self.Entity:SetNWString("PrintName",str_printer_name)
	self.PrintName = str_printer_name
	self.Entity:SetMaterial( "models/shiny" )
	self.Payout = {int_printer_cost, str_printer_name}
	self.LastUsed = CurTime()+0.3
end

function ENT:giveMoney()
	local ply = self.Entity:GetBWOwner()
	local found = false
	local deposit
	for k,v in pairs(ents.FindInSphere(self.Entity:GetPos(), 200)) do
		if (v:GetClass() == "moneyvault" || v:GetClass() == "bank") then
			if ply == v:GetBWOwner() then
				found = true
				deposit = v
			end
		end
	end
	if(IsValid(ply) && !self.Entity.Inactive && self.Entity:IsPowered() && found == true) then
        local level = self.Entity:GetUpgrade()
        local amount = math.Round((self.Entity.PrinterCost/30) + (RetrieveBenifits(self.Entity:GetBWOwner())*self.Entity.PrinterCost)/100)
		local printValue = tonumber(self.Entity:GetNWString("totalPrint"))
		amount = amount* (2^(level*0.99))
		amount = math.Round(amount)

		deposit:GetTable().Money = deposit:GetTable().Money + amount
		deposit:UpdateMoney()
		self.PrintCount = self.PrintCount+1;

		self.Entity:SetNWString("totalPrint",""..(printValue+amount))
		NotifyPrinter( ply, self.Entity.printNum, 0, amount )

		if(self.PrintCount> self.NearInact) then self.Entity:notifypl() end
	elseif (self.Entity.Inactive) then
		NotifyPrinter( ply, self.Entity.printNum, 1, 0)
	elseif !self.Entity:IsPowered() then
		NotifyPrinter(ply, self.Entity.printNum, 2, 0)
	elseif found == false then
		NotifyPrinter(ply, self.Entity.printNum, 3, 0)
	end
end

function ENT:Think()
	if (!IsValid(self.Entity:GetBWOwner())) then
		self.Entity:Remove()
	end
	if(self.lastPrint+self.Entity.FINE_PrintTime< CurTime()) then self.Entity:giveMoney() self.lastPrint = CurTime() end
end

function ENT:shutOff()
		self.Entity.Inactive = true
		NotifyPrinter( self.Entity:GetBWOwner(), self.Entity.printNum, 4, 0)
		self.Entity:SetColor(Color(255,0,0,254))
end

function ENT:notifypl()
		NotifyPrinter( self.Entity:GetBWOwner(), self.Entity.printNum, 5, 0)
		self.Entity:SetColor(Color(255,150,150,254))
		self.Entity:SetNWBool("isLow",true)
		local testNum = self.NearInact + math.random(1,10)
		if(self.PrintCount>testNum) then self.Entity:shutOff() end
end

function ENT:Use(activator,caller)
if(!caller:IsPlayer()) then return end
		if self.LastUsed>CurTime() then
		self.LastUsed = CurTime()+0.3
		return
	end
	self.LastUsed = CurTime()+0.3


	umsg.Start("PrinterMenu",caller)
	umsg.Short(self.Entity:EntIndex())
	umsg.End()
end

function Resuply(ply,cmd,args)
	ents.GetByIndex(args[1]):Reload(ply)
end
concommand.Add("ResuplyPrinter",Resuply)

function ENT:Reload(ply)
	if(ply != self.Entity:GetBWOwner()) then return end
	if ((self.NearInact <self.PrintCount || self.Entity.Inactive == true) && self.Entity:GetBWOwner() == ply && self.Entity:GetNWBool("sparking")==false && ply:CanAfford(self.Entity.ResuplyValue)) then
		local upkeepfee = self.Entity.ResuplyValue*(2^(self.Entity:GetUpgrade()))
		local upkeeptotal = tonumber(self.Entity:GetNWString("totalUpkeep"))
			ply:AddMoney( -1*upkeepfee)
		self.Entity:SetNWString("totalUpkeep",""..(upkeeptotal+upkeepfee))

		NotifyPrinter(self.Entity:GetBWOwner(), self.Entity.printNum, 6, 0)

		self.Entity.Inactive = false
		self.PrintCount = 0;
		self.Entity:SetNWBool("isLow",false)
		self.lastPrint = CurTime()
		self.Entity:SetColor(self.Entity.color)
	end
end
