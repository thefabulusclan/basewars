AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:BWInit()
  self.Entity:SetModel( "models/props_trainstation/trainstation_clock001.mdl")
  self.Entity:PhysicsInit(SOLID_VPHYSICS)
  self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
  self.Entity:SetSolid(SOLID_VPHYSICS)
  self.Entity:SetAngles(Angle(-90, 0, 0))
  local ply = self.Entity:GetBWOwner()
  local phys = self.Entity:GetPhysicsObject()
	phys:EnableMotion(false)
  if(phys:IsValid()) then phys:Wake() end
  self.Entity:SetMaxHealth(100)
  self.Entity:SetHealth(100)
  local spawnpointp = true
  ply:GetTable().Spawnpoint = self.Entity
	self.Entity:SetNWInt("power",0)
end

function ENT:Think( )
	if !IsValid(self.Entity:GetBWOwner()) then
		self.Entity:Remove()
	end
end
