AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:Initialize()

	if (self.Entity:GetModel() == "" || self.Entity:GetModel() == nil || self.Entity:GetModel() == "models/error.mdl") then
		self.Entity:SetModel( "models/props_lab/jar01a.mdl")
	end
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	local phys = self.Entity:GetPhysicsObject()
	if(phys:IsValid()) then phys:Wake() end
	self.used = false

	if (self.Owner != nil) then
		self.Owner:GetTable().maxDrugs = self.Owner:GetTable().maxDrugs + 1
	end

	timer.Simple(60, function()
		if IsValid(self.Entity) then
			self:Remove()
		end
	end)

	self.Entity.count = 1
	self.Entity.diddled = false

end

function ENT:Use(activator,caller)
	if IsValid( caller ) and caller:IsPlayer() then
		self.DrugEffect(caller)
		self.used = true
		self.Entity:Remove()
	end
end

// Legacy code...
function ENT:Touch(diddler)
	if diddler:GetClass() == "basewars_drug" then
		if diddler.diddled == false and self.Entity.diddled == false then
			if diddler.drugType == self.Entity.drugType then
				if diddler.drugType == "drug" || diddler.drugType == "booze" then
					diddler.diddled = true
					self.Entity.count = self.Entity.count + diddler.count
					diddler:Remove()
				end
			end
		end
	end
end

function ENT:OnRemove()
	local effectdata = EffectData()
	effectdata:SetOrigin( self.Entity:GetPos() )
	effectdata:SetMagnitude( 2 )
	effectdata:SetScale( 2 )
	effectdata:SetRadius( 3 )
	util.Effect( "Sparks", effectdata )
	timer.Destroy(tostring(self.Entity))
	if (self.Owner != nil) then
		self.Owner:GetTable().maxDrugs = self.Owner:GetTable().maxDrugs - 1
	end
end
