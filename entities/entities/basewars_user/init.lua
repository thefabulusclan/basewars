-- ============================================
-- =                                          =
-- =          Crate SENT by Mahalis           =
-- =                                          =
-- ============================================

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:BWInit()

	self.Entity:SetModel( "models/props_combine/combine_intmonitor001.mdl")
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	local phys = self.Entity:GetPhysicsObject()
	if(phys:IsValid()) then phys:Wake() end
	self.Entity:SetMaxHealth(300)
	self.Entity:SetHealth(300)
	local ply = self.Entity:GetBWOwner()
	self.Entity.Inactive = false
	timer.Create( tostring(self.Entity), 600, 0, function() self:giveMoney() end)
	timer.Create( tostring(self.Entity) .. "fuckafkfags", 1800, 1, function() self:shutOff() end)
	timer.Create( tostring(self.Entity) .. "notifyoff", 1580, 1, function() self:notifypl() end)
end

function ENT:giveMoney()
	local ply = self.Entity:GetBWOwner()
	if(IsValid(ply) && !self.Entity.Inactive) then
		if ply:CanAfford(5000) then
			ply:AddMoney( -5000 );
			Notify( ply, 2, 3, "$5000 spent to keep Auto-Refill running." );
		else
			Notify( ply, 4, 3, "Auto-Refill has shut off from lack of money" )
			self.Entity:shutOff()
		end
	elseif (self.Entity.Inactive) then
		Notify( ply, 4, 3, "An Auto-Refill is inactive, press use on it to make it active again." );
	end
end

function ENT:shutOff()
	local ply = self.Entity:GetBWOwner()
	self.Entity.Inactive = true
	Notify( ply, 1, 3, "NOTICE: AN AUTO-REFILL HAS GONE INACTIVE" );
	Notify( ply, 1, 3, "PRESS USE ON IT TO MAKE IT WORK AGAIN" );
	self.Entity:SetColor(Color(255,0,0,255))
end
function ENT:notifypl()
	local ply = self.Entity:GetBWOwner()
	Notify( ply, 4, 3, "NOTICE: AN AUTO-REFILL IS ABOUT TO GO INACTIVE" );
	Notify( ply, 4, 3, "PRESS USE ON IT TO KEEP IT WORKING" );
	self.Entity:SetColor(Color(255,150,150,255))
end

function ENT:Use(activator,caller)
	if activator:CanAfford(5000) && activator==self.Entity:GetBWOwner() then
		timer.Start( tostring(self.Entity) .. "fuckafkfags")
		timer.Start( tostring(self.Entity) .. "notifyoff")
		self.Entity.Inactive = false
		self.Entity:SetColor(Color(255,255,255,255))
	end
end

function ENT:Think()
	self.Entity:ActivateNearby()
	if !IsValid(self.Entity:GetBWOwner()) then
		self.Entity:Remove()
	end
	self.Entity:NextThink(CurTime() + 1)
	return true
end

function ENT:BWRemove( )
	timer.Destroy(tostring(self.Entity))
	timer.Destroy(tostring(self.Entity) .. "fuckafkfags")
	timer.Destroy(tostring(self.Entity) .. "notifyoff")
end

function ENT:ActivateNearby()
	if !self.Entity:IsPowered() then return end
	for k, v in pairs( ents.FindInSphere(self.Entity:GetPos(), 1000)) do
		if v.Structure then
			if v.Inactive && v:GetBWOwner() == self.Entity:GetBWOwner() then
				v:Reload(self.Entity:GetBWOwner())
				self.Entity:GetBWOwner():AddMoney( -500 );
			end
		end
	end
	return nil
end
