ENT.Type 		= "anim"

ENT.PrintName	= "Base Structure"
ENT.Author		= "Dadadah"
ENT.Contact		= ""

ENT.Spawnable		= false
ENT.AdminSpawnable	= false

// used by gamemode for power plant
ENT.PowerReq = 0
ENT.Structure = true
ENT.MaxUpgrade = 0

function ENT:IsPowered()
	if self.Entity:GetPower() < self.PowerReq then return false else return true end
end

function ENT:SetupDataTables()
	self:NetworkVar("Int", 0, "Power")
	self:NetworkVar("Int", 1, "Upgrade")
	self:NetworkVar("Entity", 0, "BWOwner")
	if SERVER then
		self:SetPower(0)
		self:SetUpgrade(0)
	end
	self.Entity:SetupBWTables()
end

function ENT:SetupBWTables()
	// Abstract Function
end
