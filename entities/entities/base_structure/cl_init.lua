include('shared.lua')

function ENT:Think()
	if tonumber(LocalPlayer():GetInfo("bw_showsparks"))==1 then
		if self.dospark==nil then self.dospark=0 end
		if self.Entity:GetNWBool("sparking") && self.SparkPos!=nil && self.dospark==0 then
			self.dospark=self.dospark+1
			local ang = self.Entity:GetAngles()
			local spos = self.SparkPos
			local effectdata = EffectData()
				effectdata:SetOrigin( self.Entity:GetPos()+ang:Forward()*spos.x+ang:Right()*spos.y+ang:Up()*spos.z)
				effectdata:SetMagnitude( 1 )
				effectdata:SetScale( 1 )
				effectdata:SetRadius( 2 )
			util.Effect( "Sparks", effectdata )
		else
			self.dospark=self.dospark+1
			if self.dospark==3 then self.dospark=0 end
		end
		self.Entity:NextThink(CurTime()+1)
		return true
	end
	self.Entity:NextThink(CurTime()+2)
	return true
end

local sprite = Material("sprites/light_glow02_add")
function ENT:Draw( bDontDrawModel )

	if ( self.RenderGroup == RENDERGROUP_OPAQUE ) then
		self.OldRenderGroup = self.RenderGroup
		self.RenderGroup = RENDERGROUP_TRANSLUCENT
	end

	if ( !bDontDrawModel ) then self:DrawModel() end

	if self.SparkPos!=nil && tonumber(LocalPlayer():GetInfo("bw_showsparks"))==0 then
		local ang = self.Entity:GetAngles()
		local spos = self.SparkPos
		local dotpos = self.Entity:GetPos()+ang:Forward()*spos.x+ang:Right()*spos.y+ang:Up()*spos.z
		local dotcolor = Color(200,200,200,200)
		if self.Entity:GetNWBool("sparking") then
			dotcolor = Color(255,100,100,200)
		end
		render.SetMaterial(sprite)
		render.DrawSprite(dotpos,10,10,dotcolor)
	end
end
