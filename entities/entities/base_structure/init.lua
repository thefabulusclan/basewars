AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:Initialize()
	self.Entity:BWPrePhys()
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	local phys = self.Entity:GetPhysicsObject()
	if(phys:IsValid()) then phys:Wake() end
	self.Entity:BWInit()
end

function ENT:OnTakeDamage(dmg)
	local damage = dmg:GetDamage()
	local attacker=dmg:GetAttacker()
	local inflictor=dmg:GetInflictor()
	self.Entity:SetHealth(self.Entity:Health() - damage)
	if self.Entity:Health() <= 0 then
		if self.Payout!=nil && attacker:IsPlayer() then
			self.Entity:GetBWOwner():GetTable().Value = self.Entity:GetBWOwner():GetTable().Value - (self.Payout[1]*(2^self.Entity:GetUpgrade()))
			local pay=self.Payout[1]*(2^self.Entity:GetUpgrade())*0.75
              if attacker == self.Entity:GetBWOwner() then
                  pay=pay*0.3
              end
			pay=math.ceil(pay)
			attacker:AddMoney(pay)
			Notify(attacker,2,3,"Paid "..tostring(pay).." for destroying a level "..tostring(self.Entity:GetUpgrade()).." "..self.Payout[2])
		end
		self.Entity:Remove()
	end
end

function ENT:OnRemove()
	self.Entity:BWRemove()
	local vPoint = self.Entity:GetPos()
	local effectdata = EffectData()
	effectdata:SetStart( vPoint )
	effectdata:SetOrigin( vPoint )
	effectdata:SetScale( 1 )
	util.Effect( "Explosion", effectdata )
end

function ENT:BWRemove()
		// Abstract Function
end

function ENT:BWPrePhys()
	// Abstract Function
end

function ENT:BWInit()
	// Abstract Function
end

function ENT:Reload(ply)
	// Abstract Function
	self:Use(ply, ply, USE_SET, 0)
end
