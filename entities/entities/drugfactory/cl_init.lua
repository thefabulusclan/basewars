include('shared.lua')

function ENT:MsgDrugFactory( msg )
	local upgrade = self:GetUpgrade()
	local drugs = msg:ReadShort()
	local rands = msg:ReadShort()
	local sdefense = msg:ReadShort()
	local soffense = msg:ReadShort()
	local smobile = msg:ReadShort()
	local mode = msg:ReadShort()

	local inputenabled = false
	if( HelpToggled or GUIToggled ) then
		inputenabled = true
	end
	local panel = vgui.Create( "DFrame" )
	panel:SetPos( (ScrW()/2) - 100, (ScrH()/2) - 105)
	panel:SetName( "Panel" )
	panel:SetSize(200, 210)
	panel:SetTitle("Drug Refinery")
	panel:SetSizable(false)
	panel:MakePopup()
	panel:SetVisible( true )
	local ybutton = {}
	local ylabel = {}

	local maxdrug = 50

	ylabel[2] = vgui.Create( "DLabel", panel )
	ylabel[2]:SetPos( 15, 50 )
	ylabel[2]:SetSize( 130, 14 )
	ylabel[2]:SetText( "Drugs: "..tostring(drugs).."/"..tostring(maxdrug) )
    ylabel[2]:SizeToContents()
	ylabel[2]:SetVisible( true )

	ylabel[3] = vgui.Create( "DLabel", panel )
	ylabel[3]:SetPos( 15, 65 )
	ylabel[3]:SetSize( 130, 14 )
	ylabel[3]:SetText( "Random Drugs: "..tostring(rands).."/15" )
    ylabel[2]:SizeToContents()
	ylabel[3]:SetVisible( true )

	ylabel[4] = vgui.Create( "DLabel", panel )
	ylabel[4]:SetPos( 15, 80 )
	ylabel[4]:SetSize( 130, 14 )
	ylabel[4]:SetText( "Super Drugs: "..tostring(soffense).."/3, "..tostring(sdefense).."/3, "..tostring(smobile).."/3" )
    ylabel[2]:SizeToContents()
	ylabel[4]:SetVisible( true )

	local rmode = "Money"
	if mode==1 then rmode="S. Offense"
	elseif mode==2 then rmode="S. Defense"
	elseif mode==3 then rmode="S. Mobile"
	end

	ylabel[5] = vgui.Create( "DLabel", panel )
	ylabel[5]:SetPos( 15, 95 )
	ylabel[5]:SetSize( 130, 14 )
	ylabel[5]:SetText( "Refining to "..rmode )
    ylabel[5]:SizeToContents()
	ylabel[5]:SetVisible( true )

	ybutton[1] = vgui.Create( "DButton", panel )
	ybutton[1]:SetPos( 15, 110 )
	ybutton[1]:SetSize( 130, 14 )
	ybutton[1]:SetText( "Eject Super Drugs" )
	ybutton[1].DoClick = function(msg) LocalPlayer():ConCommand( "setrefinerymode " .. self.Entity:EntIndex() .. " eject\n" ) panel:Close() end
	ybutton[1]:SetVisible( true )

	ybutton[5] = vgui.Create( "DButton", panel )
	ybutton[5]:SetPos( 15, 125 )
	ybutton[5]:SetSize( 130, 14 )
	ybutton[5]:SetText( "Refine to Money" )
	ybutton[5].DoClick = function(msg) LocalPlayer():ConCommand( "setrefinerymode " .. self.Entity:EntIndex() .. " money\n" ) panel:Close() end
	ybutton[5]:SetVisible( true )
	local ymod=0
	if upgrade>=1 then
		ybutton[2] = vgui.Create( "DButton", panel )
		ybutton[2]:SetPos( 15, 140 )
		ybutton[2]:SetSize( 130, 14 )
		ybutton[2]:SetText( "Refine to Offense" )
		ybutton[2].DoClick = function(msg) LocalPlayer():ConCommand( "setrefinerymode " .. self.Entity:EntIndex() .. " offense\n" ) panel:Close() end
		ybutton[2]:SetVisible( true )

		ybutton[3] = vgui.Create( "DButton", panel )
		ybutton[3]:SetPos( 15, 155 )
		ybutton[3]:SetSize( 130, 14 )
		ybutton[3]:SetText( "Refine to Defense" )
		ybutton[3].DoClick = function(msg) LocalPlayer():ConCommand( "setrefinerymode " .. self.Entity:EntIndex() .. " defense\n" ) panel:Close() end
		ybutton[3]:SetVisible( true )

		ybutton[4] = vgui.Create( "DButton", panel )
		ybutton[4]:SetPos( 15, 170 )
		ybutton[4]:SetSize( 130, 14 )
		ybutton[4]:SetText( "Refine to Mobile" )
		ybutton[4].DoClick = function(msg) LocalPlayer():ConCommand( "setrefinerymode " .. self.Entity:EntIndex() .. " mobile\n" ) panel:Close() end
		ybutton[4]:SetVisible( true )
		ymod = 45
	end
	if upgrade>=2 && soffense>=3 && sdefense>=3 && smobile>=3 then
		ybutton[6] = vgui.Create( "DButton", panel )
		ybutton[6]:SetPos( 15, 140+ymod )
		ybutton[6]:SetSize( 130, 14 )
		ybutton[6]:SetText( "Create Uber Drug" )
		ybutton[6].DoClick = function(msg) LocalPlayer():ConCommand("setrefinerymode "..self.Entity:EntIndex().." uber\n") panel:Close() end
		ybutton[6]:SetVisible( true )
	end
end

usermessage.Hook("drugfactorygui",function( msg )
	local E = msg:ReadShort()
	ents.GetByIndex(E):MsgDrugFactory(msg)
end)
