-- ============================================
-- =                                          =
-- =          Crate SENT by Mahalis           =
-- =                                          =
-- ============================================

// copy pasta from DURG lab

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:BWPrePhys()
	self.Entity:SetModel( "models/props_c17/furniturestove001a.mdl")
end

function ENT:BWInit()
	self.Entity:SetNWBool("sparking",false)
	self.Entity:SetMaxHealth(250)
	self.Entity:SetHealth(250)
	self.LastUsed = CurTime()
	self.Drugs = 0
	self.RandomDrugs = 0
	self.SOffense = 0
	self.SDefense = 0
	self.SMobile = 0
	self.Refmode = 0
end

function ENT:Use(activator,caller)
	if self.LastUsed + 0.3 > CurTime() then
		self.LastUsed = CurTime()
		return
	end
	self.LastUsed = CurTime()

	umsg.Start( "drugfactorygui", activator )
		umsg.Short( self.Entity:EntIndex() )
		umsg.Short( self.Drugs )
		umsg.Short( self.RandomDrugs )
		umsg.Short( self.SDefense )
		umsg.Short( self.SOffense )
		umsg.Short( self.SMobile )
		umsg.Short( self.Refmode )
	umsg.End()
end

function ENT:Think()
	if (!IsValid(self.Entity:GetBWOwner())) then
		self.Entity:Remove()
	end
end

function ENT:DropDrug()
	local drug = ents.Create("basewars_drug")
	drug.Owner = self.Entity:GetBWOwner()
	drug:SetPos( self.Entity:GetPos()+Vector(0,0,50));
	drug:SetColor(Color(25,25,25,255))
	drug.drugType = "random"
	drug.DrugEffect = function(caller)
		CallDrug(GetRandomDrug() ,caller)
	end
	drug:Spawn();
end

function ENT:DropSuperDrug()
	if self.Refmode==0 then
		Notify(self.Entity:GetBWOwner(),2,3,"Paid $160,000 from refining drugs")
		self.Entity:GetBWOwner():AddMoney(160000)
	else
		local drug = ents.Create("basewars_drug")
		drug.Owner = self.Entity:GetBWOwner()
		drug:SetPos( self.Entity:GetPos()+Vector(0,0,50));
		if (self.Refmode == 1) then
			drug.drugType = "Super Offensive"
			drug.DrugEffect = function(caller)
				caller:CallSuperDrug(drugsIndex["Super Offensive"])
			end
		elseif (self.Refmode == 2) then
			drug.drugType = "Super Defensive"
			drug.DrugEffect = function(caller)
				caller:CallSuperDrug(drugsIndex["Super Defensive"])
			end
		else
			drug.drugType = "Super Mobility"
			drug.DrugEffect = function(caller)
				caller:CallSuperDrug(drugsIndex["Super Mobility"])
			end
		end
		drug:Spawn();
	end
end

function ENT:EjectSuperDrugs()
	for i=1,self.SOffense do
		local drug = ents.Create("basewars_drug")
		drug.Owner = self.Entity:GetBWOwner()
		drug:SetPos( self.Entity:GetPos()+Vector((i*10)-10,10,50));
		drug.drugType = "Super Offensive"
		drug.DrugEffect = function(caller)
			caller:CallSuperDrug(drugsIndex["Super Offensive"])
		end
		drug:Spawn();
	end
	self.SOffense=0
	for i=1,self.SDefense do
		local drug = ents.Create("basewars_drug")
		drug.Owner = self.Entity:GetBWOwner()
		drug:SetPos( self.Entity:GetPos()+Vector((i*10)-10,0,50));
		drug.drugType = "Super Defensive"
		drug.DrugEffect = function(caller)
			caller:CallSuperDrug(drugsIndex["Super Defensive"])
		end
		drug:Spawn();
	end
	self.SDefense=0
	for i=1,self.SMobile do
		local drug = ents.Create("basewars_drug")
		drug.Owner = self.Entity:GetBWOwner()
		drug:SetPos( self.Entity:GetPos()+Vector((i*10)-10,-10,50));
		drug.drugType = "Super Mobility"
		drug.DrugEffect = function(caller)
			caller:CallSuperDrug(drugsIndex["Super Mobility"])
		end
		drug:Spawn();
	end
	self.SMobile=0
end

function ENT:DropUberDrug()
	local drug = ents.Create("basewars_drug")
	drug.Owner = self.Entity:GetBWOwner()
	drug:SetPos( self.Entity:GetPos()+Vector(0,0,50));
	drug.drugType = "uber"
	drug.DrugEffect = function(caller)
		caller:CallSuperDrug(drugsIndex["Uber"])
	end
	drug:Spawn();
end

function ENT:BWRemove( )
	self.Entity:EjectSuperDrugs()
end

function ENT:Touch(gun)
	if self.Entity:IsPowered() then
		local upgrade = self.Entity:GetUpgrade()
		local drugamt = 50
		local randamt = 15
		if (gun:GetClass() == "basewars_drug" && gun.used == false) then
			if (gun.drugType == "drug") then
				self.Drugs=self.Drugs+gun.count
				gun.used = true
				gun:Remove()
			end
			if (gun.drugType == "random" && upgrade>=1) then
				self.RandomDrugs=self.RandomDrugs+1
				gun.used = true
				gun:Remove()
			end
			if upgrade>=2 then
				if (gun.drugType == "Super Offensive" && self.SOffense<3) then
					self.SOffense=self.SOffense+1
					gun.used = true
					gun:Remove()
				end
				if (gun.drugType == "Super Defensive" && self.SDefense<3) then
					self.SDefense=self.SDefense+1
					gun.used = true
					gun:Remove()
				end
				if (gun.drugType == "Super Mobility" && self.SMobile<3) then
					self.SMobile=self.SMobile+1
					gun.used = true
					gun:Remove()
				end
			end
			while (self.Drugs>=drugamt) do
				self.Drugs = self.Drugs - drugamt
				self.Entity:DropDrug()
			end
			while (self.RandomDrugs >= randamt) do
				self.RandomDrugs = self.RandomDrugs - randamt
				self.Entity:DropSuperDrug()
			end
		end
	end
end

function ENT:CanRefine(mode,ply)
	if ply!=self.Entity:GetBWOwner() && mode!="eject" then
		return "Only the owner of this Drug Refinery can change settings."
	end
	if !self.Entity:IsPowered() && mode=="uber" then
		return "Low power."
	end
	if mode=="eject" || mode=="money" then return true end
	if (mode=="offense" || mode=="defense" || mode=="mobile") then
		if self.Entity:GetUpgrade()>=1 then
			return true
		else
			return "This Refinery is not upgraded enough."
		end
	end
	if mode=="uber" then
		if self.Entity:GetUpgrade()>=2 then
			if self.SOffense>=3 && self.SDefense>=3 && self.SMobile>=3 then
				return true
			else
				return "Not enough Superdrugs."
			end
		else
			return "This Refinery is not upgraded enough."
		end
	end
	return "You're doing it wrong."
end

function ENT:SetMode(mode)
	if mode=="eject" then
		self.Entity:EjectSuperDrugs()
	end
	if mode=="money" then
		self.Refmode=0
	elseif mode=="offense" then
		self.Refmode=1
	elseif mode=="defense" then
		self.Refmode=2
	elseif mode=="mobile" then
		self.Refmode=3
	elseif mode=="uber" then
		self.Entity:DropUberDrug()
		self.SOffense=0
		self.SDefense=0
		self.SMobile=0
		UberDrugExists()
	end
end

function ccSetRefineryMode( ply, cmd, args )
	if (args[1]==nil || args[2] == nil) then
		return
	end
	mode = tostring(args[2])
	if !IsValid(ents.GetByIndex(args[1])) || (mode!="money" && mode!="offense" && mode!="defense" && mode!="mobile" && mode!="eject" && mode!="uber") then
		return
	end
	local vault = ents.GetByIndex(args[1])
	if (vault:GetClass()!="drugfactory" || ply:GetPos():Distance(vault:GetPos())>300) then return end

	local ref = vault:CanRefine(mode,ply)
	if (ref==true) then
		vault:SetMode(mode)
	else
		Notify(ply,4,3,ref)
	end
end
concommand.Add( "setrefinerymode", ccSetRefineryMode );
