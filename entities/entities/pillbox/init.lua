AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

util.AddNetworkString( "PillBoxGUI" )

function ENT:BWPrePhys()
	self.Entity:SetModel( "models/props_c17/furniturefridge001a.mdl")
end

function ENT:BWInit()
	self.Entity:SetNWBool("sparking",false)
	self.Entity:SetMaxHealth(750)
	self.Entity:SetHealth(750)
	self.Locked = true
	self.LastUsed = CurTime()
	self.LastTouch = CurTime()
	self.Drugs = {}
	self.MaxDrugs = 10
end

function ENT:Use(activator,caller)
	if (self.LastUsed+0.5>CurTime()) then
		self.LastUsed = CurTime()
	else
		self.LastUsed = CurTime()
		local ply = self.Entity:GetBWOwner()
		if (activator==ply || !self.Locked || activator:IsAllied(ply)) then
			if (activator==ply) then
				self.Locked = true
			end
			net.Start("PillBoxGUI")
				net.WriteInt(self.Entity:EntIndex(), 16)
				net.WriteTable(self.Drugs)
			net.Send(activator)
		else
			Notify(activator,4,3,"This pill box is locked! use a lock pick to force it open.")
		end
	end
end

function ENT:Think()
	if (!IsValid(self.Entity:GetBWOwner())) then
		self.Entity:Remove()
	end
end

function ENT:Touch(item)
	if self.LastTouch+1.5 > CurTime() then
		self.LastTouch = CurTime()
		return
	else
		if item:GetClass() == "basewars_drug" && item.drugType != "booze" && item.drugType != "drug" then
			if #self.Drugs <= self.MaxDrugs then
				if self.Drugs[item.drugType] == nil then
					self.Drugs[item.drugType] = 0
				end
				self.Drugs[item.drugType] = self.Drugs[item.drugType] + 1
				item:Remove()
				self.LastTouch = CurTime()
			end
		end
	end
end

function ENT:IsLocked()
	return self.Locked
end

function ENT:SetUnLocked()
	self.Locked = false
	return true
end

function WithdrawDrug(ply, cmd, args)
	local ent = ents.GetByIndex(args[2])
	local drug = drugsIndex[args[1]]
	if (ent == nil || ent.Drugs == nil || drug == nil
		|| ent.Drugs[drug.Class] == nil || ent.Drugs[drug.Class] == 0
		|| (ply != ent:GetBWOwner() && !ply:IsAllied(ent:GetBWOwner()))) then
		return
	end
	ent.Drugs[drug.Class] = ent.Drugs[drug.Class] - 1
	ent.LastTouch = CurTime()

	local drugent = ents.Create("basewars_drug")
	drugent.Owner = ply
	drugent:SetPos(ent:GetPos() + ent:GetForward() * 50);
	drugent:SetColor(drug.Color)
	drugent.drugType = drug.Class
	drugent.DrugEffect = function(caller)
		caller:CallDrug(drug)
	end
	drugent:Spawn();
end
concommand.Add("withdrawdrug", WithdrawDrug)
