include('shared.lua')

function ENT:PillBoxMenu( drugs, index )
	local panel = vgui.Create( "DFrame" )
	panel:SetPos( ScrW()/2-80 , ScrH() / 2 - 100 )
	panel:SetName( "Panel" )
	panel:SetSize(160,200)
	panel:SetSizable(false)
	panel:SetTitle("Select Item")
	panel:SetVisible( true )
	panel:MakePopup()
  for k,v in pairs(drugs) do
    for i = 1, v, 1 do
      local ybutton = vgui.Create( "DButton", panel )
      ybutton:SetPos( 15, 20+(i*15) );
      ybutton:SetSize( 130, 14 );
      ybutton:SetText( k );
      ybutton.DoClick = function(msg)
        LocalPlayer():ConCommand( "withdrawdrug " .. k .. " " .. index )
        panel:Close()
      end
      ybutton:SetVisible( true );
    end

  end
end

net.Receive("PillBoxGUI", function( len, ply )
  local E = net.ReadInt(16)
  local drugs = net.ReadTable()
  ents.GetByIndex(E):PillBoxMenu(drugs, E)
end)
