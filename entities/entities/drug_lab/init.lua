AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:BWInit()

	self.Entity:SetModel( "models/props_combine/combine_mine01.mdl")
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	local phys = self.Entity:GetPhysicsObject()
	if(phys:IsValid()) then phys:Wake() end
	self.Entity:SetNWBool("sparking",false)
	self.Entity:SetMaxHealth(250)
	self.Entity:SetHealth(250)
	local ply = self.Entity:GetBWOwner()
	self.Entity.Inactive = false
	self.unowned = false
	self.Payout={self.cost,"Drug Lab"}
	self.Entity:SetColor(Color(255,255,255,255))
end

function ENT:Use(ply)
	local ply = self.Entity:GetBWOwner()
	if IsValid(ply) && self.Entity:IsPowered() then
		self.Entity:SetNWBool("sparking",true)
		timer.Create( tostring(self.Entity) .. "drug", 43-(self.Entity:GetUpgrade()*4), 1, function() self:createDrug() end)
		self.Entity.Inactive = false
		self.Entity:SetColor(Color(255,255,255,255))
	end
end

function ENT:createDrug()
	local spos = self.SparkPos
	local ang = self.Entity:GetAngles()
	if (self.Entity:GetBWOwner():GetTable().maxDrugs <= 30) then
		drug = ents.Create("basewars_drug")
		drug.Owner = self.Entity:GetBWOwner()
		drug:SetModel( "models/props_junk/glassjug01.mdl")
		drug:SetPos(self.Entity:GetPos()+ang:Forward()*spos.x+ang:Right()*spos.y+ang:Up()*spos.z)
		drug.DrugEffect = function(caller)
			DrugPlayer(caller)
		end
		drug.drugType = "drug"
		drug:Spawn()
	end
	self.Entity.Inactive = true
	self.Entity:SetNWBool("sparking",false)
end

function ENT:Think()
	if !IsValid(self.Entity:GetBWOwner()) then
		self.Entity:Remove()
	end
	self.Entity:NextThink(CurTime()+0.1)
	return true
end

function ENT:BWRemove( )
	timer.Destroy(tostring(self.Entity) .. "drug")
end
