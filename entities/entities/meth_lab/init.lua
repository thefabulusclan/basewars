-- ============================================
-- =                                          =
-- =          Crate SENT by Mahalis           =
-- =                                          =
-- ============================================

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:BWInit()

	self.Entity:SetModel( "models/props/de_train/processor_nobase.mdl")
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	local phys = self.Entity:GetPhysicsObject()
	if(phys:IsValid()) then phys:Wake() end
	self.Entity:SetMaxHealth(50)
	self.Entity:SetHealth(50)
	local ply = self.Entity:GetBWOwner()
	self.Entity:SetColor(Color(255,255,255,255))
	self.Payout = {self.cost, "Meth Lab"}
	self.Entity.Inactive = true
end

function ENT:Use(activator,caller)
	self.Entity:SetNWBool("sparking",true)
	timer.Create( tostring(self.Entity) .. "drug", 180, 1, function() self:createDrug() end)
	self.Entity.Inactive = false
	self.Entity:SetColor(Color(0,255,0,255))
end

function ENT:createDrug()
	if math.Rand(0,1) < (0.1 - (0.01 * self.Entity:GetUpgrade())) then
		util.BlastDamage( self.Entity, self.Entity:GetBWOwner(), self.Entity:GetPos(), 384, 55 )
		self.Entity:Remove()
		Notify( ply, 1, 3, "A METH LAB HAS BLOWN RIGHT THE FUCK UP" )
		return
	end
	local spos=self.SparkPos
	local ang=self.Entity:GetAngles()
	local drugPos = self.Entity:GetPos()
	local drug = ents.Create("basewars_drug")
	drug.Owner = self.Entity:GetBWOwner()
	drug:SetPos(self.Entity:GetPos()+ang:Forward()*spos.x+ang:Right()*spos.y+ang:Up()*spos.z)
	drug:SetColor(Color(25,25,25,255))
	drug.drugType = "random"
	drug.DrugEffect = function(caller)
		CallDrug(GetRandomDrug() ,caller)
	end
	drug:Spawn();
	self.Entity.Inactive = true
	self.Entity:SetNWBool("sparking",false)
	self.Entity:SetColor(Color(255,255,255,255))
end

function ENT:Think()
	if (!IsValid(self.Entity:GetBWOwner())) then
		self.Entity:Remove()
	end
	self.Entity:NextThink(CurTime()+0.1)
	return true
end

function ENT:BWRemove( )
	timer.Destroy(tostring(self.Entity) .. "drug")
end
