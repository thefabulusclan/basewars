include('shared.lua')

function SWEP:DrawHUD()
	if( LocalPlayer():KeyDown(1)) then
			local trace = self.Owner:GetEyeTrace()
			if (trace.Entity:GetNWInt("welddamage")!=nil && trace.HitPos:Distance(LocalPlayer():GetShootPos()) <= 128 and (trace.Entity:GetClass()=="prop_physics_multiplayer" || trace.Entity:GetClass()=="prop_physics_respawnable" || trace.Entity:GetClass()=="prop_physics" || trace.Entity:GetClass()=="phys_magnet" || trace.Entity:GetClass()=="gmod_spawner" || trace.Entity:GetClass()=="gmod_wheel" || trace.Entity:GetClass()=="gmod_thruster" || trace.Entity:GetClass()=="gmod_button" || trace.Entity:GetClass()=="sent_keypad")and trace.Entity:IsValid()) then
				draw.RoundedBox( 2, ScrW()/2+10, ScrH()/2+25, 100, 33, Color(0,0,0,50) )
				local weldpercent = -((tonumber(trace.Entity:GetNWInt("welddamage"))/100)*100)+100
				draw.RoundedBox( 1, ScrW()/2+11, ScrH()/2+26, weldpercent, 6, Color(150,150,0,100) )
			end
	end
end
