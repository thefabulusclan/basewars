// copy pasta from lock pick.

if(SERVER) then
	AddCSLuaFile( "cl_init.lua" )
	AddCSLuaFile( "shared.lua" )
end

if( CLIENT ) then

	SWEP.PrintName = "Blowtorch";
	SWEP.Slot = 5;
	SWEP.SlotPos = 8;
	SWEP.DrawAmmo = false;
	SWEP.DrawCrosshair = false;

end

// Variables that are used on both client and server

SWEP.Author			= "HLTV Proxy"
SWEP.Instructions	= "Blowtorch: Hold left click on a prop to eventually remove it."
SWEP.Contact		= ""
SWEP.Purpose		= ""

SWEP.ViewModelFOV	= 73
SWEP.ViewModelFlip	= false
SWEP.ViewModel = Model( "models/weapons/v_IRifle.mdl" )
SWEP.WorldModel = Model( "models/weapons/w_IRifle.mdl" )

SWEP.Spawnable			= false
SWEP.AdminSpawnable		=false

SWEP.Sound = Sound( "physics/wood/wood_box_impact_hard3.wav" );

SWEP.Primary.ClipSize		= -1					// Size of a clip
SWEP.Primary.DefaultClip	= 0				// Default number of bullets in a clip
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo			= ""

SWEP.Secondary.ClipSize		= -1				// Size of a clip
SWEP.Secondary.DefaultClip	= 0				// Default number of bullets in a clip
SWEP.Secondary.Automatic	= true			// Automatic/Semi Auto
SWEP.Secondary.Ammo			= ""



/*---------------------------------------------------------
   Name: SWEP:Initialize( )
   Desc: Called when the weapon is first loaded
---------------------------------------------------------*/
function SWEP:Initialize()

	if( SERVER ) then
		// make it an ar2 model, but hold it as a pistol so that people know its the welder.
		self:SetWeaponHoldType( "pistol" );

	end
	util.PrecacheSound("physics/metal/metal_box_impact_soft2.wav")
	util.PrecacheSound("ambient/energy/spark1.wav")
	util.PrecacheSound("ambient/energy/spark2.wav")
	util.PrecacheSound("ambient/energy/spark3.wav")
	util.PrecacheSound("ambient/energy/spark4.wav")
end


/*---------------------------------------------------------
   Name: SWEP:Precache( )
   Desc: Use this function to precache stuff
---------------------------------------------------------*/
function SWEP:Precache()
end


/*---------------------------------------------------------
   Name: SWEP:PrimaryAttack( )
   Desc: +attack1 has been pressed
---------------------------------------------------------*/

function SWEP:PrimaryAttack()
	self.Weapon:SetNextPrimaryFire(CurTime() + 0.05)
	local trace = self.Owner:GetEyeTrace()
	local snd = math.random(1,4)
	if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 128 && trace.Entity:IsValid() &&
	(
		trace.Entity:GetClass()=="prop_ragdoll" ||
		trace.Entity:GetClass()=="prop_physics_multiplayer" ||
		trace.Entity:GetClass()=="prop_physics_respawnable" ||
		trace.Entity:GetClass()=="prop_physics" ||
		trace.Entity:GetClass()=="phys_magnet" ||
		trace.Entity:GetClass()=="gmod_spawner" ||
		trace.Entity:GetClass()=="gmod_wheel" ||
		trace.Entity:GetClass()=="gmod_thruster" ||
		trace.Entity:GetClass()=="gmod_button" ||
		trace.Entity:GetClass()=="sent_keypad"
	) then
		local effectdata = EffectData()
			effectdata:SetOrigin(trace.HitPos)
			effectdata:SetMagnitude( 1 )
			effectdata:SetScale( 2 )
			effectdata:SetRadius( 2 )
		util.Effect( "Sparks", effectdata )

		self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
		self.Owner:SetAnimation( PLAYER_ATTACK1 )
		self.Weapon:EmitSound("ambient/energy/spark" .. snd .. ".wav")

		if SERVER then
			timer.Destroy(tostring(trace.Entity) .. "unweldamage")

			timer.Create(tostring(trace.Entity) .. "unweldamage", 10, 1, function() trace.Entity:SetNWInt("welddamage", 255) end)
		end

		if trace.Entity:GetNWInt("welddamage") == nil ||
		trace.Entity:GetNWInt("welddamage") <=0 ||
		trace.Entity:GetNWInt("welddamage") == 100 then
			trace.Entity:SetNWInt("welddamage", 99)
			if SERVER then
					entowner = player.GetByUniqueID(trace.Entity:GetVar("PropProtection"))
					Notify(entowner, 1, 3, "Someone is destroying one of your props with a blowtorch!")
			end
		elseif(trace.Entity:GetNWInt("welddamage") > 1) then
			trace.Entity:SetNWInt("welddamage", trace.Entity:GetNWInt("welddamage") - 1)
			if SERVER then
				if (trace.Entity:GetNWInt("welddamage")==50) then
					if (trace.Entity:CPPIGetOwner()==true) then
						entowner = player.GetByUniqueID(trace.Entity:GetVar("PropProtection"))
						Notify(entowner, 1, 3, "Someone is half finished destroying one of your props using blowtorch!")
					end
				end
			end
		else
			if SERVER then
				if (player.GetByUniqueID(trace.Entity:GetVar("PropProtection"))!=false) then
						entowner = player.GetByUniqueID(trace.Entity:GetVar("PropProtection"))
						Notify(entowner, 1, 3, "Someone has destroyed one of your props using a blowtorch!")
				end
			end
			local effectdata = EffectData()
				effectdata:SetStart( trace.Entity:GetPos() )
				effectdata:SetOrigin( trace.Entity:GetPos() )
				effectdata:SetScale( 1 )
			util.Effect( "Explosion", effectdata )
			if SERVER then
				timer.Destroy(tostring(trace.Entity) .. "unweldamage")
				trace.Entity:Remove()
			end
		end
	else
		self.Weapon:EmitSound("ambient/energy/spark" .. snd.. ".wav")
		self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
		self.Owner:SetAnimation( PLAYER_ATTACK1 )
	end
end


// secondary attack - repair shit.

function SWEP:SecondaryAttack()

end

// this isnt needed anymore, since there is a hook to make it not drop.
function SWEP:Equip()
	if SERVER then
		if (self.WasEquipped==1) then self.Weapon:Remove() end
		self.WasEquipped=1
	end
end

function SWEP:ShouldDropOnDie()
	return false
end
