# README #

Welcome, to Basewars.

This version is the version (HEAVILY) modified by Dadadah, Oggal, and Leo1997 (With some credit to Ninja.) It is based on RBasewars, a fork of the original Laughing Llama Basewars. This version of Basewars has been extremely optimized and rewritten. However, that said, this version is also an attempt to feel as "Classic" as it can be. With Drugs, Printers, Guns, The spawn menu, and Factions being completely rewritten, this gamemode is barely like its predecessor LLAMABW. The gamemode now runs much more efficiently than it ever has. There are plans to also rewrite other parts of the gamemode as well. More than 80% of dead, needless, or otherwise outdated code has also been removed. There have been API's added for Drugs and Printers, as well as a smart rank system.

To see the files with the largest changes, compare the following with older versions of basewars:

- rplol.lua

- weapons.lua

- copypasta.lua

- ExtraCrap.lua

There is still plenty of work to be done! Some of the things that need desperate rewrites/organization/optimizations include:

- Client side rendering

- Gun Lab

- Prop damage

- Explosion damage

- UMSG < NET

- Messaging System
