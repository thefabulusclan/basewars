team.SetUp( 1, "Baser", Color( 125, 125, 125, 255 ) )

GM.Email 	= "jacob.lee.schlecht@gmail.com"
GM.Name = "Basewars";
GM.Author = "Dadadah";
includeName = "basewars_dev"

function RetrieveBenifits(ply)
	local benifit = 0
	local id = ply:SteamID()
	if minidonators[id] then
		benifit = 1
	end
	if donators[id] || ply:IsUserGroup("mod") then
		benifit = 2
	end
	if superdonators[id] then
		benifit = 3
	end
	if novadonators[id] then
		benifit = 4
	end
	if goddonators[id] || ply:IsAdmin() || ply:IsSuperAdmin() then
		benifit = 5
	end
	if sponsor[id] || ply:IsUserGroup("owner") || ply:IsUserGroup("dev") || game.SinglePlayer() then
		benifit = 6
	end
	return benifit
end

//TODO: Utilize IsRanked function.
function IsRanked(ply)
	if ply:IsAdmin() || ply:IsSuperAdmin() || ply:IsUserGroup("owner") || ply:IsUserGroup("dev") || game.SinglePlayer() then
		return true
	else
		return false
	end
end

function resource.AddDir(dir)
	local f, d = file.Find(dir .. '/*', 'GAME')

	for k, v in pairs(f) do
		resource.AddSingleFile(dir .. '/' .. v)
	end

	for k, v in pairs(d) do
		resource.AddDir(dir .. '/' .. v)
	end
end
resource.AddDir("sound/gg")
resource.AddDir("materials/tfc")
resource.AddSingleFile("sound/tfc/chaching.mp3")

if SERVER then
  resource.AddWorkshop(160250458) // Wiremod
end
