DeriveGamemode( "sandbox" );

drugeffect_doubletapmod = 0.675

GUIToggled = false;
HelpToggled = false;

local drugTable = {}
allPlayersMoney = {}

--------------Tribe
Tribes = {}

AdminTellAlpha = -1;
AdminTellStartTime = 0;
AdminTellMsg = "";

util.PrecacheSound("tfc/chaching.mp3")

afkbigness = 255
afkdir = true
local Extracrap = { }

if( HelpVGUI ) then
	HelpVGUI:Remove();
end

HelpVGUI = nil;

StunStickFlashAlpha = -1;

local viewpl = nil
local viewpltime = 0

local viewstructure = nil
local viewstructuretime = 0

surface.CreateFont("AFKFont", {
	font = "ChatFont",
	size = 72,
	weight = 700,
	antialias = true
})

surface.CreateFont("HUDNumber", {
    font = "Trebuchet MS",
    size = 40,
    weight = 900,
})

surface.CreateFont("HUDNumber1", {
    font = "Trebuchet MS",
    size = 41,
    weight = 900,
})

surface.CreateFont("HUDNumber2", {
    font = "Trebuchet MS",
    size = 42,
    weight = 900,
})

surface.CreateFont("HUDNumber3", {
    font = "Trebuchet MS",
    size = 43,
    weight = 900,
})

surface.CreateFont("HUDNumber4", {
    font = "Trebuchet MS",
    size = 44,
    weight = 900,
})

surface.CreateFont("HUDNumber5", {
    font = "Trebuchet MS",
    size = 45,
    weight = 900,
})

surface.CreateFont("MessageFont", {
	font = "Default",
	size = 72,
	weight = 700,
	antialias = true
})

include( "shared.lua" )
include( "copypasta.lua" )
include( "cl_helpvgui.lua" )
include( "cl_scoreboard.lua" )
include( "cl_msg.lua" )
include( "cl_menu.lua" )
include( "cl_hud.lua" )
include("sh_vars.lua")
include("tfcskin.lua")
include( "ranks.lua" )
include( "cl_raidui.lua" )
for k, v in pairs(file.Find(includeName.."/gamemode/weapons/*.lua","LUA")) do include("weapons/" .. v); end
for k, v in pairs(file.Find(includeName.."/gamemode/structures/*.lua","LUA")) do include("structures/" .. v); end
for k, v in pairs(file.Find(includeName.."/gamemode/drugs/*.lua","LUA")) do include("drugs/" .. v); end

// FPP BEGIN

include("fpp/fpp/client/menu.lua")
include("fpp/fpp/client/hud.lua")
include("fpp/fpp/client/buddies.lua")
include("fpp/fpp/client/ownability.lua")
include("fpp/fpp/sh_cppi.lua")
include("fpp/fpp/sh_settings.lua")

// FPP END

surface.CreateFont("AckBarWriting", {
	font = "akbar",
	size = 20,
	weight = 500,
	antialias = true
})

surface.CreateFont("HL2Symbols", {
	font = "HalfLife2",
	size = 96,
	weight = 500,
	antialias = true
})
function GetTextHeight( font, str )

	surface.SetFont( font );
	local w, h = surface.GetTextSize( str );

	return h;

end

function GM:ForceDermaSkin()
	return "TFCSkin"
end

function GM:ContextMenuOpen()
    if !LocalPlayer():IsUserGroup("owner") && !LocalPlayer():IsUserGroup("dev") then return false end
end

local function DrawBox(startx, starty, sizex, sizey, color1,color2 )
	draw.RoundedBox( 0, startx, starty, sizex, sizey, color2 )
	draw.RoundedBox( 0, startx+1, starty+1, sizex-2, sizey-2, color1 )
end

function DrawPlayerInfo( ply, scn )

	if( not ply:Alive() ) then return; end

	local pos = ply:EyePos();

	pos.z = pos.z + 14;
	pos = pos:ToScreen();
	draw.DrawText( ply:Nick(), "TargetID", pos.x + 1, pos.y + 1, Color( 0, 0, 0, 255 ), 1 );
	draw.DrawText( ply:Nick(), "TargetID", pos.x, pos.y, team.GetColor( ply:Team() ), 1 );
end

local Scans = {}

function ScannerSweep(msg)
	local ply = msg:ReadEntity()
	local pos = msg:ReadVector()
	local num = msg:ReadShort()
	Scans[num] = {}
	Scans[num].Time = CurTime()+10
	Scans[num].Pos = pos
	Scans[num].Ply = ply
end
usermessage.Hook("RadarScan", ScannerSweep)

function clearscan(index)
	//Msg(player.GetByID(index):GetName().."\n")
	Scans[index] = nil
end

function DrawScans()
	for k, v in pairs(Scans) do
		if v!=nil then
			local ply = v.Ply
			if IsValid(ply) then
				local pos = v.Pos + Vector(0,0,100)

				pos = pos:ToScreen();
				draw.DrawText( ply:Nick(), "TargetID", pos.x + 1, pos.y + 1, Color( 0, 0, 0, 255 ), 1 );
				draw.DrawText( ply:Nick(), "TargetID", pos.x, pos.y, team.GetColor( ply:Team() ), 1 );
				draw.DrawText( "SCAN", "TargetID", pos.x + 1, pos.y-16 + 1, Color( 0, 0, 0, 255 ), 1 );
				draw.DrawText( "SCAN", "TargetID", pos.x, pos.y-16, Color(255,0,0,255 ), 1 );
			end
			if v.Time<CurTime() then
				clearscan(k)
			end
		end
	end
end



function DrawBombInfo( ent )
	if LocalPlayer():GetPos():Distance(ent:GetPos())<2048 && ent:GetNWBool("armed") then
		local pos = ent:GetPos()+ent:GetAngles():Up()*30;

		pos.z = pos.z + 14;
		pos = pos:ToScreen();
		local time = math.ceil(ent:GetNWFloat("goofytiem")-CurTime())
		if time<=2 then
			time = "YOU JUST LOST\nTHE GAME"
		end
		draw.DrawText( "BOMB\n" .. tostring(time), "TargetID", pos.x + 1, pos.y + 1, Color( 0, 0, 0, 255 ), 1 );
		draw.DrawText( "BOMB\n" .. tostring(time), "TargetID", pos.x, pos.y, Color(255,0,0,255), 1 );
	end
end

function DrawMythrilInfo( ent )
	if LocalPlayer():GetPos():Distance(ent:GetPos())<5120 then
		local pos = ent:GetPos()

		pos.z = pos.z + 14;
		pos = pos:ToScreen();
		draw.DrawText( "Mythril Printer\n", "TargetID", pos.x + 1, pos.y + 1, Color( 0, 0, 0, 255 ), 1 );
		draw.DrawText( "Mythril Printer\n", "TargetID", pos.x, pos.y, Color(0,100,100,255), 1 );
	end
end

function DrawAdamantiteInfo( ent )
	if LocalPlayer():GetPos():Distance(ent:GetPos())<5120 then
		local pos = ent:GetPos()

		pos.z = pos.z + 14;
		pos = pos:ToScreen();
		draw.DrawText( "Adamantite Printer\n", "TargetID", pos.x + 1, pos.y + 1, Color( 0, 0, 0, 255 ), 1 );
		draw.DrawText( "Adamantite Printer\n", "TargetID", pos.x, pos.y, Color(50,0,0,255), 1 );
	end
end


function DrawTurretInfo( ent )
	if LocalPlayer():GetPos():Distance(ent:GetPos())<258 && ent:GetNWBool("NotBuilt") then
		local pos = ent:GetPos()+ent:GetAngles():Up()*25;

		pos.z = pos.z + 14;
		pos = pos:ToScreen();

		draw.DrawText( "Hold 'E' to Activate your Turret", "TargetID", pos.x, pos.y, Color(255,255,255,255), 1 );
	end
end

function DrawStructureInfo( ent, alpha )
	local pos = ent:GetPos();

	pos.z = pos.z + 14;
	pos = pos:ToScreen();
	DrawBox(pos.x-100, pos.y-50, 200, 100, Color(0,0,0,alpha), Color(50,50,50,alpha*1.5),1)
	local power = ent.PowerReq
	local printname = ent.PrintName
	if ent:GetClass() == 'base_printer'then
	 	printname = ent:GetNWString("PrintName")
		power = ent:GetPowerReq()
	end
	local upgradestring = ""
	local upgrade = ent:GetUpgrade()
	if upgrade==1 then
		upgradestring= "I"
	elseif upgrade==2 then
		upgradestring = "II"
	elseif upgrade==3 then
		upgradestring = "III"
	elseif upgrade==4 then
		upgradestring = "IV"
	elseif upgrade==5 then
		upgradestring = "V"
	elseif upgrade==6 then
		upgradestring = "VI"
	elseif upgrade==7 then
		upgradestring = "VII"
	elseif upgrade==8 then
		upgradestring = "VIII"
	elseif upgrade==9 then
		upgradestring = "IX"
	elseif upgrade==10 then
		upgradestring = "X"
	end
	draw.DrawText(printname, "TargetID", pos.x-10,pos.y-50, Color(100,150,200,alpha*2.55),1)
	draw.RoundedBox(0,pos.x-100,pos.y-28, 200, 1, Color(50,50,50,alpha*1.5))
	draw.RoundedBox(0,pos.x-100,pos.y+27, 200, 1, Color(50,50,50,alpha*1.5))

	if upgrade>0 then
		draw.DrawText(upgradestring, "Default", pos.x+90,pos.y-46, Color(200,200,0,alpha*2.55),1)
	end

	if ent:GetClass() != "powerplant" && power > 0 then
		local pwr = false
		if power > ent:GetPower() then
			pwr = true
		end
		if pwr then
			draw.DrawText("Low power!", "TargetID", pos.x-60,pos.y+30, Color(150,0,0,alpha*2.55),1)
			draw.DrawText( ent:GetPower().."/"..power, "HUDNumber", pos.x-5, pos.y-20, Color(150,0,0,alpha*2.55), 1 );
		else
			draw.DrawText( ent:GetPower().."/"..power, "HUDNumber", pos.x-5, pos.y-20, Color(0,150,0,alpha*2.55), 1 );
		end
		if ent:GetClass()=="gunfactory" && ent:GetTable().TimeToFinish!=nil && ent:GetTable().TimeToFinish>CurTime() then
			draw.DrawText(tostring(math.ceil(ent:GetTable().TimeToFinish-CurTime())), "TargetID", pos.x+80,pos.y, Color(150,150,150,alpha*2.55),1)
		end
	elseif ent:GetClass()=="powerplant" then
		local pwrtaken = ent:GetTakenSockets()
		draw.DrawText( tostring(ent:MaxSlots()-pwrtaken).."/" .. ent:MaxSlots(), "HUDNumber", pos.x-5, pos.y-20, Color(200,200,200,alpha*2.55), 1)
	end
end

function DrawDisplay()
	local tr = LocalPlayer():GetEyeTrace();
	for k, v in pairs( ents.FindByClass("bigbomb") ) do

		DrawBombInfo( v );

	end
	for k, v in pairs( ents.FindByClass("money_printer_mythril") ) do

		DrawMythrilInfo( v );

	end
	for k, v in pairs( ents.FindByClass("money_printer_adamantite") ) do

		DrawAdamantiteInfo( v );

	end
	for k, v in pairs( ents.FindByClass("auto_turret") ) do

		DrawTurretInfo( v );

	end
	DrawScans()

	if !IsValid(tr.Entity) && IsValid(viewpl) && viewpltime>CurTime() && LocalPlayer():GetNWBool("scannered") then
		DrawPlayerInfo(viewpl,LocalPlayer():GetNWBool("scannered"))
	end
	if( tr.Entity!=nil and tr.Entity:IsValid() and tr.Entity:GetPos():Distance( LocalPlayer():GetPos() ) <= 768 ) then

		if( tr.Entity:IsPlayer() ) then
			viewpl = tr.Entity
			viewpltime = CurTime()+.5
			local scanner = LocalPlayer():GetNWBool("scannered")
			if(tr.Entity:GetPos():Distance(LocalPlayer():GetPos())<512 || scanner) then
				DrawPlayerInfo( tr.Entity,scanner );
			end

		elseif (tr.Entity:GetTable().Structure) then
			viewpltime = 0
			if viewstructure==tr.Entity then
				viewstructuretime = viewstructuretime+30*FrameTime()
			else
				viewstructure = tr.Entity
				viewstructuretime = 0
			end
			if viewstructuretime>=30 then
				local scanner = LocalPlayer():GetNWBool("scannered")
				if(tr.Entity:GetPos():Distance(LocalPlayer():GetPos())<256) then
					DrawStructureInfo( tr.Entity, math.Clamp((viewstructuretime-30)*5, 0, 100) );
				end
			end
		else
			if IsValid(viewpl) && viewpltime>CurTime() && LocalPlayer():GetNWBool("scannered") then
				DrawPlayerInfo(viewpl,LocalPlayer():GetNWBool("scannered"))
			end
			viewstructure = nil
			viewstructuretime = 0
		end
	end
	DrawRaids()
end

function GM:HUDPaint()

	DrawDrugs()

	self.BaseClass:HUDPaint();
	self:PaintMessages()

end

function GM:HUDDrawTargetID()

end

surface.CreateFont("ArmorFont", {
	font = "ChatFont",
	size = 22,
	weight = 400,
	antialias = true
})

surface.CreateFont("DrugFont", {
	font = "csd",
	size = 32,
	weight = 400,
	antialias = true
})

surface.CreateFont("CSDFont", {
	font = "Counter-Strike",
	size = 32,
	weight = 400,
	antialias = true
})

surface.CreateFont("DrugFont2", {
	font = "HalfLife2",
	size = 50,
	weight = 400,
	antialias = true
})

surface.CreateFont("RealDrugs", {
	font = "Trebuchet MS",
	size = 20,
	weight = 400,
	antialias = true
})


function DrawDrugs()
	local effects = 0
	local x = 5;
	local y = ScrH() - 65;

	// put a little circle at the health bar or some shit to show having snipe shield
	// better yet, a cs armor logo.
	if (LocalPlayer():GetNWBool("shielded")) then
		draw.DrawText( "p", "DrugFont", x+129, y-109, Color(  0,  0,  0,200), 0)--49
		draw.DrawText( "p", "DrugFont", x+128, y-110, Color(255,255,255,255),0) --50
	end
	if (LocalPlayer():GetNWBool("helmeted")) then
		draw.DrawText( "E", "DrugFont", x+149, y-109, Color(  0,  0,  0,200), 0)--49
		draw.DrawText( "E", "DrugFont", x+148, y-110, Color(255,255,255,255),0) --50
	end
	if (LocalPlayer():GetNWBool("poisoned")) then
		draw.DrawText("C", "DrugFont", x+93, y-94, Color( 0,0,  0,200), 0)--39
		draw.DrawText("C", "DrugFont", x+92, y-95, Color(50,250,0,255),0) --40
	end
	if (LocalPlayer():GetNWBool("scannered")) then
		draw.DrawText( "H", "DrugFont", x+132, y-89, Color(  0,  0,  0,200), 0) --29
		draw.DrawText( "H", "DrugFont", x+131, y-90, Color(255,255,255,255),0)  --30
	end
	if (LocalPlayer():GetNWBool("tooled")) then
		draw.DrawText( "f", "CSDFont", x+153, y-96, Color(  0,  0,  0,200), 0) --34
		draw.DrawText( "f", "CSDFont", x+154, y-97, Color(255,255,255,255),0) ---35
	end
	if (LocalPlayer():GetNWBool("nightvisioned")) then
		draw.DrawText( "s", "CSDFont",x+169, y-94, Color(0,0,0,200),0) ------59
		draw.DrawText( "s", "CSDFont",x+168, y-95, Color(255,255,255,255),0) -----60
	end

	y = y-100
	for k,v in pairs(drugTable) do
		draw.DrawText( v.Class, "RealDrugs",x, y, v.Color,0)
		y = y-20
	end

end

function DrawDrugTrail(color, ent, effect)
	local type = effect or "drug_trail"
	if type==none then return end
	local r = color.r
	local g = color.g
	local b = color.b
	if type=="drug_bolt" then
		local gun = ent:GetActiveWeapon()
		if IsValid(gun) then
			local attachpos = gun:GetAttachment(1)
			if attachpos!=nil then
				local effectdata = EffectData()
					effectdata:SetOrigin( attachpos.Pos )
					effectdata:SetStart( Vector( r, g, b ) )
					effectdata:SetEntity( ent )
					effectdata:SetNormal(ent:GetAimVector())
				util.Effect( effect, effectdata )
			end
		end
	else
		local effectdata = EffectData()
			effectdata:SetOrigin( ent:GetPos() )
			effectdata:SetStart( Vector( r, g, b ) )
			effectdata:SetRadius(64)
			effectdata:SetEntity( ent )
		util.Effect( effect, effectdata )
	end
end

LetterY = 0;
LetterAlpha = -1;
LetterMsg = "";
LetterType = 0;
LetterStartTime = 0;
LetterPos = Vector( 0, 0, 0 );

function GM:Think()

end

function ToggleClicker()
	vgui.Create("BW_bmenu")
end
usermessage.Hook( "ToggleClicker", ToggleClicker );


/*==========================================================
==							  ==
==							  ==
==========================================================*/

GVGUI = { }
PanelNumg = 0;

function TestDrawHud()

local struc = {}
struc.pos = {}
struc.pos[1] = ScrW()*.5 -- x pos
struc.pos[2] = ScrH()*.005 -- y pos
struc.color = Color(0,0,0,255)
struc.text = "Basewars v 1.5" -- Text
struc.font = "DefaultFixed" -- Font
struc.xalign = TEXT_ALIGN_CENTER -- Horizontal Alignment
struc.yalign = TEXT_ALIGN_CENTER -- Vertical Alignment
draw.Text( struc )

end
hook.Add("HUDPaint", "HUD_TEST", TestDrawHud)

language.Add("CombineCannon_ammo", "Flamethrower Fuel")
language.Add("SniperRound_ammo", "Sniper Rounds")
language.Add("SMG_ammo", "Rifle Rounds")
language.Add("Pistol_ammo", "Pistol Ammo")
language.Add("SLAM_ammo", "Grenades")
language.Add("XBowBolt_ammo", "Dart Gun Bolts")
language.Add("env_fire", "Fire")
language.Add("env_physexplosion", "The Game")
language.Add("entityflame", "Flames")
language.Add("env_explosion", "Explosion")
language.Add("SBoxLimit_magnets", "You have hit the Magnet limit!")

/*---------------------------------------------------------
  Tribe menu
---------------------------------------------------------*/
function GM.OpenTribeMenu()
	vgui.Create("BW_TribeMenu")
end

concommand.Add("bw_factionmenu",GM.OpenTribeMenu)

/*---------------------------------------------------------
   Tribe system
---------------------------------------------------------*/

function GM.ReceiveTribe(data)
	local name = data:ReadString()
	local id = data:ReadShort()
	local red = data:ReadShort()
	local green = data:ReadShort()
	local blue = data:ReadShort()
	team.SetUp(id,tostring(name),Color(red,green,blue,255))
end
usermessage.Hook("newTribe",GM.ReceiveTribe)

function chaching()
	surface.PlaySound( "tfc/chaching.mp3" )
end
usermessage.Hook("chaching", chaching)

function Upgradegui()
	Master = vgui.Create("DFrame")
	Master:SetPos(ScrW()/2-200, ScrH()/2-150)
	Master:SetSize(400, 340)
	Master:SetTitle("Upgrading")
	Master:SetVisible(true)
	Master:SetDeleteOnClose(true)
	Master:MakePopup()

	List = vgui.Create("DListView", Master)
	List:SetPos(10,30)
	List:SetSize(380, 260)
	List:Clear()
	List:SetMultiSelect(true)
	List:AddColumn("Entity")
	List:AddColumn("Level")
	List:AddColumn("Max Level")

	for k,v in pairs(ents.FindInSphere(LocalPlayer():GetPos(), 500)) do
		if v.Structure && v.MaxUpgrade > 0 then
			if v:GetBWOwner() == LocalPlayer() then
				local vname = v.PrintName
				if vname == "Base Printer" then vname = v:GetNWString("PrintName") end
				List:AddLine(vname, v:GetUpgrade(), v.MaxUpgrade, v:EntIndex())
			end
		end
	end

	List.DoDoubleClick = function(parent, index, line)
		LocalPlayer():ConCommand("bw_upgrade " .. line:GetValue(4))
		timer.Simple(0.5, function()
			List:Clear()
			for k,v in pairs(ents.FindInSphere(LocalPlayer():GetPos(), 500)) do
				if v.Structure && v.MaxUpgrade > 0 then
					if v:GetBWOwner() == LocalPlayer() then
						local vname = v.PrintName
						if vname == "Base Printer" then vname = v:GetNWString("PrintName") end
						List:AddLine(vname, v:GetUpgrade(), v.MaxUpgrade, v:EntIndex())
					end
				end
			end
		end)
	end

	Upgrade = vgui.Create("DButton", Master)
	Upgrade:SetPos(100, 290)
	Upgrade:SetSize(200, 49)
	Upgrade:SetText("Upgrade Selected")

	Upgrade.DoClick = function(parent, index, line)
		for k,v in pairs(List:GetSelected()) do
			LocalPlayer():ConCommand("bw_upgrade " .. v:GetValue(4))
		end
		timer.Simple(0.5, function()
			List:Clear()
			for k,v in pairs(ents.FindInSphere(LocalPlayer():GetPos(), 500)) do
				if v.Structure && v.MaxUpgrade > 0 then
					if v:GetBWOwner() == LocalPlayer() then
						local vname = v.PrintName
						if vname == "Base Printer" then vname = v:GetNWString("PrintName") end
						List:AddLine(vname, v:GetUpgrade(), v.MaxUpgrade, v:EntIndex())
					end
				end
			end
		end)
	end
end
usermessage.Hook("Upgradegui", Upgradegui)

function RunCommand(umsg)
    LocalPlayer():ConCommand(umsg:ReadString())
end
usermessage.Hook("RunCommand", RunCommand)

function GetMoneys(l)
	allPlayersMoney = net.ReadTable()
end
net.Receive("GiveMoneys", GetMoneys)

function StartDrug(l)
	local drug = net.ReadTable()
	drugTable[drug.Class] = drug
end
net.Receive("StartDrug", StartDrug)

function EndDrug(l)
	local drug = net.ReadTable()
	drugTable[drug.Class] = nil
end
net.Receive("EndDrug", EndDrug)

function removeOldTabls()
     for k, v in pairs( g_SpawnMenu.CreateMenu.Items ) do
        if (v.Tab:GetText() == language.GetPhrase("spawnmenu.category.npcs") or
            v.Tab:GetText() == language.GetPhrase("spawnmenu.category.entities") or
            v.Tab:GetText() == language.GetPhrase("spawnmenu.category.weapons") or
            v.Tab:GetText() == language.GetPhrase("spawnmenu.category.vehicles") or
            v.Tab:GetText() == language.GetPhrase("spawnmenu.category.postprocess") or
            v.Tab:GetText() == language.GetPhrase("spawnmenu.category.dupes") or
            v.Tab:GetText() == language.GetPhrase("spawnmenu.category.saves")) then
            g_SpawnMenu.CreateMenu:CloseTab( v.Tab, true )
            removeOldTabls()
        end
    end
end
hook.Add("SpawnMenuOpen", "blockmenutabs", removeOldTabls)
