// take quotes out of things. probably couldve done better.
function Purify ( strng )
	if (string.find(tostring(strng), [[%"]])) then
		strng = " "
	end
	if (string.find(tostring(strng), [[\n]])) then
		strng = " "
	end
	return strng
end

function Notify( ply, msgtype, len, msg )
	umsg.Start("MainMessage", ply)
		umsg.String(msg)
		umsg.Short((msgtype*10)+len)
	umsg.End()
end

--[[
	Send Notification
	Sends a notification to target player.
	New Notify method.
	ply - The target player.
	msgtype - The type of message.
	len - The time to display the message.
	msg - The message itself.
--]]
function SendNotification( ply, msgtype, len, msg )
	net.Start("Notification")
		net.WriteInt(msgtype, 4)
		net.WriteInt(len, 8)
		net.WriteString(msg)
	net.Send(ply)
end

function NotifyPayday( ply, amt, afk )
	if !afk then
		umsg.Start("PaydayMessage", ply)
			umsg.Short(amt)
		umsg.End()
	else
		umsg.Start("AFKMessage", ply)
		umsg.End()
	end
end

function NotifyPrinter( ply, printer , printed, amt)
	net.Start("PrinterMessage")
		net.WriteInt(printer, 5)
		net.WriteInt(printed, 5)
		net.WriteString(amt)
	net.Send(ply)
end

function NotifyAll( msgtype, len, msg )
	for k, v in pairs( player.GetAll() ) do
		Notify( v, msgtype, len, msg );
	end
end

function PrintMessageAll( msgtype, msg )
	for k, v in pairs( player.GetAll() ) do
		v:PrintMessage( msgtype, msg );
	end
end

function FindPlayer( info )
	for k, v in pairs( player.GetAll() ) do
		if( tonumber( info ) == v:EntIndex() ) || ( info == v:SteamID() ) || ( string.find( string.lower(v:Nick()), string.lower(tostring(info)) ) != nil ) then
			return v;
		end
	end
	return nil;
end

function SpyScan(ply,target)
	Notify(ply,2,3,"Printing information on scan target in your console")
	local weapon = "Nothing"
	if IsValid(target:GetActiveWeapon()) then
		weapon = target:GetActiveWeapon():GetClass()
	end
	ply:PrintMessage(2, "\n" ..target:GetName() .. "\n" .. target:Health() .. "/" .. target:GetMaxHealth() .. " Health and " .. target:Armor() .. "/100 Armor\nHolding weapon: " .. weapon .. "\nOther weapons: \n")
	for k,v in pairs(target:GetWeapons()) do
		if v!=target:GetActiveWeapon() then
			ply:PrintMessage(2, v:GetClass() .. "\n")
		end
	end
	local shield = ""
	local helmet = ""
	local raidable = " is not "
	local canRaid = false
	if target:GetNWBool("shielded") then shield = "shield " end
	if target:GetNWBool("helmeted") then helmet = "helmet " end
	if helmet == "" && shield == "" then shield = "nothing" end
	ply:PrintMessage(2, "Equipped: " .. shield .. helmet .. "\n\n")
	for k,v in pairs(ents.FindInSphere(target:GetPos(), 512)) do
		if v.Structure && v:GetBWOwner() == target then
			if string.StartWith(v:GetClass(), "meth") ||
			string.StartWith(v:GetClass(), "drug_l") ||
			string.StartWith(v:GetClass(), "gunfact") ||
			(v:GetClass() == "base_printer" && v.printNum > 1) then
				raidable = " is "
				canRaid = true
			end
		end
	end
	if(canRaid) then
		//going to add a menu to start raid here
		Notify(ply,3,3,target:GetName()..raidable.."raidable")
		ply:ChatPrint(target:GetName()..raidable.."raidable")
	else
		Notify(ply,3,3,target:GetName()..raidable.."raidable")
		ply:ChatPrint(target:GetName()..raidable.."raidable")
	end
	return canRaid
end

function ReconScan(ply, target)
	ply:ChatPrint("Printing a list of things found in scan to your console")
	local scanpos = target:GetPos()
	local stuff = 0
	ply:PrintMessage(2,"\n")
	for k, v in pairs(ents.FindInSphere(scanpos, 512)) do
		if IsValid(v) then
			if v:GetTable().Structure then
				stuff = stuff+1
				ply:PrintMessage(2, v:GetTable().PrintName .. " " .. v:GetBWOwner():GetName())
			end
		end
	end
	ply:PrintMessage(2,"\n")
	if stuff>0 then
		Notify(ply,3,3,"Scan has found " .. tostring(stuff) .. " structures near " .. target:GetName().."")
	else
		Notify(ply,4,3,"Scan has found nothing")
	end
end

local uberexists=false
function UberDrugExists()
	if !uberexists then
		NotifyAll(1,5,"Someone has created an UberDrug!")
		uberexists=true
	end
end

function ccBuyDrugs( ply, command, args )
	local drug = args[1]
	if ply:GetTable().LastBuy+1.5<CurTime() then
		ply:GetTable().LastBuy=CurTime()
		if drug == "health" || drug == "helmet" || drug == "scanner" || drug == "toolkit" || drug == "armor" || drug == "snipeshield" then
			BuyItems(ply, drug)
    elseif drug=="currentammo" then
			BuyAmmo(ply)
		end
	end
end
concommand.Add( "buydrug", ccBuyDrugs );

function ccBuySpecial( ply, command, args )
	local building = args[1]
	if ply:GetTable().LastBuy+1.5<CurTime() then
		ply:GetTable().LastBuy=CurTime()
		if( building == "knife" ) then
			BuyKnife(ply)
		elseif( building == "pipebomb" ) then
			BuyPBomb(ply)
		elseif( building == "welder" ) then
			BuyWelder(ply)
		elseif( building == "bigbomb" ) then
			BuyBomb(ply)
		elseif( building == "gunvault" ) then
			BuyGunvault(ply)
		elseif( building == "fueltank" ) then
			BuyIncedAmmo(ply)
		elseif( building == "sentry") then
			BuyTurret(ply)
		elseif( building == "spawn") then
			BuySpawn(ply)
		end
	end
end
concommand.Add( "buyspecial", ccBuySpecial );

function GiveMoneys(len, ply)
	if IsRanked(ply) then
		moneys = {}
		for k,v in pairs(player.GetAll()) do
			moneys[v:SteamID()] = v:GetTable().Money
		end
		net.Start("GiveMoneys")
			net.WriteTable(moneys)
		net.Send(ply)
	end
end
net.Receive("RequestMoneys", GiveMoneys)
