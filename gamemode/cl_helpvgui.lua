//TODO: This is for Jet. Rewrite this HTML to display the text you want. IE. For your specific server.
local helphtml =
[[
<html>
<body bgcolor="#C3C3C3" lang=EN-US style="background-color:#C3C3C3;text-align:center">

<style>
p
{
font-family:"Courier New";
font-size:10pt;
}
table
{
font-family:"Courier New";
font-size:10pt;
}
h2
{
font-family:tahoma;
font-size:16pt;
font-style:bold;
}
h3
{
font-family:tahoma;
font-size:14pt;
font-style:bold;
color:#ff0000;
}
h1
{
font-family:tahoma;
font-size:20pt;
text-align:center;
font-style:bold;
}
</style>

<h1>The Fabulous Clan Base Wars Help</h1>
<hr>
<h2>Spawn Menu</h2>
<hr>
<p>In the spawn menu(default F3) there are two custom menus. To the left
you click on the tab that says Basewars Buy Menu to buy weapons
and structures. On the right you click Basewars Options to set your
allies. Also to the right of your Q menu is the Utilities tab. Click on that
one to set your prop protection and to undo things.</p>
<hr>
<h2>Structures</h2>
<hr>
<p>Printers are the main way of earning money in basewars. You will need
them to get any money at all. To spawn a printer, go in the shop.
The money printer needs to be near a money vault in order to print.
The money will go directly to the money vault. Over a set amount of time,
the printer(s) will become inactive. Press use(default E) on them to
re-activate them.</p>
<hr>
<h2>Chat Commands</h2>
<hr>
<table border="1" align="center">
<tr><td>/scan</td><td>Opens scan menu</td></tr>
<tr><td>/dropmoney (amount)</td><td>Drops amount of money</td></tr>
<tr><td>/upgradetrace</td><td>Upgrades the structure you are looking at</td></tr>
<tr><td>/upgrade</td><td>Opens the upgrade menu</td></tr>
<tr><td>@(text)</td><td>Sends (text) to the admins</td></tr>
<tr><td>!p playername message</td><td>Sends message to player</td></tr>
</table>

</body>

</html>
]]

local ruleshtml =
[[
<html>

<body bgcolor="#C3C3C3" lang=EN-US style="background-color:#C3C3C3;text-align:center">

<style>
ol
{
font-family:"Courier New";
font-size:10pt;
text-align:left;
}
h2
{
font-family:tahoma;
font-size:16pt;
font-style:bold;
}
h1
{
font-family:tahoma;
font-size:20pt;
text-align:center;
font-style:bold;
}
</style>
<h1>The Fabulous Clan Base Wars Rules</h1>
<hr>
<h2>Basic Rules (Forums Outdated)</h2>
<hr>
<ol>
<li>Do not spam props, ropes, or anything else no matter what(except scanning).</li>
<li>Do not prop block anything EXCEPT your base.</li>
<li>Do not trap players. If you do trap them by accident, let them out immediately.</li>
<li>Do not hack or exploit.</li>
<li>Do not spawn camp.</li>
<li>Do not build while being raided, blowtorched, or bombed.</li>
<li>Listen to all admins and staff.</li>
<li>Do not block off large parts of the map. You must be basing in AT LEAST 75% of the
blocked area to be legal.</li>
<li>You can use curse words, just don't fucking over do it!</li>
<li>Report admin abuse on the <a href="http://tfcbasewars.boards.net/">forums</a>. Pictures would be helpful.</li>
<li>Report any and all bugs on the <a href="http://tfcbasewars.boards.net/">forums</a>.</li>
<li>Do not ask for moderator or admin! Apply on the <a href="http://tfcbasewars.boards.net/">forums!</a></li>
<li>Do not kill people randomly! This is called RDM!</li>
<li>Do not advertise other servers!</li>
<li>This is not Dark RP! YOU CANNOT MUG PEOPLE!</li>
<li>Do not place turrets out where they can shoot people! Even if they are covered by
a fading prop!</li>
<li>Do not use fading doors as a shield! If you open a fading door to shoot someone,</li>
keep it open for five or more seconds or it will be counted as fading door abuse!</li>
<li>Remember 2 wrongs DO NOT make a right, talk to an Admin or Mod to solve your problem.</li>
<li>Do not use Wire Components as protection for your base.</li>
</ol>
<hr>
<h2>Raiding Rules</h2>
<hr>
<ol>
<li>You must scan before raiding.</li>
<li>You must be raidable.</li>
<li>THE SCAN DOES NOT LIE. If it says they are raidable, they are raidable.</li>
<li>If you are accused for illeagal raiding and an admin is trying to sort it out, </li>
prove that you scanned by pasting the scanlog in chat.</li>
<li>If you are being raided, DO NOT destroy anything.</li>
<li>DO NOT build during a raid.</li>
<li>Building is moving ANYTHING with a phys gun or grav cannon.</li>
<li>If you place a big bomb and it is defused, the raid is OVER.</li>
<li>You must wait 10 minutes after a raid to raid again.</li>
<li>Raids can only last 10 minutes.</li>
</ol>
</body>

</html>
]]


local guideshtml =
[[
<html>

<body bgcolor="#C3C3C3" lang=EN-US style="background-color:#C3C3C3;text-align:center">

<style>
p
{
font-family:"Courier New";
font-size:10pt;
}
h2
{
font-family:tahoma;
font-size:16pt;
font-style:bold;
}
h1
{
font-family:tahoma;
font-size:20pt;
text-align:center;
font-style:bold;
}
</style>


<h1>The Fabulous Clan Base Wars Guide</h1>
<hr>
<h2>Basing Guide</h2>
<hr>
<p>The goal of Basewars is to make money(duh!) There are multiple ways to make money
by basing. The best way is printers. Press F3 and go to Basewars Buy Menu tab, scroll
down to Structures-Profitable and click on the icon to buy it. Once you have a printer,
you have to get something to put the cash it prints. Buy a bank or money vault, which is
also in the buy menu, and put it near you printer. After that, you need a source of power,
buy a generator in the menu. After all of this, you will start printing money. There are
also more ways of making money, but we will leave this up to you to find out.</p>
<hr>
<h2>Raiding Guide</h2>
<hr>
<p>You must be raidable to raid or be raided. In order to raid
someone, you need to have a radar tower to scan people. To scan someone, type /scan
then double click their name. On the top left, it will say if they are raidable or not.
IF IT SAYS NOT RAIDABLE DO NOT RAID THEM!!!! Then you may raid them, up to 10 minutes of
raid time are allowed. If the other team fends you off for 10 minutes or defuses
your big bomb then the raid is over. If this happens you must wait 10 minutes before
raiding again. Use this time to make drugs to strengthen your next raid.</p>
</body>

</html>
]]

local faqshtml =
[[
<html>

<body bgcolor="#C3C3C3" lang=EN-US style="background-color:#C3C3C3;text-align:center">

<style>
p
{
font-family:"Courier New";
font-size:10pt;
}
h2
{
font-family:tahoma;
font-size:16pt;
font-style:bold;
}
h1
{
font-family:tahoma;
font-size:20pt;
text-align:center;
font-style:bold;
}
</style>

<h1>The Fabulous Clan Base Wars FAQs</h1>
<hr>

<h2>What is the difference between a bank and a money vault?</h2>
    <p>They both store your money. However, the bank gives you a small amount of interest every five minutes.</p>
<hr>
<h2>What can I do with my Swag Dollars?</h2>
    <p>Press F4. That's what you do with your Swag Dollars.</p>
<hr>
<h2>How can I earn Swag Dollars?</h2>
    <p>You get fifteen Swag Dollars every ten minutes. You can also get Swag Dollars by donating. Once we get enough subscribers, we will also do little riddles to give away points.</p>
<hr>
<h2>How do I donate?</h2>
    <p>You can go on the <a href="http://tfcbasewars.boards.net/">website</a> to donate.</p>

</body>

</html>
]]

function DermaHelp()

	local DermaPanel = vgui.Create( "DFrame" )

	DermaPanel:SetPos(((ScrW() - 750) * 0.5),((ScrH() - 600) * 0.5))
	DermaPanel:SetSize(800, 600)
	DermaPanel:SetTitle("GG BaseWars")
	DermaPanel:SetVisible(true)
	DermaPanel:SetDraggable(false)
	DermaPanel:ShowCloseButton(true)
	DermaPanel:MakePopup()

	local PropertySheet = vgui.Create( "DPropertySheet")
	PropertySheet:SetParent( DermaPanel )
	PropertySheet:SetPos( 5, 30 )
	PropertySheet:SetSize( 790, 562 )

	local helmhtml = vgui.Create("HTML")
    helmhtml:StretchToParent(5,5,5,5)
    helmhtml:SetHTML(helphtml or "Error, contents not found")

	local rulehtml = vgui.Create("HTML")
    rulehtml:StretchToParent(5,5,5,5)
    rulehtml:SetHTML(ruleshtml or "Error, contents not found")

	local guidehtml = vgui.Create("HTML")
    guidehtml:StretchToParent(5,5,5,5)
    guidehtml:SetHTML(guideshtml or "Error, contents not found")

	local faqhtml = vgui.Create("HTML")
    faqhtml:StretchToParent(5,5,5,5)
	faqhtml:SetHTML(faqshtml or "Error, contents not found")


    PropertySheet:AddSheet("Help", helmhtml, "icon16/book.png", false, false, "Help")
    PropertySheet:AddSheet("Rules", rulehtml, "icon16/exclamation.png", false, false, "Rules")
    PropertySheet:AddSheet("Guide", guidehtml, "icon16/controller.png", false, false, "Guide")
    PropertySheet:AddSheet("Faq's", faqhtml, "icon16/bell.png", false, false, "FAQ's")
end

local PANEL = {}

function PANEL:Init()
	self.timeOpened = os.time()
	self:SetTitle("Create-A-Faction")
	self:SetKeyboardInputEnabled(true)
	self:SetMouseInputEnabled(true)
	self:MakePopup()
	self:SetSize(ScrW() / 4, 540)
	self:SetPos(ScrW() / 2 - (self:GetWide() / 2), 50)
	self:Center()

	local line = 30
	local tab = 0
	local tjtstring = ""

	local tnamelabel = vgui.Create("DLabel", self)
		tnamelabel:SetPos(5,30)
		tnamelabel:SetText("Faction Name")
		tnamelabel:SizeToContents()

	local tname = vgui.Create("DTextEntry",self)
		tname:SetSize(self:GetWide()-10, 30)
        tname:SetPos(5, 50)

	local tpwlabel = vgui.Create("DLabel", self)
		tpwlabel:SetPos(5,85)
		tpwlabel:SetText("Facton Password")
		tpwlabel:SizeToContents()

	local tpw = vgui.Create("DTextEntry",self)
		tpw:SetSize(self:GetWide()-10, 30)
        tpw:SetPos(5, 105)

	local tcollabel = vgui.Create("DLabel", self)
		tcollabel:SetPos(5,140)
		tcollabel:SetText("Faction Color")
		tcollabel:SizeToContents()

	local tcolor = vgui.Create("DColorMixer",self)
		tcolor:SetSize(self:GetWide()-10, 150)
        tcolor:SetPos(5, 160)

	local button = vgui.Create("DButton",self)
         button:SetSize(self:GetWide()-10, 30)
         button:SetPos(5, 315)
         button:SetText("Create Your Faction!")
		 button.DoClick = function()
			RunConsoleCommand("bw_createfaction", tname:GetValue(), tcolor:GetColor().r, tcolor:GetColor().g, tcolor:GetColor().b, tpw:GetValue())
			self:Close()
		 end

	local tjlabel = vgui.Create("DLabel", self)
		tjlabel:SetPos(5, 350)
		tjlabel:SetText("Join-A-Faction")
		tjlabel:SizeToContents()

	local tjt = vgui.Create("DComboBox", self)
		tjt:SetPos(5, 370)
		tjt:SetSize(self:GetWide()-10, 30)
		tjt:SetValue("Choose Faction")
		for k,v in pairs(team.GetAllTeams()) do
			if (team.GetName(k) != "Baser" and team.NumPlayers(k) > 0) then
				tjt:AddChoice(team.GetName(k))
			end
		end

	local tjpwlabel = vgui.Create("DLabel", self)
		tjpwlabel:SetPos(5,410)
		tjpwlabel:SetText("Faction Password")
		tjpwlabel:SizeToContents()

	local tjpw = vgui.Create("DTextEntry",self)
		tjpw:SetSize(self:GetWide()-10, 30)
        tjpw:SetPos(5, 430)

	local button1 = vgui.Create("DButton",self)
        button1:SetSize(self:GetWide()-10, 30)
        button1:SetPos(5, 470)
        button1:SetText("Join Faction")
		button1.DoClick = function()
			RunConsoleCommand("bw_join", tjt:GetValue(), tjpw:GetValue())
			self:Close()
		end

	local button2 = vgui.Create("DButton",self)
        button2:SetSize(self:GetWide()-10, 30)
        button2:SetPos(5, 505)
        button2:SetText("Leave Current Faction")
		button2.DoClick = function()
			RunConsoleCommand("bw_leave")
			self:Close()
		end
end

function PANEL:Think()
	if os.time() - self.timeOpened >= 1 then
		if input.IsKeyDown(93) then
			self:Close()
		end
	end
end

vgui.Register("BW_TribeMenu",PANEL,"DFrame")
concommand.Add( "helpmenu", DermaHelp )
