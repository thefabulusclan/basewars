function ccDeclareRaid(ply, cmd, args)
	if scanPlayer(ply, cmd, args) then
		DeclareRaid(ply, FindPlayer(args[1]))
	end
end
concommand.Add("bw_raid", ccDeclareRaid)

function DeclareRaid(ply, target)
	if ply == target then
		Notify(ply, 4, 3, "You can't raid yourself.")
		return
	end
	if ply:GetNWInt("RaidingID") != 0 then
		Notify(ply, 4, 3, "You are already raiding someone!")
		return
	end
	if target:GetNWInt("RaidingID") != 0 then
		Notify(ply, 4, 3, "That player is already in a raid!")
		return
	end
	if ply:GetTable().raidableEntities < 1 then
		Notify(ply, 4, 3, "You can't raid if you're not raidable.")
		return
	end
	if target:GetTable().raidableEntities < 1 then
		Notify(ply, 4, 3, "Your target is not raidable.")
		return
	end
	if !ply:IsInBaserFaction() && !ply:GetNWBool("FactionLeader") then
		Notify(ply, 4, 3, "Only the faction leader can declare a faction raid.")
		return
	end
	local newtarget = target
	if !newtarget:IsInBaserFaction() && !newtarget:GetNWBool("FactionLeader") then
		newtarget = GAMEMODE.Tribes[team.GetName(newtarget:Team())].Leader
	end
	local lastRaid = 0
	local factionornot = ""
	if ply:GetTable().raidAttempts[newtarget:Team()] != nil then
		lastRaid = ply:GetTable().raidAttempts[newtarget:Team()] - CurTime()
		factionornot = "faction"
	elseif ply:GetTable().raidAttempts[newtarget:UserID()] != nil then
		lastRaid = ply:GetTable().raidAttempts[newtarget:UserID()] - CurTime()
		factionornot = "player"
	end
	if lastRaid > 0 then
		local minutes = math.floor(lastRaid/60)
		local seconds = math.Round(lastRaid%60)
		local minutestring = ""
		local secondstring = ""
		if minutes > 0 then
			minutestring = minutes .. " minutes"
		end
		if seconds > 0 then
			secondstring = seconds .. " seconds"
			if minutes > 0 then
				secondstring = " and " .. secondstring
			end
		end
		Notify(ply, 4, 3, "You must wait " .. minutestring .. secondstring .. " before raiding that " .. factionornot .. " again.")
		return
	end
	if !newtarget:IsInBaserFaction() then
		ply:GetTable().raidAttempts[newtarget:Team()] = CurTime() + 600
	else
		ply:GetTable().raidAttempts[newtarget:UserID()] = CurTime() + 600
	end
	net.Start("ConfirmRaid")
		net.WriteEntity(ply)
	net.Send(newtarget)

	newtarget:GetTable().raider = ply
end

function ConfirmRaid(len, target, ply)
	if ply == nil then
		ply = net.ReadEntity()
	end
	if target:GetTable().raider != ply then return end
	local raidPlayers = {}
	if !ply:IsInBaserFaction() then
		if !target:IsInBaserFaction() then
			PropagateFactionWar(ply:Team(), target:Team())
		else
			PropagateTeamRaid(target, ply:Team())
		end
	elseif !target:IsInBaserFaction() then
		PropagateTeamRaid(ply, target:Team())
	else
		PropagateRaid(ply, target)
	end
  // This seems like a bad way to do this. What if later we transition faction ownership on disconnect?
  // Then we would have to reset this timer, and that's a pain
  //TODO: Do this better...
  timer.Create(ply:SteamID64() .. "raiding", 300, 1, function() EndRaid(ply) end)
end
net.Receive("ConfirmRaid", ConfirmRaid)

function DeclineRaid(len, target)
	ply = net.ReadEntity()
	if target:GetTable().raider != ply then return end
	local payamount = 0
	if !target:IsInBaserFaction() then
		for k,v in pairs(team.GetPlayers(target:Team())) do
			payamount = payamount + v:NetWorth()
		end
	else
		payamount = target:NetWorth()
	end
	payamount = math.Round(payamount / 20)
	if target:CanAfford(payamount) then
		target:AddMoney(payamount * -1)
		ply:AddMoney(payamount / 10)
		Notify(ply, 4, 3, "Your target has declined the raid!")
		Notify(ply, 4, 3, "You were paid " .. (math.Round(payamount/10)) .. ".")
		Notify(target, 2, 3, "You declined the raid. You have paid " .. (payamount) .. ".")
		target:GetTable().raider = nil
	else
		Notify(target, 4, 3, "You can't afford to decline this raid!")
		NotifyAll(2, 3, target:Name() .. " tried to decline a raid, but can't afford it!")
		ConfirmRaid(len, target, ply)
	end
end
net.Receive("DeclineRaid", DeclineRaid)

function EndRaidCheck(ply)
	if ply:GetNWInt("RaidingID") == 0 then return end
	if !ply:IsInBaserFaction() then
		for k,v in pairs(team.GetPlayers(ply:Team())) do
			if v:GetTable().raidableEntities > 0 then return end
		end
	else
		if ply:GetTable().raidableEntities > 0 then return end
	end
	EndRaid(ply)
end

function EndRaid(ply)
	if ply:GetNWInt("RaidingID") == 0 then return end
	if !ply:IsInBaserFaction() then
		if ply:GetNWBool("IsRaidingFaction") then
			PropagateFactionWarEnd(ply:Team(), ply:GetNWInt("RaidingID"))
		else
			PropagateTeamRaidDeclare(Player(ply:GetNWInt("RaidingID")), ply:Team())
		end
	else
		if ply:GetNWBool("IsRaidingFaction") then
			PropagateTeamRaidVictim(ply, ply:GetNWInt("RaidingID"))
		else
			PropagateRaidEnd(Player(ply:GetNWInt("RaidingID")), ply)
		end
	end
end

--[[
  Raid Propogation
  Each of these methods will make all involved in a raid flagged with a raid value.
  ply - The player initiating the raid
  target - The player being initiated with
  team - The target team
  team1 - The initiating team
  team2 - The targeted team
--]]
function PropagateRaid(ply, target)
	ply:SetNWBool("IsRaidingFaction", false)
	ply:SetNWInt("RaidingID", target:UserID())
	SendNotification(ply, 1, 4, "Raid against " .. target:Name() .. " has begun.")
	target:SetNWBool("IsRaidingFaction", false)
	target:SetNWInt("RaidingID", ply:UserID())
	SendNotification(target, 1, 4, "Raid against " .. ply:Name() .. " has begun.")
end

function PropagateTeamRaid(ply, tteam)
	ply:SetNWBool("IsRaidingFaction", true)
	ply:SetNWInt("RaidingID", tteam)
	SendNotification(ply, 1, 4, "Raid against " .. team.GetName(tteam) .. " has begun.")
	for k,v in pairs(team.GetPlayers(tteam)) do
		v:SetNWBool("IsRaidingFaction", false)
		v:SetNWInt("RaidingID", ply:UserID())
		SendNotification(v, 1, 4, "Raid against " .. ply:Name() .. " has begun.")
	end
end

function PropagateFactionWar(team1, team2)
	for k,v in pairs(team.GetPlayers(team1)) do
		v:SetNWBool("IsRaidingFaction", true)
		v:SetNWInt("RaidingID", team2)
		SendNotification(v, 1, 4, "Raid against " .. team.GetName(team2) .. " has begun.")
	end
	for k,v in pairs(team.GetPlayers(team2)) do
		v:SetNWBool("IsRaidingFaction", true)
		v:SetNWInt("RaidingID", team1)
		SendNotification(v, 1, 4, "Raid against " .. team.GetName(team1) .. " has begun.")
	end
end

--[[
  Raid End Propogation
  Each of these methods will make all involved in a raid flagged with an invalid raid value.
  ply - The player initiating the raid
  target - The player being initiated with
  tteam - The target team
  team1 - The initiating team
  team2 - The targeted team
--]]
function PropagateRaidEnd(ply, target)
	ply:SetNWBool("IsRaidingFaction", false)
	ply:SetNWInt("RaidingID", 0)
	ply:GetTable().raider = nil
	SendNotification(ply, 1, 4, "Raid against " .. target:Name() .. " has ended.")
	if target:GetTable().raider == nil then
		target:GetTable().raidAttempts[ply:UserID()] = target:GetTable().raidAttempts[ply:UserID()] + 600
	end
	target:GetTable().raider = nil
	target:SetNWBool("IsRaidingFaction", false)
	target:SetNWInt("RaidingID", 0)
	SendNotification(target, 1, 4, "Raid against " .. ply:Name() .. " has ended.")
end

function PropagateTeamRaidDeclare(ply, tteam)
	ply:SetNWBool("IsRaidingFaction", false)
	ply:SetNWInt("RaidingID", 0)
	ply:GetTable().raider = nil
	SendNotification(ply, 1, 4, "Raid against " .. team.GetName(tteam) .. " has ended.")
	for k,v in pairs(team.GetPlayers(tteam)) do
		v:SetNWBool("IsRaidingFaction", false)
		v:SetNWInt("RaidingID", 0)
		if v:GetNWBool("FactionLeader") && v:GetTable().raider == nil then
			v:GetTable().raidAttempts[ply:UserID()] = v:GetTable().raidAttempts[ply:UserID()] + 600
		end
		v:GetTable().raider = nil
		SendNotification(v, 1, 4, "Raid against " .. ply:Name() .. " has ended.")
	end
end

function PropagateTeamRaidVictim(ply, tteam)
	ply:SetNWBool("IsRaidingFaction", false)
	ply:SetNWInt("RaidingID", 0)
	if ply:GetTable().raider == nil then
		ply:GetTable().raidAttempts[tteam] = ply:GetTable().raidAttempts[tteam] + 600
	end
	ply:GetTable().raider = nil
	SendNotification(ply, 1, 4, "Raid against " .. team.GetName(tteam) .. " has ended.")
	for k,v in pairs(team.GetPlayers(tteam)) do
		v:SetNWBool("IsRaidingFaction", false)
		v:SetNWInt("RaidingID", 0)
		v:GetTable().raider = nil
		SendNotification(v, 1, 4, "Raid against " .. ply:Name() .. " has ended.")
	end
end

function PropagateFactionWarEnd(team1, team2)
	for k,v in pairs(team.GetPlayers(team1)) do
		v:SetNWBool("IsRaidingFaction", false)
		v:SetNWInt("RaidingID", 0)
		if v:GetNWBool("FactionLeader") && v:GetTable().raider == nil then
			v:GetTable().raidAttempts[team2] = v:GetTable().raidAttempts[team2] + 600
		end
		v:GetTable().raider = nil
		SendNotification(v, 1, 4, "Raid against " .. team.GetName(team2) .. " has ended.")
	end
	for k,v in pairs(team.GetPlayers(team2)) do
		v:SetNWBool("IsRaidingFaction", false)
		v:SetNWInt("RaidingID", 0)
		v:GetTable().raider = nil
		SendNotification(v, 1, 4, "Raid against " .. team.GetName(team1) .. " has ended.")
	end
end
