-------------
-- Argent Dawn Basewars
-- By: Dadadah
-- Started March 18, 2013
-- Ended March 25, 2015
-------------
-------------
-- Basewars
-- By: Dadadah, Oggal, Leo1997
-- Started January 7th, 2017
-- Or maybe October 2nd, 2017?
-------------

-- Net Strings caching:
util.AddNetworkString( "RequestMoneys" )
util.AddNetworkString( "GiveMoneys" )
util.AddNetworkString( "MoneyChange" )
util.AddNetworkString( "PrinterMessage" )
util.AddNetworkString( "StartDrug" )
util.AddNetworkString( "EndDrug" )
util.AddNetworkString( "ConfirmRaid" )
util.AddNetworkString( "DeclineRaid" )
util.AddNetworkString( "Notification" )

DeriveGamemode( "sandbox" )

AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "cl_msg.lua" )
AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "cl_scoreboard.lua" )
AddCSLuaFile( "cl_helpvgui.lua" )
AddCSLuaFile( "cl_menu.lua" )
AddCSLuaFile( "copypasta.lua" )
AddCSLuaFile( "cl_hud.lua" )
AddCSLuaFile( "tfcskin.lua" )
AddCSLuaFile( "sh_vars.lua" )
AddCSLuaFile( "ranks.lua" )
AddCSLuaFile( "cl_raidui.lua" )

include("physables.lua")

include( "player.lua" )
include( "money.lua" )
include( "shared.lua" )
include( "chat.lua" )
include( "rplol.lua" )
include( "weapons.lua" )
include( "util.lua" )
include( "drugs.lua" )
include( "admincc.lua" )
include( "sh_vars.lua" )
include( "Extracrap.lua" )
include( "ranks.lua" )
include( "raid.lua" )

for k, v in pairs(file.Find(includeName.."/gamemode/weapons/*.lua","LUA")) do AddCSLuaFile("weapons/" .. v); include("weapons/" .. v); end
for k, v in pairs(file.Find(includeName.."/gamemode/structures/*.lua","LUA")) do AddCSLuaFile("structures/" .. v); include("structures/" .. v) end
for k, v in pairs(file.Find(includeName.."/gamemode/drugs/*.lua","LUA")) do AddCSLuaFile("drugs/" .. v); include("drugs/" .. v) end

// FPP----------------------------------------------------
include("fpp/config/mysql.lua")
include("fpp/libraries/mysqlite/mysqlite.lua")
include("fpp/fpp/sh_cppi.lua")
include("fpp/fpp/sh_settings.lua")
include("fpp/fpp/server/defaultblockedmodels.lua")
include("fpp/fpp/server/settings.lua")
include("fpp/fpp/server/core.lua")
include("fpp/fpp/server/antispam.lua")
include("fpp/fpp/server/ownability.lua")

AddCSLuaFile("fpp/fpp/client/menu.lua")
AddCSLuaFile("fpp/fpp/client/hud.lua")
AddCSLuaFile("fpp/fpp/client/buddies.lua")
AddCSLuaFile("fpp/fpp/client/ownability.lua")
AddCSLuaFile("fpp/fpp/sh_cppi.lua")
AddCSLuaFile("fpp/fpp/sh_settings.lua")

if not BlockedModelsExist then
	include("fpp/fpp/server/defaultblockedmodels.lua") -- Load the default blocked models
end

if FPP_MySQLConfig and FPP_MySQLConfig.EnableMySQL then
	hook.Add("DatabaseInitialized", "FPP Init", FPP.Init)
	MySQLite.connectToMySQL(FPP_MySQLConfig.Host, FPP_MySQLConfig.Username, FPP_MySQLConfig.Password, FPP_MySQLConfig.Database_name, FPP_MySQLConfig.Database_port)
else
	FPP.Init()
end
// -------------------------------------------------------

util.PrecacheSound("tfc/chaching.mp3")
util.PrecacheSound("nuclearprinter")

SetGlobalString( "cmdprefix", "/" ); --Prefix before any chat commands

----------------------------------------
----------------------------------------

function GM:Initialize()
	self.BaseClass:Initialize();
	timer.Simple(10, function()
		for k,v in pairs (ents.GetAll()) do
			local class = v:GetClass()
			if class == "prop_door_rotating" || class == "func_door_rotating" || class == "prop_dynamic" || class == "func_door" then
				v:Fire("lock", "", 0)
				v:Fire("unlock", "", 0.1)
			end
		end
	end)
	timer.Create( "Payday", 60, 0, function() payall() end)
	for k, v in pairs( player.GetAll() ) do
		v:NewData();
		GetMoney(v);
	end
end

function GM:ShowHelp( ply )
	ply:ConCommand( "helpmenu" );
end

function GM:ShowTeam( ply )
	ply:ConCommand( "bw_factionmenu" )
end

function GM:ShowSpare1( ply )
 	umsg.Start( "ToggleClicker", ply ); umsg.End();
end

function GM:ShowSpare2( ply )
	ply:ConCommand( "gm_showspare2\n" );
end

function payall()
    for k,v in pairs( player.GetAll() ) do
		if v:GetNWBool("AFK") then
			NotifyPayday(v, 0, true)
			return
		end
        local amount = math.random( 1, 9 );
		amount = amount+((RetrieveBenifits(v)*10))
        v:AddMoney( amount )
        NotifyPayday(v,amount,false);
    end
end

/*---------------------------------------------------------
   Tribe system
---------------------------------------------------------*/

GM.Tribes = {}
GM.NumTribes = 1

function CreateTribeCmd( ply, cmd, args )
	if !args[4] or args[4] == "" then
		ply:ChatPrint("Syntax is: bw_createfaction \"factionname\" red green blue [password(optional)]") return
	end
	if args[5] and args[5] != "" then
		CreateTribe( ply, args[1], args[2], args[3], args[4], args[5] )
	else
		CreateTribe( ply, args[1], args[2], args[3], args[4], "" )
	end
end
concommand.Add( "bw_createfaction", CreateTribeCmd )

function joinTribe( ply, cmd, args )
	local pw = ""
	if !args[1] or args[1] == "" then
		ply:ChatPrint("Syntax is: bw_join \"faction\" [password(if needed)]") return
	end
	if args[2] and args[2] != "" then
		pw = args[2]
	end
	for i,v in pairs(GAMEMODE.Tribes) do
		if string.lower(i) == string.lower(args[1]) then
			if v.Password and v.Password != pw then ply:PrintMessage(3,"Incorrect Faction Password") return end
			leaveTribe(ply, "", "")
			ply:SetTeam(v.id)
			ply:SetNWBool("FactionLeader", false)
			Notify(ply, 0, 3,"Successfully Joined A Faction" )
		end
	end
end
concommand.Add( "bw_join", joinTribe )

function leaveTribe( ply, cmd, args )
	if (ply:GetNWBool("FactionLeader")) then
		table.remove(GAMEMODE.Tribes, table.KeyFromValue(GAMEMODE.Tribes, team.GetName(ply:Team())))
		for k,v in pairs(team.GetPlayers(ply:Team())) do
			if (ply != v) then
				leaveTribe(v, "", "")
			end
		end
	end
	ply:SetTeam(1)
	Notify(ply,0,3,"Successfully Left A Faction")
	ply:SetNWBool("FactionLeader", false)
end
concommand.Add( "bw_leave", leaveTribe )

function CreateTribe( ply, name, red, green, blue, password )

	local Password = false

	if password and password != "" then
		Password = password
	end

	GAMEMODE.NumTribes = GAMEMODE.NumTribes + 1
	GAMEMODE.Tribes[name] = {
		id = GAMEMODE.NumTribes,
		Password = Password,
		Leader = ply
	}
	umsg.Start("newTribe")
		umsg.String(name)
		umsg.Short(GAMEMODE.NumTribes)
		umsg.Short(red)
		umsg.Short(green)
		umsg.Short(blue)
	umsg.End()

	team.SetUp(GAMEMODE.NumTribes,tostring(name),Color(red,green,blue,255))
	ply:SetTeam(GAMEMODE.NumTribes)
	ply:ChatPrint("Successfully Created A Faction",5,Color(255,255,255,255))
	ply:SetNWBool("FactionLeader", true)
end

function GM:CanDrive(ply, ent)
	return false
end

function GM:PlayerSpawnSENT( ply, class )
	return false
end

function GM:PlayerSpawnSWEP( ply, class )
	return false
end

function GM:PlayerGiveSWEP( ply, class )
	return false
end
