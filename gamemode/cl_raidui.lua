local raidTime = nil
local drawRaid = 0
local raidingID = 0
local raidingFaction = false
local raidingTargetName = ""

function ConfirmRaid(l)
	local raider = net.ReadEntity()
	local panel = vgui.Create( "DFrame" )
	panel:SetPos( ScrW()/2-200 , ScrH() / 2 - 100 )
	panel:SetName( "Panel" )
	panel:SetSize(400,200)
	panel:SetSizable(false)
	panel:SetTitle("Raid Notification!")
	panel:SetVisible( true )
	panel:MakePopup()
	local warningLabel = vgui.Create("DLabel", panel)
	warningLabel:SetPos(15, 15)
	local name = raider:GetName()
	if raider:Team() != TEAM_CONNECTING && raider:Team() != TEAM_UNASSIGNED && raider:Team() != TEAM_SPECTATOR && raider:Team() != 1 then
		name = team.GetName(raider:Team())
	end
	if LocalPlayer():GetNWBool("FactionLeader") then
		warningLabel:SetText(name .. " has initiated a raid with your faction.")
	else
		warningLabel:SetText(name .. " has initiated a raid with you.")
	end
	warningLabel:SizeToContents();
	local tosLabel = vgui.Create("DLabel", panel)
	tosLabel:SetPos(15, 50)
	tosLabel:SetText("Declining a raid will result in a cost of 5% you or your faction's net worth!\nThe raid will be automatically accepted in 60 seconds!")
	tosLabel:SizeToContents()
	local acceptButton = vgui.Create("DButton", panel)
	acceptButton:SetPos(15, 150)
	acceptButton:SetText("Accept Raid?")
	acceptButton.DoClick = function(msg)
		net.Start("ConfirmRaid")
			net.WriteEntity(raider)
		net.SendToServer()
		timer.Destroy(LocalPlayer():UserID() .. "RaidTimer")
		panel:Close()
	end
	acceptButton:SetVisible(true)
	local declineButton = vgui.Create("DButton", panel)
	declineButton:SetPos(115, 150)
	declineButton:SetText("Decline Raid?")
	declineButton.DoClick = function(msg)
		net.Start("DeclineRaid")
			net.WriteEntity(raider)
		net.SendToServer()
		timer.Destroy(LocalPlayer():UserID() .. "RaidTimer")
		panel:Close()
	end
	declineButton:SetVisible(true)
	timer.Create(LocalPlayer():UserID() .. "RaidTimer", 60, 1, function()
			net.Start("ConfirmRaid")
				net.WriteEntity(raider)
			net.SendToServer()
			panel:Close()
	end)
end
net.Receive("ConfirmRaid", ConfirmRaid)

function DrawRaids()
	local opponentName = ""
	local ourName = LocalPlayer():Name()
	if LocalPlayer():GetNWInt("RaidingID") != 0 then
		if drawRaid < 255 then
			drawRaid = drawRaid + 1
		end
		raidingFaction = LocalPlayer():GetNWBool("IsRaidingFaction")
		raidingID = LocalPlayer():GetNWInt("RaidingID")
		if raidingFaction then
			raidingTargetName = team.GetName(raidingID)
		else
			local opponent = Player(raidingID)
			if opponent == nil then raidingTargetName = "HERPA DERPA" else raidingTargetName = opponent:Name() end
		end
		if raidTime == nil then
			raidTime = CurTime()
		end
	else
		raidTime = nil
		if drawRaid > 0 then
			drawRaid = drawRaid - 1
		end
	end
	if drawRaid <= 0 then
		return
	end
	if LocalPlayer():Team() != 1 then
		ourName = team.GetName(LocalPlayer():Team())
	end
	surface.SetFont( "Default" )
	local normalizedRaidTime = 0
	if raidTime != nil then
		normalizedRaidTime = math.Round(raidTime + 300 - CurTime())
	end
	local raidstring = ourName .. " VS " .. raidingTargetName .. " : " .. normalizedRaidTime
	local stringSize = surface.GetTextSize(raidstring)
	local polyVert = {
		{x = (ScrW()/2) - ((stringSize * 0.5) + 20), y = ScrH()},
		{x = (ScrW()/2) - ((stringSize * 0.5) + 10), y = ScrH() - 30},
		{x = (ScrW()/2) + ((stringSize * 0.5) + 10), y = ScrH() - 30},
		{x = (ScrW()/2) + ((stringSize * 0.5) + 20), y = ScrH()}
	}
	surface.SetDrawColor( 10, 10, 10, drawRaid )
	draw.NoTexture()
	surface.DrawPoly( polyVert )
	surface.SetTextColor( 255, 255, 255, drawRaid )
	surface.SetTextPos((ScrW()/2) - (0.5 * stringSize), ScrH() - 25)
	surface.DrawText(raidstring)
end
