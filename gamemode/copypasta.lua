local shopTable = {}
local gunTable = {}
local drugTable = {}
gunTable["Pistols"] = {}
gunTable["Pistols"].Shipment = false
gunTable["Pistols"].Cmd = "buyweapon"
gunTable["Pistols"].Weapons = {}
gunTable["General"] = {}
gunTable["General"].Shipment = true
gunTable["General"].Cmd = "buyweapon"
gunTable["General"].CmdTwo = "buyshipment"
gunTable["General"].Weapons = {}
gunTable["Grenades"] = {}
gunTable["Grenades"].Shipment = false
gunTable["Grenades"].Cmd = "buyweapon"
gunTable["Grenades"].CmdTwo = "buyshipment"
gunTable["Grenades"].Weapons = {}
gunTable["Equipment"] = {}
gunTable["Equipment"].Shipment = false
gunTable["Equipment"].Cmd = "buydrug"
gunTable["Equipment"].Weapons = {}
gunTable["Other"] = {}
gunTable["Other"].Shipment = false
gunTable["Other"].Cmd = "buyspecial"
gunTable["Other"].Weapons = {}
shopTable["Base Structures"] = {}
shopTable["Base Structures"].Cmd = "buystruct"
shopTable["Base Structures"].items = {}
shopTable["Drug Structures"] = {}
shopTable["Drug Structures"].Cmd = "buystruct"
shopTable["Drug Structures"].items = {}
shopTable["Printers"] = {}
shopTable["Printers"].Cmd = "buyprinter"
shopTable["Printers"].items = {}
shopTable["Other"] = {}
shopTable["Other"].Cmd = "buyspecial"
shopTable["Other"].items = {}
drugTable["Movement"] = {}
drugTable["Movement"].Cmd = "buydrg"
drugTable["Movement"].items = {}
drugTable["Offensive"] = {}
drugTable["Offensive"].Cmd = "buydrg"
drugTable["Offensive"].items = {}
drugTable["Defensive"] = {}
drugTable["Defensive"].Cmd = "buydrg"
drugTable["Defensive"].items = {}
local PANEL = {}

/*---------------------------------------------------------
   Name: Init
---------------------------------------------------------*/
function PANEL:Init()
	local ply = LocalPlayer()
	self.timeOpened = os.time()
	local dps = vgui.Create("DPropertySheet", self)
		dps:Dock(FILL)

	self.PanelList = vgui.Create( "DScrollPanel", dps )
	self.PanelList:SetSize(self.PanelList:GetParent():GetSize())
	self.PanelList:Dock(FILL)

	self.WeaponList = vgui.Create( "DScrollPanel", dps )
	self.WeaponList:SetSize(self.WeaponList:GetParent():GetSize())
	self.WeaponList:Dock(FILL)

	self.DrugList = vgui.Create( "DScrollPanel", dps )
	self.DrugList:SetSize(self.DrugList:GetParent():GetSize())
	self.DrugList:Dock(FILL)

	if IsRanked(ply) && !game.SinglePlayer() then
		self.AdminList = vgui.Create( "DScrollPanel", dps )
		self.AdminList:SetSize(self.AdminList:GetParent():GetSize())
		self.AdminList:Dock(FILL)
		net.Start("RequestMoneys")
		net.SendToServer()
		timer.Simple(0.5, function()
			self:BuildAdminList()
		end)
	end

	self:BuildList()
	self:SetTitle("Basewars Menu")
	self:SetDeleteOnClose(true)
	self:SetKeyboardInputEnabled(true)
	self:SetMouseInputEnabled(true)
	self:MakePopup()
	self:SetSize(ScrW() / 2, 540)
	self:SetPos(ScrW() / 4, 50)
	self:Center()
	dps:AddSheet("Structures", self.PanelList, "icon16/brick.png", false, false, "Structures")
	dps:AddSheet("Weapons", self.WeaponList, "icon16/brick.png", false, false, "Weapons")
	dps:AddSheet("Drugs", self.DrugList, "icon16/brick.png", false, false, "Drugs")
	if IsRanked(ply) then
		dps:AddSheet("Admin", self.AdminList, "icon16/user.png", false, false, "Admin")
	end
end

function PANEL:Think()
	if os.time() - self.timeOpened >= 2 then
		if input.IsKeyDown(94) then
			self:Close()
		end
	end
end

/*---------------------------------------------------------
   Name: BuildList
---------------------------------------------------------*/


function PANEL:BuildList()
	self.PanelList:Clear()

	local i = 0

	for k, v in pairs(shopTable) do

		local cat = vgui.Create("DCollapsibleCategory", self.PanelList)
		cat:SetLabel(k)
		cat:SetCookieName("EntitySpawn" .. k)
		cat:SetSize(ScrW()/2 - 30, 10)
		cat:SetPos(5, i)
		cat.OnToggle = function()
			self:BuildList()
		end

		local cont = vgui.Create("DIconLayout", cat)
		cat:SetContents(cont)

		for k1, v1 in pairs( shopTable[k].items ) do

			local Icon = vgui.Create("SpawnIcon")
			Icon:SetModel(v1.Model)
			Icon:SetToolTip( v1.Name .. "\nCost: $" .. string.Comma(v1.Cost))
			if k == "Printers" then
				Icon.DoClick = function ()
					RunConsoleCommand( shopTable[k].Cmd, v1.Name )
				end
			else
				Icon.DoClick = function ()
					RunConsoleCommand( shopTable[k].Cmd, v1.Class )
				end
			end
			local label  = vgui.Create("DLabel", Icon)
			label:SetFont( "default" )
			label:SetTextColor( color_white )
			label:SetText(v1.Name)
			label:SetContentAlignment( 5 )
			label:SetWide( self:GetWide() )
			label:AlignBottom( -42 )
			cont:Add(Icon)
		end
		if(cat:GetExpanded()) then
			i = i + 20 + 69*math.ceil(#shopTable[k].items / (((ScrW()/2)-30) / 64))
		else
			i = i+20
		end
	end

	self.WeaponList:Clear()

	local i = 0

	for k, v in pairs(gunTable) do

		local cat = vgui.Create("DCollapsibleCategory", self.WeaponList)
		cat:SetLabel(k)
		cat:SetCookieName("EntitySpawn" .. k)
		cat:SetSize(ScrW()/2 - 30, 10)
		cat:SetPos(5, i)
		cat.OnToggle = function()
			self:BuildList()
		end

		local cont = vgui.Create("DIconLayout", cat)
		cat:SetContents(cont)

		for k1, v1 in pairs( gunTable[k].Weapons ) do

			local Icon = vgui.Create("SpawnIcon")
			Icon:SetModel(v1.Model)
			if(gunTable[k].Shipment) then
				Icon:SetToolTip("Item: " .. v1.Name .. "\nSingle Cost: $" .. string.Comma(v1.Cost).."\nShipment Cost: $" .. string.Comma(v1.Cost*3))
				Icon.DoClick = function ()
					local menu1 = DermaMenu()
					menu1:AddOption("Buy Single Weapon", function() RunConsoleCommand(gunTable[k].Cmd, v1.Data) end)
					menu1:AddOption("Buy Shipment", function() RunConsoleCommand( gunTable[k].CmdTwo, v1.Data ) end)
					menu1:Open()
				end
			else
				Icon:SetToolTip( v1.Name .. "\nCost: $" .. string.Comma(v1.Cost))
				Icon.DoClick = function ()
					RunConsoleCommand( gunTable[k].Cmd, v1.Data )
				end
			end
			local label  = vgui.Create("DLabel", Icon)
			label:SetFont( "default" )
			label:SetTextColor( color_white )
			label:SetText(v1.Name)
			label:SetContentAlignment( 5 )
			label:SetWide( self:GetWide() )
			label:AlignBottom( -42 )
			cont:Add(Icon)
		end
		if(cat:GetExpanded()) then
			i = i + 20 + 69*math.ceil(#gunTable[k].Weapons / (((ScrW()/2)-30) / 64))
		else
			i = i+20
		end
	end

	self.DrugList:Clear()

	local i = 0

	for k, v in pairs(drugTable) do

		local cat = vgui.Create("DCollapsibleCategory", self.DrugList)
		cat:SetLabel(k)
		cat:SetCookieName("EntitySpawn" .. k)
		cat:SetSize(ScrW()/2 - 30, 10)
		cat:SetPos(5, i)
		cat.OnToggle = function()
			self:BuildList()
		end

		local cont = vgui.Create("DIconLayout", cat)
		cat:SetContents(cont)

		for k1, v1 in pairs( drugTable[k].items ) do

			local Icon = vgui.Create("SpawnIcon")
			Icon:SetModel(v1.Model)
			Icon:SetToolTip( v1.Class .. "\nCost: $" .. string.Comma(v1.Cost))
			Icon.DoClick = function ()
				RunConsoleCommand( drugTable[k].Cmd, v1.Class )
			end
			local label  = vgui.Create("DLabel", Icon)
			label:SetFont( "default" )
			label:SetTextColor( color_white )
			label:SetText(v1.Class)
			label:SetContentAlignment( 5 )
			label:SetWide( self:GetWide() )
			label:AlignBottom( -42 )
			cont:Add(Icon)
		end
		if(cat:GetExpanded()) then
			i = i + 20 + 69*math.ceil(#drugTable[k].items / (((ScrW()/2)-30) / 64))
		else
			i = i+20
		end
	end
end

function PANEL:BuildAdminList()
	self.AdminList:Clear()

	local plyList = vgui.Create("DListView", self.AdminList)
	plyList:SetMultiSelect(false)
	plyList:SetSize(ScrW()/2 - 11, 477)
	plyList:AddColumn("Name")
	plyList:AddColumn("Steam ID")
	plyList:AddColumn("Faction")
	plyList:AddColumn("Money")

	for k,v in pairs(player.GetAll()) do
		if allPlayersMoney[v:SteamID()] == nil then continue end
		local thatoneplayersmoney = allPlayersMoney[v:SteamID()]
		local thatoneplayersbils = math.floor(thatoneplayersmoney/1000000000)
		thatoneplayersmoney = thatoneplayersmoney - thatoneplayersbils*1000000000
		if thatoneplayersbils > 0 then
			thatoneplayersmoney = string.Comma(thatoneplayersmoney)
			while #thatoneplayersmoney < 11 do
				thatoneplayersmoney = ("0".. thatoneplayersmoney)
			end
			plyList:AddLine(v:GetName(), v:SteamID(), team.GetName(v:Team()), string.Comma(thatoneplayersbils)..","..thatoneplayersmoney )
		else
			plyList:AddLine(v:GetName(), v:SteamID(), team.GetName(v:Team()), string.Comma(thatoneplayersmoney))
		end
	end
	plyList.DoDoubleClick = function(parent, index, line)
		local menu1 = DermaMenu()
			menu1:AddOption("Set Money", function()
				local MoneyWindow = vgui.Create("DFrame")
				MoneyWindow:SetSize(100, 50)
				MoneyWindow:SetTitle("Set Moneys")
				MoneyWindow:Center()
				MoneyWindow:SetVisible(true)
				MoneyWindow:MakePopup()

				local mwTextEntry = vgui.Create("DTextEntry", MoneyWindow)
				mwTextEntry:SetSize(80, 20)
				mwTextEntry:SetPos(10, 20)
				mwTextEntry.OnEnter = function()
					LocalPlayer():ConCommand("bw_setmoney \"" .. line:GetValue(2) .. "\" " .. mwTextEntry:GetValue())
					timer.Simple(0.1, function()
						net.Start("RequestMoneys")
						net.SendToServer()
					end)
					timer.Simple(0.3, function()
						self:BuildAdminList()
					end)
					MoneyWindow:Close()
				end
			end)
			menu1:AddOption("Add Money", function()
				local MoneyWindow = vgui.Create("DFrame")
				MoneyWindow:SetSize(100, 50)
				MoneyWindow:SetTitle("Add Moneys")
				MoneyWindow:Center()
				MoneyWindow:SetVisible(true)
				MoneyWindow:MakePopup()

				local mwTextEntry = vgui.Create("DTextEntry", MoneyWindow)
				mwTextEntry:SetSize(80, 20)
				mwTextEntry:SetPos(10, 20)
				mwTextEntry.OnEnter = function()
					LocalPlayer():ConCommand("bw_addmoney \"" .. line:GetValue(2) .. "\" " ..mwTextEntry:GetValue())
					timer.Simple(0.1, function()
						net.Start("RequestMoneys")
						net.SendToServer()
					end)
					timer.Simple(0.3, function()
						self:BuildAdminList()
					end)
					MoneyWindow:Close()
				end
			end)
			menu1:AddOption("Dump Entities", function() LocalPlayer():ConCommand("bw_entdump " .. "\"" .. line:GetValue(1) .. "\"") end)
			menu1:Open()
	end
end
vgui.Register("BW_bmenu",PANEL,"DFrame")

function AddWeapon(weapon, cat)
	for k,v in pairs(gunTable[cat].Weapons) do
		if (v.Cost > weapon.Cost) then
			table.insert(gunTable[cat].Weapons, k, weapon)
			return
		end
	end
	table.insert(gunTable[cat].Weapons, weapon)
end

function AddStructure(item, cat)
	for k,v in pairs(shopTable[cat].items) do
		if (v.Cost > item.Cost) then
			table.insert(shopTable[cat].items, k, item)
			return
		end
	end
	table.insert(shopTable[cat].items, item)
end

function AddPrinter(item, cat)
	AddStructure(item, cat)
end

function AddDrug(drug, cat)
	if cat == "super" then return end
	for k,v in pairs(drugTable[cat].items) do
		if (v.Cost > drug.Cost) then
			table.insert(drugTable[cat].items, k, drug)
			return
		end
	end
	table.insert(drugTable[cat].items, drug)
end
