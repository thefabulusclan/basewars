drugsIndex = {}

--[[
	Add Drug
	Adds drug drug to the drug index.
	drug - Table representing the drug.
	type - Unused. Type of drug.
--]]
function AddDrug(drug, type)
	drugsIndex[drug.Class] = drug
end

--[[
	Apply Drug
	Applies the drug of class Class to the player ply.
	class - A string representing the requested drug.
	ply - An entity representing the targeted player.
--]]
function ApplyDrug(class, ply)
	if (class == nil || class == "" || drugsIndex[class] == nil) then
		MsgN("Invalid Drug")
		return
	end
	local drug = drugsIndex[class]
	ply:CallDrug(drug)
end

function BuyDrg(ply, cmd, args)
	local class = args[1]
	if (class == nil || class == "" || drugsIndex[class] == nil) then
		MsgN("Invalid Drug")
		return
	end
	if ply:GetTable().LastBuy+1.5<CurTime() then

		local drug = drugsIndex[class]

		if !ply:CanAfford(drug.Cost) then
			Notify(ply, 4, 3, "You cannot afford this.")
			return
		end

		ply:GetTable().LastBuy=CurTime()

		local trace = { }

		trace.start = ply:EyePos();
		trace.endpos = trace.start + ply:GetAimVector() * 85;
		trace.filter = ply;

		local tr = util.TraceLine( trace );

		ply:AddMoney(drug.Cost * -1)

		local drugent = ents.Create("basewars_drug")
		drugent.Owner = ply
		drugent:SetPos(tr.HitPos+Vector(0,0,40));
		drugent:SetColor(drug.Color)
		drugent.drugType = drug.Class
		drugent.DrugEffect = function(caller)
			caller:CallDrug(drug)
		end
		drugent:Spawn();

	end
end
concommand.Add("buydrg", BuyDrg)

--[[
	Get Random Drug
	Returns a random drug table from the drug index.
--]]
function GetRandomDrug()
	local keyset = {}
	for k,v in pairs(drugsIndex) do
    table.insert(keyset, k)
	end
	return drugsIndex[keyset[math.random(1, #keyset)]]
end

// Snipe Shield

function UnShield(ply)
	ply:SetNWBool("shielded", false)
end

// Helmet

function UnHelmet(ply)
	ply:SetNWBool("helmeted", false)
end
