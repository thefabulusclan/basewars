local drug = {}

drug.Class = "Lifesteal"
drug.Model = "models/props_lab/jar01a.mdl"
drug.Color = Color(255,100,100,255)
drug.Cost = 1000
drug.Time = 60
drug.Drug = function(ply)
  ply:SetNWBool("BWDrugLifesteal", true)
end
drug.FinishDrug = function(ply)
  ply:SetNWBool("BWDrugLifesteal", false)
end

AddDrug(drug, "Offensive")

function BWDrugLifestealUp(ply, hitgroup, dmginfo)
  local attacker = dmginfo:GetAttacker()
  if (attacker != nil && attacker:IsPlayer()) then
    if (attacker:GetNWBool("BWDrugLifesteal")) then
      if (attacker:Health() < attacker:GetMaxHealth()) then
        attacker:SetHealth(math.Round(math.max(attacker:Health() + dmginfo:GetDamage()*0.1, attacker:GetMaxHealth())))
      end
    end
  end
end
hook.Add("ScalePlayerDamage", "BWDrugLifesteal", BWDrugLifestealUp)
