local drug = {}

drug.Class = "Amplifier"
drug.Model = "models/props_lab/jar01a.mdl"
drug.Color = Color(100,100,255,255)
drug.Cost = 1000
drug.Time = 60
drug.Drug = function(ply)
  ply:SetNWBool("BWDrugAmped", true)
end
drug.FinishDrug = function(ply)
  ply:SetNWBool("BWDrugAmped", false)
end

AddDrug(drug, "Offensive")

function BWDrugampedUp(ply, hitgroup, dmginfo)
  local attacker = dmginfo:GetAttacker()
  if (attacker != nil && attacker:IsPlayer()) then
    if (attacker:GetNWBool("BWDrugAmped")) then
      dmginfo:ScaleDamage(1.5)
    end
  end
end
hook.Add("ScalePlayerDamage", "BWAmpDamageScale", BWDrugampedUp)
