local drug = {}

drug.Class = "Uber"
drug.Model = "models/props_lab/jar01a.mdl"
drug.Color = Color(100,100,255,255)
drug.Cost = 100000000000
drug.Time = 60
drug.Drug = function(ply)
  local identifier = ply:SteamID64() || "SinglePlayer"
  local drugsToApply = {}
  drugsToApply[1] = "Super Offensive"
  drugsToApply[2] = "Super Defensive"
  drugsToApply[3] = "Super Mobility"
  for i=1,#drugsToApply do
    local superDrug = drugsIndex[drugsToApply[i]]
    for j=1,#superDrug.AppliedDrugs do
      local chosenDrug = drugsIndex[superDrug.AppliedDrugs[j]]
      ply:CallSuperDrug(chosenDrug)
      timer.Destroy(identifier .. "UnDrug" .. chosenDrug.Class)
      timer.Destroy(identifier .. "FinishDrug" .. chosenDrug.Class)
    end
  end
end

AddDrug(drug, "super")
