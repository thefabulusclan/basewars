local drug = {}

drug.Class = "Thick Skin"
drug.Model = "models/props_lab/jar01a.mdl"
drug.Color = Color(255,255,255,255)
drug.Cost = 1000
drug.Time = 60
drug.Drug = function(ply)
  ply:SetNWBool("BWDrugSkin", true)
end
drug.FinishDrug = function(ply)
  ply:SetNWBool("BWDrugSkin", false)
end

AddDrug(drug, "Defensive")

function BWDrugThickSkin(ply, hitgroup, dmginfo)
  if (ply:GetNWBool("BWDrugSkin")) then
    dmginfo:ScaleDamage(0.5)
  end
end
hook.Add("ScalePlayerDamage", "BWDrugThickSkin", BWDrugThickSkin)
