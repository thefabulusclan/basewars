local drug = {}

drug.Class = "Jump Boost"
drug.Model = "models/props_lab/jar01a.mdl"
drug.Color = Color(100,100,100,255)
drug.Cost = 1000
drug.Time = 60
drug.Drug = function(ply)
  ply:SetJumpPower(400)
end
drug.FinishDrug = function(ply)
  ply:SetJumpPower(200)
end

AddDrug(drug, "Movement")
