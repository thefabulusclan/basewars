local drug = {}

drug.Class = "Adrenaline"
drug.Model = "models/props_lab/jar01a.mdl"
drug.Color = Color(100,0,0,255)
drug.Cost = 1000
drug.Time = 60
drug.Drug = function(ply)
  GAMEMODE:SetPlayerSpeed(ply, 300, 600)
end
drug.UnDrug = function(ply)
  GAMEMODE:SetPlayerSpeed(ply, 225, 450)
end
drug.FinishDrug = function(ply)
  GAMEMODE:SetPlayerSpeed(ply, 250, 500)
end

AddDrug(drug, "Movement")
