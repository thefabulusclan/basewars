local drug = {}

drug.Class = "Regeneration"
drug.Model = "models/props_lab/jar01a.mdl"
drug.Color = Color(0,100,0,255)
drug.Cost = 1000
drug.Time = 60
drug.Drug = function(ply)
	local identifier = ply:SteamID64() || "SinglePlayer"
  timer.Create(identifier .. "Regen", 1, 0, function()
    ply:SetHealth(math.Round(math.min(ply:Health() + 5, ply:GetMaxHealth())))
  end)
end
drug.FinishDrug = function(ply)
	local identifier = ply:SteamID64() || "SinglePlayer"
  timer.Remove(identifier .. "Regen")
end

AddDrug(drug, "Defensive")
