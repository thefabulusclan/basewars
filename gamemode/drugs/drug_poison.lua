local drug = {}

drug.Class = "Poison"
drug.Model = "models/props_lab/jar01a.mdl"
drug.Color = Color(0,100,0,255)
drug.Cost = 1000
drug.Time = 40
drug.Drug = function(ply)
	local identifier = ply:SteamID64() || "SinglePlayer"
  timer.Create(identifier .. "Poison", 1, 0, function()
    ply:TakeDamage(2, ply, ply)
  end)
end
drug.FinishDrug = function(ply)
	local identifier = ply:SteamID64() || "SinglePlayer"
  timer.Remove(identifier .. "Poison")
end

AddDrug(drug, "Offensive")
