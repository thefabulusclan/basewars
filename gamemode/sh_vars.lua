CfgVars = { }

// Drug costs

CfgVars["steroidcost"] = 1000
CfgVars["regencost"] = 1000
CfgVars["ampcost"] = 2000
CfgVars["painkillercost"] = 2000
CfgVars["magicbulletcost"] = 1500
CfgVars["reflectcost"] = 2000
CfgVars["armorpiercercost"] = 400
CfgVars["antidotecost"] = 1000
CfgVars["focuscost"] = 300
CfgVars["shockwavecost"] = 400
CfgVars["doubletapcost"] = 600
CfgVars["knockbackcost"] = 400
CfgVars["magicbulletcost"] = 400
CfgVars["leechcost"] = 400
CfgVars["adrenalinecost"] = 700
CfgVars["doublejumpcost"] = 400

// Drug duration

CfgVars["steroidduration"] = 140
CfgVars["regenduration"] = 100
CfgVars["ampduration"] = 100
CfgVars["painkillerduration"] = 60
CfgVars["reflectduration"] = 60
CfgVars["focusduration"] = 60
CfgVars["antidoteduration"] = 100
CfgVars["magicbulletduration"] = 60
CfgVars["leechduration"] = 100
CfgVars["doubletapduration"] = 100
CfgVars["shockwaveduration"] = 60
CfgVars["doublejumpduration"] = 250
CfgVars["knockbackduration"] = 110
CfgVars["adrenalineduration"] = 130
CfgVars["armorpiercerduration"] = 60
CfgVars["randomduration"] = 100
CfgVars["superduration"] = 100
CfgVars["uberduration"] = 200

// Structure costs

CfgVars["gunvaultcost"] = 300
CfgVars["auto_turretcost"] = 1000
CfgVars["spawncost"] = 150
CfgVars["gunlabcost"] = 600

// Structure allows

CfgVars["allowspawn"] = 1
CfgVars["allowdispensers"] = 1
CfgVars["allowturrets"] = 1

// Struture maxes

CfgVars["turretmax"] = 4
CfgVars["maxgunvaults"] = 5
CfgVars["maxgunlabs"] = 2

// Misc gun cost

CfgVars["weldercost"] = 750

// Throwables cost

CfgVars["gasgrenadecost"] = 200
CfgVars["molotovcost"] = 400
CfgVars["flashbangcost"] = 125
CfgVars["grenadecost"] = 300
CfgVars["stickygernadecost"] = 2000

// Misc cost

CfgVars["armorcost"] = 700
CfgVars["snipeshieldcost"] = 500
CfgVars["knifecost"] = 150
CfgVars["bigbombcost"] = 100000000
CfgVars["scancost"]=25
CfgVars["helmetcost"]=500
CfgVars["healthcost"] = 250
CfgVars["doorcost"] = 30
CfgVars["toolkitcost"] = 100
CfgVars["scannercost"] = 250
CfgVars["doorchargecost"] = 500
CfgVars["c4cost"] = 10000
CfgVars["propcost"] = 50

// Misc allows

CfgVars["allowhelmet"] = 1
CfgVars["allowshield"] = 1
CfgVars["allowwelder"] = 1
CfgVars["proppaying"] = 0
CfgVars["strictsuicide"] = 0

// Random

CfgVars["hltvproxywashere"] = 1
CfgVars["qetcost"] = 100
CfgVars["gunmax"] = 10
CfgVars["cratemax"] = 10
CfgVars["drugmax"] = 20
CfgVars["maxspec"] = 4
CfgVars["bigbombmax"]=2
CfgVars["crosshair"] = 1
CfgVars["drugpayamount"] = 25
CfgVars["EquipmentDecayTimer"] = 3
CfgVars["WeaponDecayTimer"] = 3
CfgVars["DrugDecayTimer"] = 3
CfgVars["upgradecost"] = 1
CfgVars["afktime"] = 600
drugeffect_doubletapmod = 0.675
drugeffect_armorpiercermod = 1.25
drugeffect_shockwavemultiplier = 1
