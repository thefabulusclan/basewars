surface.CreateFont("hudfont", {
    font = "Trebuchet MS",
    size = 27,
    weight = 900,
})
surface.CreateFont("hudfont1", {
    font = "Trebuchet MS",
    size = 20,
    weight = 900,
})

MoneyAlpha = -1
MoneyY = 0
MoneyAmount = 0

MyMoney = 0

local Avatar = vgui.Create( "AvatarImage" )
Avatar:SetSize( 64, 64 )
Avatar:SetPos( 5, ScrH() - (ScrH()/8) + 5)

function DrawAvatar()
	Avatar:SetPlayer( LocalPlayer(), 64 )
	Avatar:SetVisible(true)
end
usermessage.Hook("DrawAvatar", DrawAvatar)

function DontDrawAvatar()
	Avatar:SetVisible(false)
end
usermessage.Hook("PlayerDied", DontDrawAvatar)

function myhud()
  local client = LocalPlayer()
	if !client:Alive() then return end
	if(client:GetActiveWeapon() == NULL or client:GetActiveWeapon() == "Camera") then return end

  local HudSizeX = ScrW()/8
  local HudSizeY = ScrH()/8

	surface.SetDrawColor(0,0,0,250)
  surface.DrawRect(0, ScrH() - (HudSizeY), HudSizeX, HudSizeY)
  surface.SetDrawColor(255,255,255,250)
  surface.DrawRect(HudSizeX*(3/8) - 2, ScrH() - (HudSizeY*(4/5)) - 2, HudSizeX*(1/2) + 4, (HudSizeY/5)*(1/2) + 4)
  surface.DrawRect(HudSizeX*(3/8) - 2, ScrH() - (HudSizeY*(3/5)) - 2, HudSizeX*(1/2) + 4, (HudSizeY/5)*(1/2) + 4)
  surface.SetDrawColor(0,0,0,250)
  surface.DrawRect(HudSizeX*(3/8), ScrH() - (HudSizeY*(4/5)), HudSizeX*(1/2), (HudSizeY/5)*(1/2))
  surface.DrawRect(HudSizeX*(3/8), ScrH() - (HudSizeY*(3/5)), HudSizeX*(1/2), (HudSizeY/5)*(1/2))
  surface.SetDrawColor(150,50,50,250)
  surface.DrawRect(HudSizeX*(3/8), ScrH() - (HudSizeY*(4/5)), HudSizeX*(1/2) * (client:Health() / client:GetMaxHealth()), (HudSizeY/5)*(1/2))
  surface.SetDrawColor(50,50,150,250)
  surface.DrawRect(HudSizeX*(3/8), ScrH() - (HudSizeY*(3/5)), HudSizeX*(1/2) * (client:Armor() / 255), (HudSizeY/5)*(1/2))

  //TODO: Move this to be in a cached area -> more optimized.
  local factionColor = team.GetColor(LocalPlayer():Team())
  local R = factionColor.r
  local G = factionColor.g
  local B = factionColor.b
  local brightnessOfFactionColor = (R+R+B+G+G+G)/(6*255)
  if brightnessOfFactionColor < 0.5 then
    surface.SetDrawColor(255, 255, 255, 250)
    surface.DrawRect(2, ScrH() - HudSizeY*(1/5) + 2, HudSizeX - 4, HudSizeY*(1/5) - 4)
    surface.DrawRect(2, ScrH() - HudSizeY*(2/5) + 2, HudSizeX - 4, HudSizeY*(1/5) - 4)
  else
    surface.SetDrawColor(255, 255, 255, 250)
    surface.DrawRect(0, ScrH() - HudSizeY*(2/5), HudSizeX, HudSizeY*(2/5))
    surface.SetDrawColor(0, 0, 0, 250)
    surface.DrawRect(2, ScrH() - HudSizeY*(1/5) + 2, HudSizeX - 4, HudSizeY*(1/5) - 4)
    surface.DrawRect(2, ScrH() - HudSizeY*(2/5) + 2, HudSizeX - 4, HudSizeY*(1/5) - 4)
  end
  surface.SetTextColor(factionColor)
  surface.SetFont("hudfont1")
  local factionWidth = surface.GetTextSize(team.GetName(LocalPlayer():Team()))
  local nameWidth = surface.GetTextSize(LocalPlayer():Name())

  surface.SetTextPos((HudSizeX/2) - (factionWidth/2), ScrH() - HudSizeY*(1/5) + 3)
  surface.DrawText(team.GetName(LocalPlayer():Team()))
  surface.SetTextPos((HudSizeX/2) - (nameWidth/2), ScrH() - HudSizeY*(2/5) + 3)
  surface.DrawText(LocalPlayer():Name())


  if Bilions then
    draw.DrawText( "$" .. string.Comma(MyMoney) .. " Billion", "hudfont1", HudSizeX*(3/8), ScrH() - (HudSizeY*(39/40)), Color(180, 180, 10, 255), 0 );
  else
    draw.DrawText( "$" .. string.Comma(MyMoney), "hudfont1", HudSizeX*(3/8), ScrH() - (HudSizeY*(39/40)), Color(180, 180, 10, 255), 0 );
  end
  if MoneyAmount != 0 then
    if MoneyAmount<0 then
      draw.DrawText( MoneyAmount, "hudfont1", HudSizeX*(3/8), ScrH() - (HudSizeY*(11/10)), Color( 150, 20, 20, MoneyAlpha ), 0 );
    else
      draw.DrawText( "+" .. MoneyAmount, "hudfont1", HudSizeX*(3/8), ScrH() - (HudSizeY*(11/10)), Color( 20, 150, 20, MoneyAlpha ), 0 );
    end
  end
  if MoneyAlpha > 0 then
    MoneyAlpha = MoneyAlpha-1
  end
  DrawDisplay();
end

hook.Add("HUDPaint", "huddie", myhud)

function hidehud(name)
    for k, v in pairs{"CHudHealth", "CHudBattery", "CHudAmmo", "CHudSecondaryAmmo"} do
        if name == v then return false end
    end
end
hook.Add("HUDShouldDraw", "hidehud", hidehud)



function MoneyChange( len )

	MoneyAmount = net.ReadInt(32)
	MyMoney = net.ReadInt(32)
	Bilions = net.ReadBool()
	MoneyAlpha = 255;

end
net.Receive( "MoneyChange", MoneyChange );
