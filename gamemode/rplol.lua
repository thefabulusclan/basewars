
function DrugPlayer(pl)
	pl:ConCommand("pp_motionblur 1")
	pl:ConCommand("pp_motionblur_addalpha 0.05")
	pl:ConCommand("pp_motionblur_delay 0.035")
	pl:ConCommand("pp_motionblur_drawalpha 0.75")
	pl:ConCommand("pp_dof 1")
	pl:ConCommand("pp_dof_initlength 9")
	pl:ConCommand("pp_dof_spacing 100")
end

function UnDrugPlayer(pl)
	pl:ConCommand("pp_motionblur 0")
	pl:ConCommand("pp_dof 0")
end

function scanPlayer( ply, cmd, args )
	if( not ply:CanAfford( CfgVars["scancost"] ) ) then
		Notify( ply, 4, 3, "Cannot afford this" );
		return false
	end
	if !IsValid(ply:GetTable().Tower) then
		Notify(ply,4,3,"You must have a Radar Tower to use this command.")
	else
		local userExists = false
		local tower = ply:GetTable().Tower
		if tower:GetTable().Scans<=0 || !tower:IsPowered() then
			Notify(ply,4,3,"Your radar tower is not charged enough to scan or does not have any power.")
		else
			local target = FindPlayer(args[1])
			if IsValid(target) then
				userExists = true
				if target:GetNWBool("scannered") then
					Notify(ply,4,3,"This person has an Anti-Scanner!")
					Notify(target,4,3,"Someone Attempted to Scan You! Your Scan Blocker has been removed...")
					target:SetNWBool("scannered", false)
					return false
				end

				ply:AddMoney( CfgVars["scancost"] * -1 );
				Notify( ply, 0, 3, "Scanning..." );
				Notify(target,1,3, ply:GetName() .. " has scanned you")

				local raidable = SpyScan(ply,target)
				if tower:GetUpgrade()>=2 then
					ReconScan(ply,target)
				end

				umsg.Start("RadarScan")
					umsg.Entity(target)
					umsg.Vector(target:GetPos())
					umsg.Short(target:EntIndex())
				umsg.End()
				local effectdata = EffectData()
					effectdata:SetOrigin(target:GetPos())
					effectdata:SetRadius(512)
				util.Effect("scanring", effectdata)

				tower:GetTable().Scans = tower:GetTable().Scans-1
				return raidable
			end

			if(!userExists) then
				Notify( ply, 4, 3, "Player not found." );
			end
		end
	end
end
concommand.Add("bw_scan", scanPlayer)

function ScanGui(ply, args)
	args = Purify(args)
	if( args != "" ) then return ""; end
	umsg.Start("Scangui", ply)
	umsg.End()
	return ""
end
AddChatCommand( "/scan", ScanGui)

function BuyDrug( ply, args )
	args = Purify(args)
	//if( args == "" ) then return ""; end
	local count = tonumber(args)

	local added  = RetrieveBenifits(ply)-1
	if added < 0 then added = 0 end

	if count==nil then count = 1 end
	if count>CfgVars["maxdruglabs"] || count<1 then count = 1 end
	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 85;
	trace.filter = ply;

	local tr = util.TraceLine( trace );

	for i=1,count do
		if( not ply:CanAfford( CfgVars["drug_labcost"] ) ) then
			Notify( ply, 4, 3, "Cannot afford this" );
			return "";
		end
		if(ply:GetTable().maxDrug >= CfgVars["maxdruglabs"]+ added)then
			Notify( ply, 4, 3, "Max Drug Labs Reached!" );
			return "";
		end
		ply:AddMoney( CfgVars["drug_labcost"] * -1 );
		Notify( ply, 0, 3, "You bought a Drug Lab" );
		local druglab = ents.Create( "drug_lab" );

		druglab:SetPos( tr.HitPos+Vector(0,0,i*10));
		druglab:SetBWOwner(ply)
		druglab:CPPISetOwner( ply )
		druglab:Spawn();
	end
	umsg.Start("chaching", ply)
	umsg.End()
	return "";
end
AddChatCommand( "/buydruglab", BuyDrug );

function BuyGunvault( ply )
    if( args == "" ) then return ""; end
	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 85;
	trace.filter = ply;

	local tr = util.TraceLine( trace );

		if( not ply:CanAfford( CfgVars["gunvaultcost"] ) ) then
			Notify( ply, 4, 3, "Cannot afford this" );
			return "";
		end
		if(ply:GetTable().maxvault >= CfgVars["maxgunvaults"])then
			Notify( ply, 4, 3, "Max Gun Vaults/Pill Boxes Reached!" );
			return "";
		end
		ply:AddMoney( CfgVars["gunvaultcost"] * -1 );
		Notify( ply, 0, 3, "You bought a Gun Vault" );
		local gunlab = ents.Create( "gunvault" );

		gunlab:SetPos( tr.HitPos );
		gunlab:SetBWOwner(ply)
		gunlab:CPPISetOwner( ply )
		gunlab:Spawn();
		umsg.Start("chaching", ply)
		umsg.End()
		return "";
end
AddChatCommand( "/buygunvault", BuyGunvault );

----------------------------
--ent_mad_fuel
function BuyIncedAmmo( ply )
    if( args == "" ) then return ""; end
	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 85;
	trace.filter = ply;

	local tr = util.TraceLine( trace );

		if( not ply:CanAfford( 500 )) then
			Notify( ply, 4, 3, "Cannot afford this" );
			return "";
		end
		ply:AddMoney( -500 );
		Notify( ply, 0, 3, "You bought a Fuel Tank" );
		local fuel = ents.Create( "ent_mad_fuel" );
		fuel:SetPos( tr.HitPos+Vector(0,0,40));
		fuel:Spawn();
		umsg.Start("chaching", ply)
		umsg.End()
		return "";
end
AddChatCommand( "/buyfuel", BuyIncedAmmo );

function GiveMoney( ply, args )
	args = Purify(args)
    if( args == "" ) then return ""; end

	local trace = ply:GetEyeTrace();

	if( trace.Entity:IsValid() and trace.Entity:IsPlayer() and trace.Entity:GetPos():Distance( ply:GetPos() ) < 150 and trace.Entity:GetNWBool("AFK")==false) then

		local amount = tonumber( args );
		if amount==nil then return "" end
		if( not ply:CanAfford( amount ) ) then

			Notify( ply, 4, 3, "Cannot afford this" );
			return "";

		end
		if  (amount!=math.Round(amount)) then
			Notify(ply, 4, 3, "Must be a whole number" );
			return "";
		end
		trace.Entity:AddMoney( amount );
		ply:AddMoney( amount * -1 );

		Notify( trace.Entity, 2, 4, ply:Nick() .. " has given you " .. amount .. " dollars!" );
		Notify( ply, 0, 4, "Gave " .. trace.Entity:Nick() .. " " .. amount .. " dollars!" );

	else

		Notify( ply, 1, 3, "Must be looking at and be within distance of another player that is not AFK!" );

	end
	return "";
end
AddChatCommand( "/give", GiveMoney );

function DropMoney( ply, args )
	args = Purify(args)
    if( args == "" ) then return ""; end

	local amount = tonumber( args );
	if amount==nil then return "" end
	if( not ply:CanAfford( amount ) ) then

		Notify( ply, 4, 3, "Cannot afford this!" );
		return "";

	end

	if( amount < 10 || amount!=math.Round(amount)) then

		Notify( ply, 4, 4, "Must be atleast 10 dollars!" );
		return "";

	end

	ply:AddMoney( amount * -1 );

	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 85;
	trace.filter = ply;

	local tr = util.TraceLine( trace );

	local moneybag = ents.Create( "prop_moneybag" );
  moneybag:SetModel( "models/props/cs_assault/Money.mdl" );
	moneybag:SetPos( tr.HitPos );
	moneybag:Spawn();
	moneybag:SetColor(Color(200,255,200,255))

	moneybag:GetTable().MoneyBag = true;
	moneybag:GetTable().Amount = amount;

	return "";

end
AddChatCommand( "/moneydrop", DropMoney );
AddChatCommand( "/dropmoney", DropMoney );

function GM:PlayerSpawnProp( ply, model )
	ply:ClearAFK()
	if( not self.BaseClass:PlayerSpawnProp( ply, model ) ) then return false; end

 	if (ply:GetNWBool("shitwelding")) then
		Notify(ply, 4, 3, "You cannot spawn props when people have welded your stuff or have recently bombed you!")
		return false;
	end

		if( CfgVars["proppaying"] == 1 ) then

			if( ply:CanAfford( CfgVars["propcost"] ) ) then

				Notify( ply, 0, 4, "Deducted $" .. CfgVars["propcost"].." for spawning a prop." );
				ply:AddMoney( -CfgVars["propcost"] );

				return true;

			else

				Notify( ply, 4, 4, "Need $" .. CfgVars["propcost"] .. " to spawn a prop." );
				return false;

			end

		else

			return true;

		end

end

local function CanDupe(ply, tr, mode)
	if ply:GetNWBool("shitwelding") then
		Notify(ply,4,3,"You cannot use toolgun when people have welded your stuff or have recently bombed you!")
		return false
	end
end
hook.Add( "CanTool", "CanDupe", CanDupe )

function GM:KeyPress( ply, code )
	ply:ClearAFK()
	self.BaseClass:KeyPress( ply, code )
end


function GM:EntityTakeDamage(target, dmginfo)
	local inflictor = dmginfo:GetInflictor()
	local attacker = dmginfo:GetAttacker()
	local damage = dmginfo:GetDamage()
	local scaler = 1
	if (inflictor:GetClass()=="env_physexplosion" || inflictor:GetClass()=="env_fire") && IsValid(inflictor:GetTable().attacker) then
		attacker = inflictor:GetTable().attacker
	end

	// This gets rid of prop damage and most world damage.
	local donotwant = false
	if (attacker!=nil && attacker:IsPlayer()==false) then
		local class = attacker:GetClass()
		if class== "entityflame" || inflictor:IsVehicle() || attacker:IsVehicle() || attacker.Structure then
			donotwant = true
		end
		for k, v in ipairs(physgunables) do
			if (class==v && v!="bigbomb") || (class==v && v=="bigbomb" && !dmginfo:IsExplosionDamage()) || (inflictor:IsWorld() && !dmginfo:IsFallDamage()) then
				donotwant = true
			end
		end
	end

	if attacker:IsPlayer() && (target:IsPlayer() || target.Structure) then
		local newtarget = target
		if !target:IsPlayer() then
			newtarget = target:GetBWOwner()
		end
		if newtarget:Team() != attacker:Team() then
			if newtarget:GetNWBool("IsRaidingFaction") then
				if attacker:Team() != newtarget:GetNWInt("RaidingID") then
					donotwant = true
				end
			else
				if attacker:UserID() != newtarget:GetNWInt("RaidingID") then
					donotwant = true
				end
			end
		elseif newtarget:Team() == 1 && newtarget != attacker && attacker:UserID() != newtarget:GetNWInt("RaidingID") then
			donotwant = true
		end
		if target.Structure && newtarget == attacker && attacker:GetNWInt("RaidingID") != 0 then
			donotwant = true
		end
	end

	if inflictor:IsWeapon() && inflictor:GetClass() == "m9k_m202" then
		dmginfo:ScaleDamage(3)
	end

	if donotwant then
		dmginfo:ScaleDamage(0)
	end
end
