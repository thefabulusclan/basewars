local equip = {}

equip.Cost = CfgVars["healthcost"]
equip.Data = "health"
equip.Model = "models/items/healthkit.mdl"
equip.Name = "Medkit"

AddWeapon(equip, "Equipment")