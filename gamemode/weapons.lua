//Table to hold the data for the weapons.
local gunTable = {};
local classToData = {}

//Here we have the AddWeapon method. This method is different than the AddWeapon on the client, as it uses the same files and info
//but processes it in a way to make spawning weapons easier
function AddWeapon(gun, cat)
	if (cat == "Equipment" || cat == "Other") then return end;
	gunTable[gun.Data] = gun;
	classToData[gun.Class] = gun.Data
end

function SpawnWeapon(wep, ply, pos)
	if type(wep) == "string" then
		wep = gunTable[wep]
	end
	local weapon = ents.Create( "spawned_weapon" );
	weapon:SetModel(wep.Model);
	weapon.Owner = ply
	weapon.Data = wep.Data
	weapon:SetNWString("weaponclass", wep.Class);
	weapon:SetPos( pos );
	weapon:Spawn();
	ply:GetTable().maxGuns = ply:GetTable().maxGuns+1
	return weapon
end

function GetWeaponDataFromClass(class)
	return classToData[class]
end

// Buy weapon is the method used when the player calls the buyweapon console command.
// It takes the given value from gunTable and uses that value to decide how to spawn the weapon.
function BuyWeapon( ply, args )
	args = Purify(args)
    if( args == "" ) then return ""; end
	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 85;
	trace.filter = ply;

	local tr = util.TraceLine( trace );
	local wep = gunTable[string.lower(args)]
	if (wep == nil) then
		Notify( ply, 1, 3, "That's not an available weapon." );
		return
	end
	if( not ply:CanAfford( wep.Cost ) ) then
		Notify( ply, 4, 3, "Cannot afford this" );
		return "";
	end

	MsgN(wep.Model)

	ply:AddMoney( wep.Cost * -1 );
	Notify( ply, 0, 3, "You bought a " .. wep.Name .."!" );

  SpawnWeapon(wep, ply, tr.HitPos)

	umsg.Start("chaching", ply)
	umsg.End()
	return "";
end
AddChatCommand( "/buyweapon", BuyWeapon );

function ccBuyWeapon(ply,command,args)
	if ply:GetTable().LastBuy+5<CurTime() then
		ply:GetTable().LastBuy=CurTime()
		BuyWeapon(ply,tostring(args[1]))
	end
end
concommand.Add("buyweapon",ccBuyWeapon)

function BuyShipment( ply, args )
	args = Purify(args)
	if ply:GetTable().maxShipments > CfgVars["cratemax"] then Notify( ply, 4, 3, "You have max shipments!") return "" end
    if( args == "" ) then return ""; end
	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 85;
	trace.filter = ply;
	local mult=1
	local tr = util.TraceLine( trace );
    local wep = gunTable[string.lower(args)]
	if (wep == nil) then
		Notify( ply, 1, 3, "That's not an available weapon." );
		return
	end
	if( not ply:CanAfford( 3*mult*wep.Cost ) ) then
		Notify( ply, 4, 3, "Cannot afford this" );
		return "";
	end
    local wmodel = wep.Model
    local class = wep.Class
    local content = wep.Name .. " Shipment"

	ply:AddMoney( (3*wep.Cost*mult) * -1 );
	Notify( ply, 0, 3, "You bought a " .. wep.Name .. " shipment!" );

	local weapon = ents.Create( "spawned_shipment" );
    weapon.WeaponModel = ( wmodel );
	weapon:SetNWString("weaponclass", class);
	weapon:SetNWString("Contents", content);
	weapon.Owner = ply
	weapon:SetPos( Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z));
	weapon:Spawn();

	umsg.Start("chaching", ply)
	umsg.End()
	ply:GetTable().maxShipments = ply:GetTable().maxShipments+1
	return "";
end
AddChatCommand( "/buyshipment", BuyShipment );

function ccBuyShipment(ply,command,args)
	if ply:GetTable().LastBuy+15<CurTime() then
		ply:GetTable().LastBuy=CurTime()
		BuyShipment(ply,tostring(args[1]))
	else
		Notify(ply, 4, 3, "You must wait 15 seconds in between spawning shipments!")
	end
end
concommand.Add("buyshipment",ccBuyShipment)

//TODO: Rewrite this to support any weapon.
/*function dropWeapon( ply )

	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 85;
	trace.filter = ply;

	local tr = util.TraceLine( trace );
	local ent = ply:GetActiveWeapon()
    local model = ""
    local class = ""
	if !IsValid(ent) then return  ""; end
	local diddrop = true
	local upgrade = ent:GetNWBool("upgraded")
	if(ent:GetClass( ) == "bb_deagle_alt") then
		model = "models/weapons/3_pist_deagle.mdl"
		class = "bb_deagle_alt"
	elseif(ent:GetClass( ) == "bb_fiveseven_alt") then
		model = "models/weapons/3_pist_fiveseven.mdl"
		class = "bb_fiveseven_alt"
	elseif(ent:GetClass( ) == "bb_glock_alt") then
		model = "models/weapons/3_pist_glock18.mdl"
		class = "bb_glock_alt"
	elseif(ent:GetClass( ) == "bb_ak47_alt") then
		model = "models/weapons/3_rif_ak47.mdl"
		class = "bb_ak47_alt"
	elseif(ent:GetClass( ) == "bb_mp5_alt") then
		model = "models/weapons/3_smg_mp5.mdl"
		class = "bb_mp5_alt"
	elseif(ent:GetClass( ) == "bb_m4a1_alt") then
		model = "models/weapons/3_rif_m4a1.mdl"
		class = "bb_m4a1_alt"
	elseif(ent:GetClass( ) == "bb_galil_alt") then
		model = "models/weapons/3_rif_galil.mdl"
		class = "bb_galil_alt"
	elseif(ent:GetClass( ) == "bb_aug_alt") then
		model = "models/weapons/3_rif_aug.mdl"
		class = "bb_aug_alt"
	elseif(ent:GetClass( ) == "bb_mac10_alt") then
		model = "models/weapons/3_smg_mac10.mdl"
		class = "bb_mac10_alt"
	elseif(ent:GetClass( ) == "bb_ump45_alt") then
		model = "models/weapons/3_smg_ump45.mdl"
		class = "bb_ump45_alt"
	elseif(ent:GetClass( ) == "bb_m249_alt") then
		model = "models/weapons/3_mach_m249para.mdl"
		class = "bb_m249_alt"
	elseif(ent:GetClass( ) == "bb_m3_alt") then
		model = "models/weapons/3_shot_m3super90.mdl"
		class = "bb_m3_alt"
	elseif(ent:GetClass( ) == "bb_xm1014_alt") then
		model = "models/weapons/3_shot_xm1014.mdl"
		class = "bb_xm1014_alt"
	elseif(ent:GetClass( ) == "bb_tmp_alt") then
		model = "models/weapons/3_smg_tmp.mdl"
		class = "bb_tmp_alt"
	elseif(ent:GetClass( ) == "bb_p90_alt") then
		model = "models/weapons/3_smg_p90.mdl"
		class = "bb_p90_alt"
	elseif(ent:GetClass( ) == "bb_awp_alt") then
		model = "models/weapons/3_snip_awp.mdl"
		class = "bb_awp_alt"
	elseif(ent:GetClass( ) == "bb_scout_alt") then
		model = "models/weapons/3_snip_scout.mdl"
		class = "bb_scout_alt"
	elseif(ent:GetClass( ) == "bb_g3sg1_alt") then
		model = "models/weapons/3_snip_g3sg1.mdl"
		class = "bb_g3sg1_alt"
	elseif(ent:GetClass( ) == "weapon_turretgun") then
		model = "models/weapons/w_smg1.mdl"
		class = "weapon_turretgun"
	elseif(ent:GetClass( ) == "bb_usp_alt") then
		model = "models/weapons/3_pist_usp.mdl"
		class = "bb_usp_alt"
	elseif(ent:GetClass( ) == "bb_dualelites_alt") then
		model = "models/weapons/w_pist_elite.mdl"
		class = "bb_dualelites_alt"
	elseif(ent:GetClass( ) == "bb_p228_alt") then
		model = "models/weapons/3_pist_p228.mdl"
		class = "bb_p228_alt"
	elseif(ent:GetClass( ) == "bb_famas_alt") then
		model = "models/weapons/3_rif_famas.mdl"
		class = "bb_famas_alt"
	elseif(ent:GetClass( ) == "weapon_knife2") then
		model = "models/weapons/w_knife_t.mdl"
		class = "weapon_knife2"
	elseif(ent:GetClass( ) == "weapon_worldslayer") then
		model = "models/weapons/w_rocket_launcher.mdl"
		class = "weapon_worldslayer"
	elseif(ent:GetClass( ) == "weapon_grenadegun") then
		model = "models/weapons/w_rif_sg552.mdl"
		class = "weapon_grenadegun"
	else
		// wont include energy weapons as dropable since people will drop it to reload it.
		Notify( ply, 4, 3, "You can't drop that!!" );
		diddrop = false
        return "";
	end
	ply:StripWeapon( ent:GetClass( ) )
    local weapon = ents.Create( "spawned_weapon" );
    weapon:SetModel(model)
    weapon:SetNWString("weaponclass", class)
    weapon:SetPos( tr.HitPos );
	weapon:SetNWEntity("Owner", ply)
	weapon:Spawn();
	return "";
end
AddChatCommand( "/drop", dropWeapon );*/
