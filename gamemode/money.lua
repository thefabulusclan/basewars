function UpdateMoney( self, value )
	net.Start("MoneyChange")
		net.WriteFloat(0)
		net.WriteFloat(value)
	net.Send(self)
end

local mysql_hostname = 'localhost' -- Your MySQL server address.
local mysql_username = 'root' -- Your MySQL user-name.
local mysql_password = '' -- Your MySQL password.
local mysql_database = 'hgdb' -- Your MySQL database.
local mysql_port = 3306 -- Your MySQL port. Most likely is 3306.

local isConnectedToDatabase = false

require('mysqloo')

local db = mysqloo.connect(mysql_hostname, mysql_username, mysql_password, mysql_database, mysql_port)

function db:onConnected()
	MsgN('Basewars MySQL: Connected!')
	isConnectedToDatabase = true

	local q = db:query("CREATE TABLE IF NOT EXISTS basewarsmoney (id BIGINT NOT NULL, money BIGINT NOT NULL, value BIGINT NOT NULL)")

	q:start()
end

function db:onConnectionFailed(err)
	MsgN('Basewars MySQL: Connection Failed, please check your settings: ' .. err)
	MsgN('Data will not be persistant.')
	isConnectedToDatabase = false
end

db:connect()

function GetMoney( ply )

	if !isConnectedToDatabase then
		ply:GetTable().Money = 5000
		ply:GetTable().Value = 0
		return
	end

	local money
	local value = 0

	local q = db:query("SELECT * FROM `basewarsmoney` WHERE id = '".. ply:GetNWFloat("id") .."'")

	function q:onSuccess(data)
		if #data > 0 then
			local row = data[1]

			money = tonumber(row.money)
			value = tonumber(row.value)


			ply:GetTable().Money = money
			ply:AddMoney(value)

		else

			local qq = db:query("INSERT INTO `basewarsmoney` (id, money, value) VALUES ('"..ply:GetNWFloat("id").."','1000','0')")

			function qq:onError(err, sql)
				if db:status() ~= mysqloo.DATABASE_CONNECTED then
					db:connect()
					db:wait()
					if db:status() ~= mysqloo.DATABASE_CONNECTED then
						ErrorNoHalt("Re-connection to database server failed.")
						return
					end
				end
				MsgN('Basewars MySQL: Insert Query Failed: ' .. err .. ' (' .. sql .. ')')
			end
			ply:GetTable().Money = 1000
			ply:AddMoney(value)
			qq:start()
		end
	end

	function q:onError(err, sql)
		if db:status() ~= mysqloo.DATABASE_CONNECTED then
            db:connect()
            db:wait()
			if db:status() ~= mysqloo.DATABASE_CONNECTED then
				ErrorNoHalt("Re-connection to database server failed.")
				return
            end
        end
        MsgN('Basewars MySQL: Fetch Query Failed: ' .. err .. ' (' .. sql .. ')')
	end
	q:start()
	ply:GetTable().Value = 0
end

function SetMoney( ply, refundable )
	local money
	local valuechange

	if !isConnectedToDatabase then
		return
	end

	valuechange = ply:GetTable().Value

	money = ply:GetTable().Money

	Moneys = string.format("%.0f", money)
	Values = string.format("%.0f", valuechange)

	local q = db:query("UPDATE `basewarsmoney` SET money = '"..Moneys.."', value = '" .. Values .. "' WHERE id = '".. ply:GetNWFloat("id") .."'")

	function q:onError(err, sql)
        if db:status() ~= mysqloo.DATABASE_CONNECTED then
            db:connect()
            db:wait()
			if db:status() ~= mysqloo.DATABASE_CONNECTED then
				ErrorNoHalt("Re-connection to database server failed.")
				return
            end
        end
        MsgN('Basewars MySQL: Set Query Failed: ' .. err .. ' (' .. sql .. ')')
	end
	q:start()
end
