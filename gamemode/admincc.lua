function ccAddMoney( ply, cmd, args )

	if ply:IsUserGroup("owner") || ply:IsUserGroup("dev") || game.SinglePlayer() then
		local target = FindPlayer(args[1])
		if( target ) then
			target:AddMoney(tonumber(args[2]))
		else
			ply:PrintMessage( 2, "Did not find player: " .. args[1] )
		end
	else
		ply:PrintMessage( 2, "You are not owner!")
	end
end
concommand.Add( "bw_addmoney", ccAddMoney );

function ccSetMoney( ply, cmd, args )

	if ply:IsUserGroup("owner") || ply:IsUserGroup("dev") || game.SinglePlayer() then
		local target = FindPlayer(args[1])
		if( target ) then
			target:AddMoney(-target:GetTable().Money)
			target:AddMoney(tonumber(args[2]))
			ply:PrintMessage( 2, args[1].."'s money has been set to " .. args[2] )
		else
			ply:PrintMessage( 2, "Did not find player: " .. args[1] )
		end
	else
		ply:PrintMessage( 2, "You are not an owner!")
	end
end
concommand.Add( "bw_setmoney", ccSetMoney );

function ccCheckRaidable(ply, cmd, args)
	if ply:IsUserGroup("owner") || ply:IsUserGroup("dev") || ply:IsAdmin() || ply:IsSuperAdmin() || game.SinglePlayer() then
		local raidable = " is not "
		local target = FindPlayer(args[1])
		if( target ) then
			if target:GetTable().raidableEntities > 0 then
				raidable = " is "
			end
			Notify(ply,3,3,target:GetName()..raidable.."raidable")
			ply:ChatPrint(target:GetName()..raidable.."raidable")
		else
			ply:PrintMessage( 2, "Did not find player: " .. args[1] )
		end
	else
		ply:PrintMessage( 2, "You are not an owner!")
	end
end
concommand.Add( "bw_check", ccCheckRaidable)

function CheckRaidable(ply, args)
	args = Purify(args)

	if ply:IsUserGroup("owner") || ply:IsAdmin() || ply:IsSuperAdmin() then
		umsg.Start("RunCommand", ply)
        umsg.String("bw_check "..args)
        umsg.End()
	end

	return ""
end
AddChatCommand("/raidable", CheckRaidable)

function ccEntityDump(ply, cmd, args)
	if ply:IsUserGroup("owner") || ply:IsUserGroup("dev") || ply:IsAdmin() || ply:IsSuperAdmin() || game.SinglePlayer() then
		local target = FindPlayer(args[1])
		if( target ) then
			for k,v in pairs(ents.GetAll()) do
				local meta = getmetatable(v)
				if v.GetBWOwner != nil then
					if v:GetBWOwner() == target then
						ply:ChatPrint(v:GetClass())
					end
				end
			end
		else
			ply:PrintMessage( 2, "Did not find player: " .. args[1] )
		end
	else
		ply:PrintMessage( 2, "You are not an owner!")
	end
end
concommand.Add( "bw_entdump", ccEntityDump)

function EntityDump(ply, args)
	args = Purify(args)

	if ply:IsUserGroup("owner") || ply:IsAdmin() || ply:IsSuperAdmin() then
        umsg.Start("RunCommand", ply)
        umsg.String("bw_entdump  "..args)
        umsg.End()
	end

	return ""
end
AddChatCommand("/dump", EntityDump)

function ccEntityDumpAll(ply, cmd, args)
	if ply:IsUserGroup("owner") || ply:IsUserGroup("dev") || ply:IsAdmin() || ply:IsSuperAdmin() || game.SinglePlayer() then
		for k,v in pairs(ents.GetAll()) do
			ply:ChatPrint(v:GetClass())
		end
	else
		ply:PrintMessage( 2, "You are not an owner!")
	end
end
concommand.Add( "bw_entdumpall", ccEntityDumpAll)

function ccClearDrugs(ply, cmd, args)
	if ply:IsUserGroup("owner") || ply:IsUserGroup("dev") || game.SinglePlayer() then
		for k,v in pairs(ents.GetAll()) do
			if string.StartWith(v:GetClass(), "item_") then
				v:Remove()
			end
		end
	else
		ply:PrintMessage( 2, "You are not an owner!")
	end
end
concommand.Add( "bw_cleardrugs", ccClearDrugs )

-- function ccSetCfgVar(ply,cmd,args)
	-- if( ply:EntIndex() ~= 0 and not Admins[ply:SteamID()] ) then
		-- ply:PrintMessage( 2, "You're not an admin" );
		-- return;
	-- end
	-- local var = args[1]
	-- if var == "allowarmor" then var = "hltvproxywashere" end
	-- local num = tonumber(args[2])
	-- if var && num then
		-- if CfgVars[var]==nil then
			-- Notify(ply, 4, 3, "That is not a valid var")
			-- return
		-- else
			-- CfgVars[var]=num
			-- local lol = var
			-- if lol=="hltvproxywashere" then lol = "allowarmor" end
			-- NotifyAll(3,3, ply:GetName() .. " has set " .. tostring(lol) .. " to " .. tostring(num))
		-- end
	-- else
		-- if CfgVars[var]==nil then
			-- Notify(ply, 4, 3, "That is not a valid var")
			-- return
		-- else
			-- local lol = var
			-- if lol=="hltvproxywashere" then lol = "allowarmor" end
			-- Notify(ply,3,3, tostring(lol) .. "=" .. tostring(CfgVars[var]))
		-- end
	-- end
-- end

-- concommand.Add( "bw_setvar", ccSetCfgVar );

function runacommandonthatfucker(ply, cmd, args)
	if ply:IsUserGroup("owner") || ply:IsUserGroup("dev") || game.SinglePlayer() then
		local target = FindPlayer(args[1])
		if target && args[2] then
			umsg.Start("RunCommand", target)
			umsg.String(tostring(args[2]))
			umsg.End()
		else
			ply:PrintMessage(2, "ERR, use valid syntax with this command")
		end
	else
		ply:PrintMessage( 2, "You are not an owner!")
	end
end
concommand.Add("bw_cexec", runacommandonthatfucker)
