local printers = {}
printers[1] = "Bronze Printer"
printers[2] = "Silver Printer"
printers[3] = "Gold Printer"
printers[4] = "Diamond Printer"
printers[5] = "Tungston Printer"
printers[6] = "Console Printer"
printers[7] = "Cobalt Printer"
printers[8] = "Mythril Printer"
printers[9] = "Adamantite Printer"
printers[10] = "Nuclear Printer"

local HUDNote_c = 0
local HUDNote_i = 1
HUDNotesm = {}
surface.CreateFont("Messages", {
	font = "times new roman",
	size = 18,
	weight = 500,
	antialiasing = true
})
function GM:AddMessage( str, type, length )

	local tab = {}
	tab.text 	= str
	tab.recv 	= SysTime()
	tab.len 	= length
	tab.velx	= 0
	tab.vely	= 0
	tab.x		= 20
	tab.y		= 0
	tab.a		= 255
	tab.type	= type

	table.insert( HUDNotesm, tab )

	HUDNote_c = HUDNote_c + 1
	HUDNote_i = HUDNote_i + 1

end


local function DrawMessagem( self, k, v, i )

	local H = ScrH() / 1024
	local x = v.x
	local y = v.y

	if ( !v.w ) then

		surface.SetFont( "Messages" )
		v.w, v.h = surface.GetTextSize( v.text )

	end

	local w = v.w
	local h = v.h

	w = w + 16
	h = h+((i-1)*v.h)
	y=y+h

	// Draw Icon

	surface.SetDrawColor( 255, 255, 255, v.a )

	local textcolor = Color(0,250,0,255)
	if v.type==1 then
		textcolor = Color(250,0,0,255)
	elseif v.type==2 then
		textcolor = Color(0,100,0,255)
	elseif v.type==3 then
		textcolor = Color(150,150,0,255)
	elseif v.type==4 then
		textcolor = Color(150,0,0,255)
	end
	if LocalPlayer():GetInfoNum("bw_showmessages" ,1)==1 then
		draw.SimpleText( v.text, "Messages", x+1, y+1, Color(0,0,0,150), TEXT_ALIGN_LEFT )
		draw.SimpleText( v.text, "Messages", x, y, textcolor, TEXT_ALIGN_LEFT )
	end
end


function GM:PaintMessages()
	if LocalPlayer():GetInfoNum("bw_showmessages" ,1)==1 then
		if ScrW()>1000 then
			draw.RoundedBox( 10, 10, 10, 570, 170, Color( 30, 30, 30, 100 ) )
		else
			draw.RoundedBox( 10, 10, 10, 450, 130, Color( 30, 30, 30, 100 ) )
		end
	end
	if ( !HUDNotesm ) then return end

	while #HUDNotesm>9 do
		table.remove(HUDNotesm, 1)
	end
	local i = 0
	for k, v in pairs( HUDNotesm ) do

		if ( k != 0 ) then

			i = i + 1
			DrawMessagem( self, k, v, i)

		end

	end
end

// lol redundant

function MainMessages(msg)
	local text = msg:ReadString()
	local type = msg:ReadShort()
	local time = type%10
	type = (type-time)/10
	GAMEMODE:AddMessage(text,type,time)
end
usermessage.Hook("MainMessage", MainMessages)

function PaydayMessage(msg)
	local text = "Payday! You made $"..msg:ReadShort().."!"
	GAMEMODE:AddMessage(text,0,3)
end
usermessage.Hook("PaydayMessage", PaydayMessage)

function PrinterMessage(len, pl)
	local printer = printers[net.ReadInt(5)]
	local printed = net.ReadInt(5)
	local amt = net.ReadString()
	local type = 4
	local text = ""
	if printed==0 then
		text = printer .. " printed $" .. string.Comma(amt) .. "."
		type = 0
	elseif printed==1 then
		text = "Your " .. printer .. " is out of paper, press use on it to resupply it."
	elseif printed==2 then
		text = "Your " .. printer .. " does not have enough power. Get a power plant."
	elseif printed==3 then
		text = "Your " .. printer .. " is not close enough to a vault! Get a vault or move the printer closer!"
	elseif printed==4 then
		text = "Your " .. printer .. " has run out of paper! Press use on it to resupply it."
		type = 1
	elseif printed==5 then
		text = "Your " .. printer .. " is about to run out of paper! Press use on it to resupply it."
	elseif printed==6 then
		text = printer .. " resupplied."
		type = 3
	elseif printed==7 then
		text = "Your "..printer .. " is overheating and has lost a level!!!"
		type = 1
	elseif printed==8 then
		text = "Your "..printer .. " has overheated and is destroyed!"
		type = 1
	end
	GAMEMODE:AddMessage(text,type,3)
end
net.Receive("PrinterMessage", PrinterMessage)

function AFKMessage(msg)
	local text = "You are AFK!"
	GAMEMODE:AddMessage(text,1,3)
end
usermessage.Hook("AFKMessage", AFKMessage)

function ReceiveNotification()
	msgtype = net.ReadInt(4)
	len = net.ReadInt(8)
	msg = net.ReadString()
	if len > 0 then
		notification.AddLegacy(msg, NOTIFY_GENERIC, len)
	end
	GAMEMODE:AddMessage(msg, msgtype, len)
end
net.Receive("Notification", ReceiveNotification)
