//TODO: This file needs to be cleaned up!


local nograv = {
"auto_turret_gun",
"svehicle_part_nophysics",
"svehicle_part",
}

// not local so that it can be used in all the rest of the game.
physgunables = {
"gmod_cameraprop",
"gmod_rtcameraprop",
"gmod_balloon",
"gmod_button",
"gmod_lamp",
"gmod_light",
"gmod_anchor",
"func_physbox",
"prop_physics",
"prop_physics_multiplayer",
"spawned_weapon",
"drug_lab",
"gunlab",
"dispenser",
"phys_magnet",
"m_crate",
"prop_ragdoll",
"gmod_thruster",
"gmod_wheel",
"item_steroid",
"item_painkiller",
"item_magicbullet",
"item_antidote",
"item_amp",
"item_helmet",
"item_random",
"item_buyhealth",
"item_superdrug",
"item_booze",
"item_scanner",
"item_toolkit",
"item_regen",
"item_reflect",
"item_focus",
"item_snipeshield",
"item_armor",
"item_food",
"item_leech",
"item_shockwave",
"item_doubletap",
"item_uberdrug",
"item_knockback",
"item_doublejump",
"item_armorpiercer",
"item_superdrugoffense",
"item_superdrugdefense",
"item_superdrugweapmod",
"pillbox",
"drugfactory",
"powerplant",
"weedplant",
"meth_lab",
"prop_effect",
"money_printer",
"still",
"gunvault",
"radartower",
"gunfactory",
"bigbomb",
"keypad",
"sign"
}

function WeldControl(ent,ply)
	if (IsValid(ply)) then
		if IsValid(ent) then
			ent:SetNWInt("welddamage", 150)
		end
		ply:GetTable().shitweldcount=ply:GetTable().shitweldcount-1
		if (ply:GetTable().shitweldcount<=0) then
			ply:GetTable().shitweldcount=0
			ply:SetNWBool("shitwelding", false)
		end
	end
end

local function GetEntOwner(ent)
	if !IsValid(ent) then return false end
	local owner = ent
	if ent:GetVar("PropProtection")==nil then return false end
	if IsValid(player.GetByUniqueID(ent:GetVar("PropProtection"))) then
		owner = player.GetByUniqueID(ent:GetVar("PropProtection"))
	end
	if owner!=ent then
		return owner
	else
		return false
	end
end

local function SetOwner(ent, ply)
	// take no chances, check it here too.
	if (IsValid(ent) && IsValid(ply) && ply:IsPlayer()) then
		ent:SetVar("PropProtection", ply:UniqueID() )
		return true
	else
		return false
	end
end

local originalCleanup = cleanup.Add
function cleanup.Add(ply,type,ent)
	if (IsValid(ply) && ply:IsPlayer() && IsValid(ent)) then
		SetOwner(ent, ply)
	end
	originalCleanup(ply,type,ent)
end

function SpawnedProp(ply, model, ent)
	SetOwner(ent, ply)
end

hook.Add("PlayerSpawnedProp", "playerSpawnedProp", SpawnedProp)
