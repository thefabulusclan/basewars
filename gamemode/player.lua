local meta = FindMetaTable( "Player" );

function meta:NewData()

	self:GetTable().Money = 1000
	self:GetTable().Pay = 1;
	self:SetTeam( 1 );

end

function meta:IsAllied(ply)
	if tonumber(self:GetInfo("bw_ally_pl"..ply:EntIndex()))==1 || (ply:Team() == self:Team() && self:Team() != 1) then
		return true
	else
		return false
	end
end

function meta:CanAfford( amount )

	if( amount < 0 ) then return false; end

	if( self:GetTable().Money - amount < 0 ) then
		return false;
	end

	return true;

end

function meta:AddMoney( amount, refundable )

	local oldamount = self:GetTable().Money
	if (self:GetNWBool("AFK") && amount>0) then
		amount = -amount
		Notify(self,1,3,"You are AFK!, you just lost $" .. -amount .. "!!!")
	end
	self:GetTable().Money = oldamount+amount
	if refundable then
		self:GetTable().Value = self:GetTable().Value - amount
	end
	local bils = false
	local plmoney = self:GetTable().Money
	if plmoney > 1000000000 then
		plmoney = math.floor(plmoney/1000000000)
		bils = true
	end
	SetMoney(self)
	net.Start("MoneyChange")
		net.WriteInt(amount, 32)
		net.WriteInt(plmoney, 32)
		net.WriteBool(bils)
	net.Send(self)
end

function meta:NetWorth()
	return self:GetTable().Value + self:GetTable().Money
end

function meta:CheckOverdose()
	local drugnum = #self:GetTable().activeDrugs
	if (drugnum>=5 && math.random(1,10)>2) then
		self:TakeDamage(150,self)
		ApplyDrug("Poison", self)
		Notify(self, 1, 3, "You are overdosing!")
	elseif (drugnum>=4 && math.random(1,10)>4) then
		self:TakeDamage(90,self)
		ApplyDrug("Poison", self)
		Notify(self, 1, 3, "You have overdosed!")
	elseif drugnum==3 && math.random(1,10)>5 then
		self:TakeDamage(60, self)
		ApplyDrug("Poison", self)
		Notify(self, 1, 3, "You are overdosing!")
	elseif drugnum==2 && math.random(1,10)>9 then
		self:TakeDamage(30, self)
		ApplyDrug("Poison", self)
		Notify(self, 1, 3, "You are overdosing!")
	end
end

function UpdateDrugs(ply)

	local identifier = ply:SteamID64() || "SinglePlayer"
	for k,v in pairs(ply:GetTable().activeDrugs) do
		local clientDrug = {}
		clientDrug.Class = v.Class
		clientDrug.Color = v.Color
		timer.Destroy(identifier .. "UnDrug" .. v.Class)
		timer.Destroy(identifier .. "FinishDrug" .. v.Class)
		if (v.UnDrug != nil) then
			v.UnDrug(ply)
		end
		if (v.FinishDrug != nil) then
			v.FinishDrug(ply)
		end

		net.Start("EndDrug")
			net.WriteTable(clientDrug)
		net.Send(ply)

	end

	ply:GetTable().activeDrugs = {}

	// OLD STUFF

	ply:SetNWBool("shielded", false)
	ply:SetNWBool("tooled", false)
	ply:SetNWBool("scannered", false)
	ply:SetNWBool("helmeted", false)

end

function GM:PlayerDeath( ply, weapon, killer )

	ply:CreateRagdoll()
	ply:Ignite(0.001,0)
	ply:Extinguish()
	UpdateDrugs(ply)
	ply:ConCommand("pp_motionblur 0")
	ply:ConCommand("pp_dof 0")
	local IDSteam = string.gsub(ply:SteamID(), ":", "")

	self.BaseClass:PlayerDeath( ply, weapon, killer )

	for k, v in pairs(ply:GetWeapons()) do
		local class = v:GetClass()
		if GetWeaponDataFromClass(class) != nil then
      if ply:GetTable().maxGuns <= CfgVars["gunmax"] then
				SpawnWeapon(GetWeaponDataFromClass(class), ply, ply:GetPos()+Vector(math.random(-10,10),math.random(-10,10),math.random(10,40)))
      end
			v:Remove()
		end
	end

	umsg.Start("PlayerDied", ply)
	umsg.End()

end

function GM:PlayerSpawn( ply )

	GAMEMODE:PlayerSetModel(ply)
	self.BaseClass:PlayerSpawn( ply );
  local default_walk = 250
  local default_run = 500
  GAMEMODE:SetPlayerSpeed(ply, default_walk, default_run)

  ply:Extinguish()
	ply:SetPlayerColor(Vector(ply:GetInfo("cl_playercolor")))
	ply:SetWeaponColor(Vector(ply:GetInfo("cl_weaponcolor")))

	ply:ConCommand("pp_motionblur 0")
	ply:ConCommand("pp_dof 0")

	ply:SetHealth(100+(20*RetrieveBenifits(ply)))
	ply:SetMaxHealth(100+(20*RetrieveBenifits(ply)))

	ply:GetTable().Headshot = false
	ply:UnSpectate()
	ply:StripWeapons()
	ply:RemoveAllAmmo()
	GAMEMODE:PlayerLoadout( ply )

	ply:CrosshairEnable();

	if( CfgVars["crosshair"] == 0 ) then

		ply:CrosshairDisable();

	end

	if ( IsValid(ply:GetTable().Spawnpoint )  ) then
    	local cspawnpos = ply:GetTable().Spawnpoint:GetPos()
			local trace = { }
    	trace.start = cspawnpos+Vector(0,0,2)
			trace.endpos = trace.start+Vector(0,0,16)
			trace.filter = ply:GetTable().Spawnpoint
			trace = util.TraceLine(trace)
			if IsValid(trace.Entity) then
				local minge = player.GetByUniqueID(trace.Entity:GetVar("PropProtection"))
				if (tobool(trace.Entity:GetVar("PropProtection"))) then
					trace.Entity:Remove()
				end
				ply:SetPos(cspawnpos+Vector(0,0,16))
			else
				ply:SetPos(cspawnpos+Vector(0,0,16))
			end
    end

	umsg.Start("DrawAvatar", ply)
	umsg.End()

end


function GM:PlayerLoadout( ply, tr )

	ply:Give( "weapon_physcannon" );
	ply:Give( "gmod_camera" );
	ply:Give( "gmod_tool" );
	ply:Give( "weapon_physgun" );
	ply:Give("fas2_p226")
	ply:GiveAmmo(120, "pistol")

end


function GM:PlayerInitialSpawn( ply )

	GAMEMODE:PlayerSetModel(ply)

	self.BaseClass:PlayerInitialSpawn( ply );

	ply:NewData();
	ply:GetTable().LastBuy = CurTime()
	ply:GetTable().maxDrugs = 0
	ply:GetTable().maxGuns = 0
	ply:GetTable().maxShipments = 0
	ply:GetTable().maxBigBombs = 0
	ply:GetTable().activeDrugs = {}
	ply:GetTable().raidAttempts = {}
	ply:GetTable().raidableEntities = 0
  ply:SetNWFloat("id", ply:SteamID64())
	ply:SetNWBool("FactionLeader", false)
	ply:SetNWBool("drawmoneytitle", true)
	ply:SetNWBool("AFK", false)
	ply:SetNWInt("RaidingID", 0)
	ply:SetNWBool("IsRaidingFaction", false)
	GetMoney(ply)

	ply:SetNWBool("helpExtracrap", false)

	ply:AllowFlashlight( true )

	ply:ConCommand("helpmenu")
	ply:ConCommand("bw_showsparks 0")

	timer.Simple( 2, function()
			for k,v in pairs(team.GetAllTeams()) do
			name = team.GetName(k)
			color = team.GetColor(k)
			red = color.r
			green = color.g
			blue= color.b
			umsg.Start("newTribe", ply)
				umsg.String(name)
				umsg.Short(k)
				umsg.Short(red)
				umsg.Short(green)
				umsg.Short(blue)
			umsg.End()
		end
	end)

end

function GM:PlayerDisconnected( ply )

	self.BaseClass:PlayerDisconnected( ply )

	ply:GetTable().Value = ply:GetTable().Value*0.3

	local identifier = ply:SteamID64() || "SinglePlayer"
	for k,v in pairs(ply:GetTable().activeDrugs) do
		timer.Destroy(identifier .. "UnDrug" .. v.Class)
		timer.Destroy(identifier .. "FinishDrug" .. v.Class)
		if (v.UnDrug != nil) then
			v.UnDrug(ply)
		end
		if (v.FinishDrug != nil) then
			v.FinishDrug(ply)
		end
	end
	//TODO: Transition faction Leader
	//TODO: Punish user for leaving during a raid
	if ply:GetNWInt("RaidingID") != 0 then
		if !ply:IsInBaserFaction() then
			if ply:GetNWBool("FactionLeader") then
				EndRaid(ply)
			end
		else
			EndRaid(ply)
		end
	end
end

function basewarsDamageScale( ply, hitgroup, dmginfo )
	ply:GetTable().Headshot = false
	if ( hitgroup == HITGROUP_HEAD ) then
		ply:GetTable().Headshot = true
	 	if ply:GetNWBool("helmeted") then
			dmginfo:ScaleDamage( 0.1 )
			local effectdata = EffectData()
				effectdata:SetOrigin( ply:GetPos()+Vector(0,0,60) )
				effectdata:SetMagnitude( 1 )
				effectdata:SetScale( 1 )
				effectdata:SetRadius( 2 )
			util.Effect( "Sparks", effectdata )
		end
	end
end
hook.Add("ScalePlayerDamage", "basewarsDamageScale", basewarsDamageScale)

function meta:SetAFK()
	self:SetNWBool("AFK", true)
end

function meta:ClearAFK()
	if (self:GetNWBool("AFK")) then
		self:SetNWBool("AFK", false)
	end
	timer.Destroy(self:SteamID() .. "AFK")
	timer.Create( self:SteamID() .. "AFK", CfgVars["afktime"], 1, function() self:SetAFK() end);
end

function GM:SetupPlayerVisibility(ply)
	for k,v in pairs(ents.FindByClass("bigbomb")) do
		if v:GetNWBool("armed") then
			AddOriginToPVS(v:GetPos())
		end
	end
end

function meta:CallDrug(drug)
	drug.Drug(self)
	table.insert(self:GetTable().activeDrugs, drug)
	local clientDrug = {}
	clientDrug.Class = drug.Class
	clientDrug.Color = drug.Color
	net.Start("StartDrug")
		net.WriteTable(clientDrug)
	net.Send(self)
	if drug.Class != "Poison" then
		self:CheckOverdose()
	end
	local identifier = self:SteamID64() || "SinglePlayer"
	if (drug.UnDrug != nil) then
		timer.Create(identifier .. "UnDrug" .. drug.Class, drug.Time, 1, function() drug.UnDrug(self) end)
	end
	if (drug.FinishDrug != nil) then
		timer.Create(identifier .. "FinishDrug" .. drug.Class, drug.Time/2 + drug.Time, 1, function()
			drug.FinishDrug(self)
			table.RemoveByValue(self:GetTable().activeDrugs, drug)
			net.Start("EndDrug")
				net.WriteTable(clientDrug)
			net.Send(self)
		end)
	end
end

function meta:CallSuperDrug(drug)
	drug.Drug(self)
	table.insert(self:GetTable().activeDrugs, drug)
	local clientDrug = {}
	clientDrug.Class = drug.Class
	clientDrug.Color = drug.Color
	net.Start("StartDrug")
		net.WriteTable(clientDrug)
	net.Send(self)
	local identifier = self:SteamID64() || "SinglePlayer"
	if (drug.UnDrug != nil) then
		timer.Create(identifier .. "UnDrug" .. drug.Class, drug.Time, 1, function() drug.UnDrug(self) end)
	end
	if (drug.FinishDrug != nil) then
		timer.Create(identifier .. "FinishDrug" .. drug.Class, drug.Time/2 + drug.Time, 1, function()
			drug.FinishDrug(self)
			table.RemoveByValue(self:GetTable().activeDrugs, drug)
			net.Start("EndDrug")
				net.WriteTable(clientDrug)
			net.Send(self)
		end)
	end
end

function meta:IsInBaserFaction()
	return (self:Team() == 1 || self:Team() == TEAM_CONNECTING || self:Team() == TEAM_UNASSIGNED || self:Team() == TEAM_SPECTATOR)
end
