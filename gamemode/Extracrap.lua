local structTable = {};
local printerTable = {};
Extracrap = { }

// Here we have the AddStruct method. This method is different than the AddStruct on the client, as it uses the same files and info
// but processes it in a way to make spawning structures easier
function AddStructure(struct, cat)
	if (struct.Class == nil) then struct.Class = struct.Data; end
	structTable[struct.Class] = struct;
end

// Here we have the AddStruct method. This method is different than the AddStruct on the client, as it uses the same files and info
// but processes it in a way to make spawning structures easier
function AddPrinter(struct, cat)
	printerTable[struct.Name] = struct;
end

//TODO: Get rid of the unique weapons spawns and add them to BuyWeapon in weapons.lua

// UpgradeTrace finds the entity the caller is facing on and calls Upgrade on that entity.
function UpgradeTrace(ply, args)
	args = Purify(args)
	if( args != "" ) then return ""; end
	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 150;
	trace.filter = ply;

	local tr = util.TraceLine( trace );
	if (!IsValid(tr.Entity)) then
		return "";
	end
	local ent = tr.Entity
	Upgrade(ply, "traced", {ent:EntIndex()})
	return "";
end
AddChatCommand( "/upgradetrace", UpgradeTrace )

// The Upgrades menu command
function Upgrades(ply, args)
	args = Purify(args)
	if( args != "" ) then return ""; end
	umsg.Start("Upgradegui", ply)
	umsg.End()
	return ""
end
AddChatCommand("/upgrade", Upgrades)

function Upgrade(ply, command, args)
	local ent = ents.GetByIndex(args[1])
	if ent:GetUpgrade()>=ent.MaxUpgrade then
		ent:SetUpgrade(ent.MaxUpgrade)
		Notify(ply, 4, 3, "This is already fully upgraded.")
		ply:ConCommand( "play buttons/button10.wav" )
		return ""
	end

	local lvl = ent:GetUpgrade() + 1

	local class = ent:GetClass()
	local price = 10000000000
	if class == "base_printer" then
		price = printerTable[ent:GetNWString("PrintName")].Cost
	else
		price = structTable[class].Cost
	end

	price = price * math.pow(2, lvl-1)

	if (!ply:CanAfford(price)) then
		Notify(ply, 4, 3, "Cannot afford this. Cost is $" .. price)
		ply:ConCommand( "play buttons/button10.wav" )
		return "" ;
	end

	ply:AddMoney(-price, true)

	ent:SetUpgrade(lvl)

	Notify( ply, 0, 3, "Applying level " .. lvl .. " upgrade.")
	ply:ConCommand( "play buttons/button4.wav" )
	return ""
end
concommand.Add("bw_upgrade", Upgrade)

// BuyStruct - buy a structure given in the arguments.
function BuyStruct(ply, structure)
  if( structure == "" ) then return ""; end
	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 85;
	trace.filter = ply;

	local tr = util.TraceLine( trace );

	local struct = structTable[structure]
	if (struct == nil) then return end
	if( not ply:CanAfford(struct.Cost) ) then
		Notify( ply, 4, 3, "Cannot afford this" );
		return "";
	end

	// If a variable to hold the amount of this specific structure we have isn't initialized, make it.
	if (ply:GetTable()["max" .. struct.Class] == nil) then ply:GetTable()["max" .. struct.Class] = 0; end

	if(ply:GetTable()["max" .. struct.Class] >= struct.Max)then
		Notify( ply, 4, 3, "Max " .. struct.Name .. " reached." );
		return "";
	end
	ply:AddMoney( struct.Cost * -1, true );
	Notify( ply, 0, 3, "You bought a " .. struct.Name );
	local spawnedStruct = ents.Create( struct.Class );

	// If the struct is something that pays out on destruction, we must send it's cost to the entity for payout calculation
	spawnedStruct.cost = struct.Cost;

	spawnedStruct:SetBWOwner(ply)
	spawnedStruct:CPPISetOwner( ply )
	spawnedStruct:SetPos( tr.HitPos+Vector(0,0,40));
	spawnedStruct:Spawn();
	if struct.Raidable then
		ply:GetTable().raidableEntities = ply:GetTable().raidableEntities + 1
	end
	ply:GetTable()["max" .. struct.Class] = ply:GetTable()["max" .. struct.Class] + 1
	umsg.Start("chaching", ply)
	umsg.End()
	return "";
end

// Console command to buy a structure
function ccBuyStructure( ply, command, args )
	local building = args[1]
	if ply:GetTable().LastBuy+1.5<CurTime() then
		ply:GetTable().LastBuy=CurTime()
		BuyStruct(ply, building);
	end
end
concommand.Add( "buystruct", ccBuyStructure );

// BuyPrinter - buy a printer given in the arguments.
function BuyPrinter(ply, structure)
  if( structure == "" ) then return ""; end
	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 85;
	trace.filter = ply;

	local tr = util.TraceLine( trace );

	local struct = printerTable[structure]
	if (struct == nil) then return end
	if( not ply:CanAfford(struct.Cost) ) then
		Notify( ply, 4, 3, "Cannot afford this" );
		return "";
	end

	// If a variable to hold the amount of this specific structure we have isn't initialized, make it.
	if (ply:GetTable()["max" .. struct.Name] == nil) then ply:GetTable()["max" .. struct.Name] = 0; end

	if(ply:GetTable()["max" .. struct.Name] >= struct.Max)then
		Notify( ply, 4, 3, "Max " .. struct.Name .. " reached." );
		return "";
	end
	ply:AddMoney( struct.Cost * -1, true );
	Notify( ply, 0, 3, "You bought a " .. struct.Name );
	local spawnedStruct = ents.Create( struct.Class );

	// If the struct is something that pays out on destruction, we must send it's cost to the entity for payout calculation
	spawnedStruct.cost = struct.Cost;

	spawnedStruct:SetBWOwner(ply)
	spawnedStruct:CPPISetOwner( ply )
	spawnedStruct:SetPos( tr.HitPos+Vector(0,0,40));
	spawnedStruct:Spawn();
	if struct.Raidable then
		ply:GetTable().raidableEntities = ply:GetTable().raidableEntities + 1
	end
	spawnedStruct:SetColor(struct.Color)
	spawnedStruct:BaseInitialize(struct.Model, struct.Cost, struct.printerNumber, struct.Name, struct.Health)
	ply:GetTable()["max" .. struct.Name] = ply:GetTable()["max" .. struct.Name] + 1
	umsg.Start("chaching", ply)
	umsg.End()
	return "";
end

// Console command to buy a structure
function ccBuyStructure( ply, command, args )
	local building = args[1]
	if ply:GetTable().LastBuy+1.5<CurTime() then
		ply:GetTable().LastBuy=CurTime()
		BuyStruct(ply, building);
	end
end
concommand.Add( "buystruct", ccBuyStructure );

// Console command to buy a printer
function ccBuyPrinter( ply, command, args )
	local building = args[1]
	if ply:GetTable().LastBuy+1.5<CurTime() then
		ply:GetTable().LastBuy=CurTime()
		BuyPrinter(ply, building);
	end
end
concommand.Add( "buyprinter", ccBuyPrinter );

// Big Bomb

function BuyBomb( ply )
	if( args == "" ) then return ""; end
	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 85;
	trace.filter = ply;

	local tr = util.TraceLine( trace );
	if( not ply:CanAfford( CfgVars["bigbombcost"] ) ) then
		Notify( ply, 4, 3, "Cannot afford this!" );
		return "";
	end
		if(ply:GetTable().maxBigBombs >= CfgVars["bigbombmax"])then
			Notify( ply, 4, 3, "Max Big Bombs Reached!" );
			return "";
		end
	ply:AddMoney( CfgVars["bigbombcost"] * -1 );
	Notify( ply, 0, 3, "You bought the Big Bomb." );
	local Bigbomb = ents.Create( "bigbomb" );
	Bigbomb:SetPos( tr.HitPos + tr.HitNormal*15);
	Bigbomb.Owner = ply
	Bigbomb:CPPISetOwner( ply )
	Bigbomb:Spawn();
	umsg.Start("chaching", ply)
	umsg.End()
	Bigbomb:Activate();
	return "";
end
AddChatCommand( "/buybomb", BuyBomb );

// buyknife

function BuyKnife( ply )
		if ( ply:GetTable().Arrested ) then
			Notify( ply, 4, 3, "You can't buy a knife while arrested!" );
			return "";
		end
		if( not ply:CanAfford( CfgVars["knifecost"] ) ) then
			Notify( ply, 4, 3, "Cannot afford this" );
			return "";
		end
		ply:AddMoney( CfgVars["knifecost"] * -1 );
		Notify( ply, 0, 3, "You bought a knife" );

		ply:Give("weapon_knife2")
		umsg.Start("chaching", ply)
		umsg.End()
		return "";
end
AddChatCommand( "/buyknife", BuyKnife );
// Teh weld0r

function BuyWelder( ply )
		if (CfgVars["allowwelder"] == 0) then
			Notify( ply, 4, 3, "Welder has been disabled!");
			return "";
		end
		if ( ply:GetTable().Arrested ) then
			Notify( ply, 4, 3, "You can't buy a welder while arrested!" );
			return "";
		end
		if( not ply:CanAfford( CfgVars["weldercost"] ) ) then
			Notify( ply, 4, 3, "Cannot afford this" );
			return "";
		end
		ply:AddMoney( CfgVars["weldercost"] * -1 );
		Notify( ply, 0, 3, "You bought a blowtorch/welder" );

		ply:Give("weapon_welder")
		umsg.Start("chaching", ply)
		umsg.End()
		return "";
end
AddChatCommand( "/buywelder", BuyWelder );
AddChatCommand( "/buyblowtorch", BuyWelder );

// buy a sentry turret

function BuyTurret( ply )
	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 150;
	trace.filter = ply;

	local tr = util.TraceLine( trace );
		if (CfgVars["allowturrets"] == 0) then
			Notify( ply, 4, 3, "Sentry turrets have been disabled!");
			return "";
		end
		if (not tr.HitWorld) then
			Notify( ply, 4, 3, "Please look at the ground to spawn sentry turret" );
			return "";
		end
		if( not ply:CanAfford( CfgVars["auto_turretcost"] ) ) then
			Notify( ply, 4, 3, "Cannot afford this" );
			return "";
		end
		if (ply:GetTable().maxturret == nil) then ply:GetTable().maxturret = 0; end
		if(ply:GetTable().maxturret >= CfgVars["turretmax"])then
			Notify( ply, 4, 3, "Max sentry turrets Reached!" );
			return "";
		end
		local SpawnPos = tr.HitPos + tr.HitNormal * 20
		local SpawnAng = tr.HitNormal:Angle()
		for k, v in pairs( ents.FindInSphere(SpawnPos, 1250)) do
			if (v:GetClass() == "info_player_deathmatch" || v:GetClass() == "info_player_rebel" || v:GetClass() == "gmod_player_start" || v:GetClass() == "info_player_start" || v:GetClass() == "info_player_allies" || v:GetClass() == "info_player_axis" || v:GetClass() == "info_player_counterterrorist" || v:GetClass() == "info_player_terrorist") then
				Notify( ply, 4, 3, "Cannot create sentry turret near a spawn point!" );
				return "" ;
			end
		end
		ply:AddMoney( CfgVars["auto_turretcost"] * -1 );
		Notify( ply, 0, 3, "You bought a Sentry turret" );

		local ent = ents.Create( "auto_turret" )
		ent:SetPos( SpawnPos + (tr.HitNormal*-3) )
		ent:SetAngles( SpawnAng + Angle(90, 0, 0) )

		ent:SetBWOwner(ply)
		ent:CPPISetOwner( ply )
		ent:SetNWString( "ally" , "")
		ent:SetNWString( "jobally", "")
		ent:SetNWString( "enemytarget", "")
		ent:SetNWBool( "hatetarget", false)

		ent:Spawn()
		ent:Activate()
		local head = ents.Create( "auto_turret_gun" )
		head:SetPos( SpawnPos + (tr.HitNormal*18) )
		head:SetAngles( SpawnAng + Angle(90, 0, 0) )
		head:Spawn()
		head:Activate()
		head:SetParent(ent)
		head.Body = ent
		ent.Head = head
		head:SetBWOwner(ply)
		umsg.Start("chaching", ply)
		umsg.End()
		return "";
end
AddChatCommand( "/buyturret", BuyTurret );

function BuySpawn( ply )

    if( args == "" ) then return ""; end
    	local trace = { }
    		trace.start = ply:GetPos()+Vector(0,0,1)
		trace.endpos = trace.start+Vector(0,0,90)
		trace.filter = ply
	trace = util.TraceLine(trace)
	if( trace.Fraction<1 ) then
            Notify( ply, 4, 3, "Need more room" );
            return "";
        end
        if( not ply:CanAfford( CfgVars["spawncost"] ) ) then
            Notify( ply, 4, 3, "Cannot afford this" );
            return "";
        end
	if(!ply:Alive())then
            Notify( ply, 4, 3, "Dead men buy no spawn points.");
            return "";
        end
	if IsValid(ply:GetTable().Spawnpoint) then
		ply:GetTable().Spawnpoint:Remove()
		Notify(ply,1,3, "Destroyed old spawnpoint to create this one.")
	end
        ply:AddMoney( CfgVars["spawncost"] * -1, true );
        Notify( ply, 0, 3, "You bought a spawn point!" );
        local spawnpoint = ents.Create( "spawnpoint" );
		spawnpoint:SetBWOwner(ply)
		spawnpoint:CPPISetOwner( ply )
        spawnpoint:SetPos( ply:GetPos());
		spawnpoint:SetColor(Color(255, 255, 255, 254))
	ply:SetPos(ply:GetPos()+Vector(0,0,3))
    spawnpoint:Spawn();
	umsg.Start("chaching", ply)
	umsg.End()
    return "";
end
AddChatCommand( "/buyspawnpoint", BuySpawn );

function TargetTurret(ply, args)
	args = Purify(args)
	if( args == "" ) then return ""; end
	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 150;
	trace.filter = ply;

	local tr = util.TraceLine( trace );
	local targent = tr.Entity
	if (targent:GetClass()!="auto_turret") then
		return "" ;
	end
	if (targent:GetBWOwner() != ply) then
		Notify( ply, 4, 3, "This is not your turret!" );
		return "" ;
	end
	Notify( ply, 0, 3, "Set turret target string to " .. args)
	targent:SetNWString("enemytarget", args)
	return "" ;
end
//AddChatCommand( "/settarget", TargetTurret );

// armor related shit.

function BuyItems( ply, args )
	if( args == "" ) then return ""; end
    if ply:GetTable().maxDrugs > CfgVars["drugmax"] then return ""; end
	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 85;
	trace.filter = ply;

    local item = args

    local tr = util.TraceLine( trace );

        if( not ply:CanAfford( CfgVars[item.."cost"] ) ) then
			Notify( ply, 4, 3, "Cannot afford this" );
			return "";
		end
		ply:AddMoney( CfgVars[item.."cost"] * -1 );
		Notify( ply, 0, 3, "You bought some "..item );
			local vehiclespawn = ents.Create( "item_"..item );
			vehiclespawn:SetPos( tr.HitPos + Vector(0, 25, 15));
			vehiclespawn:Spawn();
            vehiclespawn.Owner = ply
	umsg.Start("chaching", ply)
	umsg.End()
    ply:GetTable().maxDrugs = ply:GetTable().maxDrugs+1
    return "";
end
//AddChatCommand( "/buyitem", BuyItems )

// Batches

function BuyDrugBatches(ply, args)
    if( args == "" ) then return ""; end
    if ply:GetTable().maxDrugs > CfgVars["drugmax"] then return ""; end
	local trace = { }

	trace.start = ply:EyePos();
	trace.endpos = trace.start + ply:GetAimVector() * 85;
	trace.filter = ply;

    local item = args

    local tr = util.TraceLine( trace );

        if( not ply:CanAfford( CfgVars[item.."cost"] * 5 ) ) then
			Notify( ply, 4, 3, "Cannot afford this" );
			return "";
		end
		ply:AddMoney( CfgVars[item.."cost"] * -5 );
		Notify( ply, 0, 3, "You bought some "..item );
		for i=-2, 2, 1 do
			local vehiclespawn = ents.Create( "item_"..item );
			vehiclespawn:SetPos( tr.HitPos + Vector(0, i*12, 15));
			vehiclespawn:Spawn();
            vehiclespawn.Owner = ply
		end
	umsg.Start("chaching", ply)
	umsg.End()
    ply:GetTable().maxDrugs = ply:GetTable().maxDrugs+5
    return "";
end
//AddChatCommand( "/buybatch", BuyDrugBatches )

function BuyAmmo( ply )
		local plyweapon = ply:GetActiveWeapon()

		if ply:GetActiveWeapon():GetClass() == "weapon_mad_c4" then
			if( not ply:CanAfford( CfgVars["c4cost"] ) ) then
				Notify( ply, 4, 3, "Cannot afford this" );
				return "";
			end
			ply:GiveAmmo(1, plyweapon:GetPrimaryAmmoType())
			ply:AddMoney( -CfgVars["c4cost"] );
			Notify( ply, 0, 3, "You purchased one C4 Explosive for 10,000 dollars." );
		elseif ply:GetActiveWeapon():GetClass() == "weapon_doorcharge" or ply:GetActiveWeapon():GetClass() == "cse_eq_hegrenade" or ply:GetActiveWeapon():GetClass() == "weapon_gasgrenade" or ply:GetActiveWeapon():GetClass() == "cse_eq_flashbang" then
			if( not ply:CanAfford( 425 ) ) then
				Notify( ply, 4, 3, "Cannot afford this" );
				return "";
			end
			ply:GiveAmmo(3, plyweapon:GetPrimaryAmmoType())
			ply:AddMoney( -425 );
			Notify( ply, 0, 3, "3 Explosives Purchased for 425 dollars." );
		elseif ply:GetActiveWeapon():GetClass() == "weapon_stickgrenade" then
			if( not ply:CanAfford( 1750 ) ) then
				Notify( ply, 4, 3, "Cannot afford this" );
				return "";
			end
			ply:GiveAmmo(3, plyweapon:GetPrimaryAmmoType())
			ply:AddMoney( -1750 );
			Notify( ply, 0, 3, "3 Stickies Purchased for 425 dollars." );
		elseif ply:GetActiveWeapon():GetClass() == "weapon_welder" or ply:GetActiveWeapon():GetClass() == "weapon_knife2" or ply:GetActiveWeapon():GetClass() == "lockpick" or ply:GetActiveWeapon():GetClass() == "keys" or ply:GetActiveWeapon():GetClass() == "gmod_tool" or ply:GetActiveWeapon():GetClass() == "weapon_physgun" or ply:GetActiveWeapon():GetClass() == "gmod_camera" or ply:GetActiveWeapon():GetClass() == "weapon_physcannon" then
			Notify( ply, 4, 3, "This weapon does not require ammo." );
			return ""
		else
			if( not ply:CanAfford( 250 ) ) then
				Notify( ply, 4, 3, "Cannot afford this" );
				return "";
			end
			ply:GiveAmmo(25, plyweapon:GetPrimaryAmmoType())
			ply:AddMoney( -250 )
			Notify( ply, 0, 3, "25 Ammo Purchased for 250 dollars." );
		end
	umsg.Start("chaching", ply)
	umsg.End()
			return ""
end
AddChatCommand( "/buyammo", BuyAmmo );
concommand.Add("buyammo", BuyAmmo)

function catchMaxSetting(ent)
	if !ent.Structure then return end
	if (IsValid(ent:GetBWOwner()) && (ent:GetBWOwner():IsPlayer())) then
		local ply = ent:GetBWOwner()
		if (ply:GetTable()["max" .. ent:GetClass()] != nil ) then
			ply:GetTable()["max" .. ent:GetClass()] = ply:GetTable()["max" .. ent:GetClass()] - 1
			if structTable[ent:GetClass()].Raidable then
				ply:GetTable().raidableEntities = ply:GetTable().raidableEntities - 1
				if ply:GetNWInt("RaidingID") != 0 then
					EndRaidCheck(ply)
				end
			end
		elseif (ply:GetTable()["max" .. ent:GetNWString("PrintName", "absolutelyNOTHIN")]) != nil then
			local name = ent:GetNWString("PrintName")
			ply:GetTable()["max" .. name] = ply:GetTable()["max" .. name] - 1
			if printerTable[name].Raidable then
				ply:GetTable().raidableEntities = ply:GetTable().raidableEntities - 1
				if ply:GetNWInt("RaidingID") != 0 then
					EndRaidCheck(ply)
				end
			end
		end
	end
end
hook.Add("EntityRemoved", "catchMaxSetting", catchMaxSetting)
